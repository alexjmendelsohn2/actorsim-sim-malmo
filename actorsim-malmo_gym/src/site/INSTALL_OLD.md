
INSTALL CUDA (if on a machine with an NVidia GPU)
=================================================

http://developer.nvidia.com/cuda-downloads
sudo dpkg -i cuda-repo-ubuntu1604-8-0-local-ga2_8.0.61-1_amd64.deb
sudo apt-get update
sudo apt-get install cuda

INSTALL CUDNN
-------------
it makes you login:
https://developer.nvidia.com/rdp/cudnn-download
get cuDNN v5.1 Library for Linux
tar -xvzf cudnn-8.0-linux-x64-v5.1.tgz
cd cuda
export LD_LIBRARY_PATH=`pwd`:$LD_LIBRARY_PATH
which we add to our .bashrc
D_LIBRARY_PATH=/home/davidisele/cuda/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH


sudo apt install python-pip
pip install numpy matplotlib scipy image opencv-python


Install Malmo
==============

Create a virtual env for the version of python you expect to use.

For Python3.6, on older machines you will need to install the ppa

``
sudo add-apt-repository ppa:jonathonf/python-3.6
sudo apt-get update
sudo apt-get install python3.6
``



Python 3.x
-----------
Malmo 0.31  Seems to work fine with Python 3.6 even though the library was purportedly built for 3.5.


If you get an error "No module named ImageTk", 
pip install image  (successor to image-tk that uses pillow instead of PIL)


Download and unpack the Malmo release zip file for the correct version. For the rest of this tutorial, the directory where you unzipped Malmo will be called MALMO

you may find it handy to create symbolic links in the virtualenv

``
ln -s MALMO/Minecraft
ln -s MALMO/Python_Examples
``

Add the following to your bin/activate

``
MALMO_XSD_PATH="/home/mroberts/virtualenv/malmo/gbla/MALMO/Schemas"
export MALMO_XSD_PATH
JAVA_HOME="/usr/lib/jvm/latest"
export JAVA_HOME
export PYTHONPATH "$PYTHONPATH:/home/mroberts/virtualenv/summer/Python_Examples"

``

If needed, add the following to bin/activate.csh

``
setenv MALMO_XSD_PATH "/home/mroberts/virtualenv/gbla/Malmo-0.31.0-Linux-Ubuntu-16.04-64bit_withBoost_Python3.5/Schemas"
setenv JAVA_HOME "/usr/lib/jvm/latest"
setenv PYTHONPATH "/home/mroberts/virtualenv/summer/Python_Examples"
``


