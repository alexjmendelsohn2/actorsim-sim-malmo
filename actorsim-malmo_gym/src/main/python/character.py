
import json
import sys
from enum import IntEnum
from json import JSONDecodeError
from typing import Dict

import numpy as np
import traceback

from goals import ComplexGoal, PrimitiveGoal, PrimitiveGoalType, ComplexGoalType
from location import Location


class ActionSelection(IntEnum):
    GOAL = 0
    RUNION = 1
    RALL = 2


class CharacterState():
    task = None
    main_goal = None

    target = PrimitiveGoal(PrimitiveGoalType.TARGET)
    approach = PrimitiveGoal(PrimitiveGoalType.APPROACH)
    pickup = PrimitiveGoal(PrimitiveGoalType.PICKUP)
    aim = PrimitiveGoal(PrimitiveGoalType.AIM)
    strike = PrimitiveGoal(PrimitiveGoalType.STRIKE)
    kill = PrimitiveGoal(PrimitiveGoalType.KILL)

    mission_achieved = False

    random_action_selection = None

    valid_task = False

    def __init__(self):
        self.target_threshold = 0.975  # cosine similarity must higher than this to achieve target subgoal
        self.approach_threshold = 3.3  # distance must be less than this to achieve approach subgoal

    #items and state from latest observation (i.e., the most current state)
    obs = None
    data = None
    health = 0
    location = Location()
    items = dict()
    items_tmp = dict()
    pitch = 0
    yaw = 0
    mobs_killed = 0
    damage_dealt = 0
    target_location = None
    look_at = None
    look_distance = 0
    look_hit = ""  # type: str
    look_in_range = False  # type: bool

    #delta of items and state from latest to previous observation
    new_mobs_killed = 0
    new_damage_dealt = 0
    new_items = dict()

    def process_observation(self, new_obs, process_obs_debug=0):
        try:
            if process_obs_debug: print("\nProcessing observation")
            if process_obs_debug > 2: print("current:{}\npast:{}".format(new_obs, self.obs))
            if type(new_obs) is str:
                if len(new_obs) == 0:
                    return
                data = None  # type: Dict
                try:
                    data = json.loads(new_obs)  # observation comes in as a JSON string...
                except JSONDecodeError:
                    return

                self.health = float(data.get('Life', 0))

                if process_obs_debug > 1: print("processing character")

                if 'XPos' in data:
                    x = float(data.get('XPos'))
                    y = float(data.get('YPos'))
                    z = float(data.get('ZPos'))
                    self.location.set(x, y, z)
                    self.pitch = float(data.get('Pitch'))
                    self.yaw = float(data.get('Yaw'))

                if 'LineOfSight' in data:
                    line_of_sight = data.get('LineOfSight')
                    if process_obs_debug > 1: print(line_of_sight)
                    x = line_of_sight.get('x')
                    y = line_of_sight.get('y')
                    z = line_of_sight.get('z')
                    if self.look_at is None:
                        self.look_at = Location()
                    self.look_at.set(x, y, z)
                    self.look_distance = line_of_sight.get('distance')
                    self.look_hit = line_of_sight.get('hitType')
                    self.look_in_range = bool(line_of_sight.get('inRange'))

                    if process_obs_debug: print("look at:{} dist:{} hit:{} in_range:{}"
                                                .format(self.look_at, self.look_distance, self.look_hit, self.look_in_range))

                self.new_mobs_killed = 0
                if process_obs_debug > 1: print("processing mobs and damage")
                current_mobs_killed = float(data.get('MobsKilled', 0))
                if current_mobs_killed > self.mobs_killed:
                    self.new_mobs_killed = current_mobs_killed - self.mobs_killed
                self.mobs_killed = current_mobs_killed

                self.new_damage_dealt = 0
                current_damage_dealt = float(data.get('DamageDealt', 0))
                if current_damage_dealt > self.damage_dealt:
                    self.new_damage_dealt = current_damage_dealt - self.damage_dealt
                self.damage_dealt = current_damage_dealt

                if process_obs_debug > 1: print("processing entities")
                entities = data.get('Entities', [])
                if process_obs_debug > 1: print("entities:{}", entities)
                # entities arrive periodically cf. mission_spec: ObservationFromNearbyEntities.update_frequency
                if len(entities) > 0:
                    found_target = None
                    for entity in entities:
                        name = entity.get('name', "")
                        if name == "apple" or name == "Zombie":
                            x = entity.get('x', 0)
                            y = entity.get('y', 0)
                            z = entity.get('z', 0)
                            found_target = Location(x, y, z)
                    if found_target is None:
                        self.target_location = None
                    else:
                        self.target_location = found_target

                if process_obs_debug: print(" target:{}".format(self.target_location))

                self.new_items.clear()
                if process_obs_debug: print("processing items")
                #note: currently, this won't track items removed!
                self.__collect_hotbar_items(data, process_obs_debug)
                if process_obs_debug: print("items_past:{}".format(self.items))
                if process_obs_debug: print("items_now:{}".format(self.items_tmp))
                for item in self.items_tmp.keys():
                    if item in self.items.keys():
                        last_count = self.items[item]
                        current_count = self.items_tmp[item]
                        if current_count > last_count:
                            self.new_items[item] = current_count - last_count
                    else:
                        self.new_items[item] = self.items_tmp[item]
                self.items.clear()
                for item, count in self.items_tmp.items():
                    self.items[item] = count
                if process_obs_debug: print("new_items:{}".format(self.new_items))

                self.obs = new_obs
                self.data = data
        except Exception as e:
            print("PROBLEM: process reward failed because {}".format(e))
            traceback.print_exc()

    def __collect_hotbar_items(self, json_data, process_obs_debug):
        if process_obs_debug > 2: print("now collecting items from '{}'".format(json_data))
        self.items_tmp.clear()
        for i in range(0,8):
            hotbar_item = "Hotbar_{}_item".format(str(i))
            hotbar_size = "Hotbar_{}_size".format(str(i))
            item = json_data.get(hotbar_item)
            count = int(json_data.get(hotbar_size))
            self.items_tmp[item] = count

    def set_action_selection(self, selection):
        if selection == ActionSelection.GOAL.name:
            self.random_action_selection = ActionSelection.GOAL
        elif selection == ActionSelection.RALL.name:
            self.random_action_selection = ActionSelection.RALL
        elif selection == ActionSelection.RUNION.name:
            self.random_action_selection = ActionSelection.RUNION

    def set_task(self, task: str):
        self.task = task
        self._set_active_subgoals()
        self.main_goal.print_subgoals("for task:{} ".format(self.task), end_in='\n')

    def _set_active_subgoals(self, debug=False, practice_list=[]):
        self.valid_task = False
        for tmp_goal in [self.target, self.approach, self.pickup, self.aim, self.strike, self.kill]:
            tmp_goal.set_active(False)

        if self.task == "apple":
            self.main_goal = ComplexGoal(ComplexGoalType.COLLECT, 5.0, "apple",
                                         [self.target, self.approach, self.pickup])
            self.valid_task = True

        if self.task == "caged" \
                or self.task == "zombie":
            self.main_goal = ComplexGoal(ComplexGoalType.KILL, 5.0, "zombie",
                                         [self.target, self.approach, self.aim, self.strike, self.kill])
            self.valid_task = True

        if self.task == "agenda":
            practice_list = [self.approach]
            self.main_goal = ComplexGoal(ComplexGoalType.PRACTICE, 1.0, "practice",
                                         practice_list)
            self.valid_task = True

        for subgoal in self.main_goal.subgoals:
            subgoal.set_active(True)

        if debug: self.main_goal.print_subgoals("set active goals")

    def get_task_descriptor(self):
        d_var = 5
        # [monstor_type, #monsters, floor_type, #apple, rotation_angle]
        task_descriptor = np.zeros(d_var)

        apple_task = False
        zombie_task = False
        caged = False
        agenda_task = False

        if self.task == "apple":
            print("setting the apple task")
            apple_task = True
        elif self.task == "zombie":
            print("setting the zombie task")
            zombie_task = True
        elif self.task == "caged":
            print("setting the caged zombie task")
            zombie_task = True
            caged = True
        elif self.task == "agenda":
            agenda_task = True

        if zombie_task:
            task_descriptor[1] = 1
            if caged:
                task_descriptor[0] = 0
            else:
                task_descriptor[0] = 1
            apple_task = False

        if apple_task:
            task_descriptor[3] = 1

        if agenda_task:
            #zombie
            #task_descriptor[0] = 1
            #task_descriptor[1] = 1
            #apple
            task_descriptor[3] = 1


        # set rotation angle to something small
        task_descriptor[4] = 1

        return task_descriptor

    def determine_achieved_subgoals(self, debug=1):
        self.main_goal.reset()

        if self.target_location is not None:
            check_next_subgoal = False
            if self.main_goal.goal_type == ComplexGoalType.PRACTICE:
                check_next_subgoal = True

            if self.target.is_active():
                char = self.location.as_array_xz()
                target = self.target_location.as_array_xz()
                target_vec = target - char
                look = self.look_at.as_array_xz()
                look_vec = look - char
                target_len = np.linalg.norm(target_vec)
                look_len = np.linalg.norm(look_vec)

                sim = sum(target_vec * look_vec) / (target_len * look_len)

                within_threshold = sim > self.target_threshold
                if debug: print("  TARGET char:{} target:{} sim:{:0.4f} threshold:{} within:{}" \
                                .format(char, target, sim,self.target_threshold, within_threshold))
                if debug > 1: print("tLen:{} lLen:{} tVec:{} lVec:{} ").format(target_len, look_len, target_vec, look_vec)
                if within_threshold:
                    self.target.set_achieved()
                    check_next_subgoal = True

            if self.approach.is_active():
                if check_next_subgoal:
                    check_next_subgoal = False
                    dist = self.location.dist(self.target_location)
                    within_threshold = dist < self.approach_threshold
                    if debug: print("  APPROACH dist:{} threshold:{} within:{}".format(dist, self.approach_threshold, within_threshold))
                    if within_threshold:
                        self.approach.set_achieved()
                        check_next_subgoal = True

            if self.aim.is_active():
                if check_next_subgoal:
                    check_next_subgoal = False
                    aiming_success = self.look_in_range and self.look_hit == "entity"
                    if debug: print("  AIM look_hit:{} inRange:{} aim:{}".format(self.look_hit, self.look_in_range, aiming_success))
                    if aiming_success:
                        self.aim.set_achieved()
                        check_next_subgoal = True

            if self.strike.is_active():
                if check_next_subgoal:
                    check_next_subgoal = False
                    achieved = self.new_damage_dealt > 0
                    if debug: print("  STRIKE damage:{} achieved:{}".format(self.new_damage_dealt, achieved))
                    if achieved:
                        self.strike.set_achieved()
                        check_next_subgoal = True

            # this test is required since the apple may be picked up without fulfilling all subgoals
            if self.pickup.is_active():
                if "apple" in self.new_items:
                    if debug: print("  SUCCESS!")
                    self.pickup.set_achieved()
                    self.mission_achieved = True

            # this test is required since the zombie may be killed without fulfilling all subgoals
            if self.kill.is_active():
                if self.new_mobs_killed > 0:
                    if debug: print("  SUCCESS!")
                    self.kill.set_achieved()
                    self.mission_achieved = True

            all_subgoals_achieved = True
            for subgoal in self.main_goal.subgoals:
                if not subgoal.is_achieved():
                    all_subgoals_achieved = False
                    break
            if all_subgoals_achieved:
                self.main_goal.set_achieved()
                self.mission_achieved = True

        else:
            if debug: print("Waiting on observation with entities")
        if debug: self.main_goal.print_subgoals("achieved: ")

    def get_lowest_subgoal(self):
        return_goal = None
        for subgoal in self.main_goal.subgoals:  # type: PrimitiveGoal
            if subgoal.is_active():
                if not subgoal.is_achieved():
                    return_goal = subgoal
                    break
        return return_goal

    def select_random_action(self):
        unachieved_goal = None  # type: PrimitiveGoal
        random_id = 0

        if self.random_action_selection == ActionSelection.GOAL:
            # select a random action from the first active goal that is *not* achieved
            subgoal = self.get_lowest_subgoal()
            if subgoal is not None:
                unachieved_goal = subgoal
                random_id = subgoal.get_random_action()
        elif self.random_action_selection == ActionSelection.RALL:
            # select a random action from 1-12
            random_id = np.random.randint(12) + 1  # add one to shift up by one
        elif self.random_action_selection == ActionSelection.RUNION:
            action_union = list(PrimitiveGoalType.NONE.get_action_union())
            index = np.random.randint(len(action_union))
            random_id = action_union[index]
        else:
            print("Unknown action selection type! Set one with set_action_selection()")
            exit(1)

        return random_id, unachieved_goal

    def process_rewards(self):
        reward = self.main_goal.get_reward()
        return reward
