import numpy as np

def build_mission_spec(task_descriptor, args):
    xStart = -10
    xEnd = 10

    zStart = -10
    zEnd = 10

    yStart = 1
    yEnd = 6  # excluding the lighting

    # Monster Type
    caged = False
    if task_descriptor[0] == 0:
        mob_type = 'Zombie'
        caged = True
    elif task_descriptor[0] == 1:
        mob_type = 'Zombie'
        caged = False
    else:
        print("only zombies are supported!")

    # Monster Number
    monster_string = ''
    num_zombies = int(task_descriptor[1])
    for i in range(num_zombies):
        yPos = yStart + 1
        xPos = 0
        zPos = 6
        zombieX = xPos + 0.5
        zombieZ = zPos + 0.5
        m_string = '''<DrawEntity x="''' + str(zombieX) + '''" y="''' + str(yPos) + '''" z="''' + str(
            zombieZ) + '''" type="''' + mob_type + '''"/>'''
        monster_string += m_string
        if caged:
            block_type = "bedrock"
            blockAbove = '''<DrawBlock x="''' + str(xPos) + '''" y="''' + str(yPos + 2) + '''" z="''' + str(
                zPos) + '''" type="''' + block_type + '''"/>'''
            blockLeft = '''<DrawBlock x="''' + str(xPos - 1) + '''" y="''' + str(yPos) + '''" z="''' + str(
                zPos) + '''" type="''' + block_type + '''"/>'''
            blockRight = '''<DrawBlock x="''' + str(xPos + 1) + '''" y="''' + str(yPos) + '''" z="''' + str(
                zPos) + '''" type="''' + block_type + '''"/>'''
            blockBack = '''<DrawBlock x="''' + str(xPos) + '''" y="''' + str(yPos) + '''" z="''' + str(
                zPos - 1) + '''" type="''' + block_type + '''"/>'''
            blockFront = '''<DrawBlock x="''' + str(xPos) + '''" y="''' + str(yPos) + '''" z="''' + str(
                zPos + 1) + '''" type="''' + block_type + '''"/>'''
            monster_string += blockAbove + blockLeft + blockRight + blockBack + blockFront

    print("num_zombies:{} monster string:{}".format(num_zombies, monster_string))

    # Material
    if task_descriptor[2] == 0:
        material = 'sandstone'
    elif task_descriptor[2] == 1:
        material = 'hardened_clay'
    elif task_descriptor[2] == 2:
        material = 'grass'
    elif task_descriptor[2] == 3:
        material = 'bedrock'  # grass sandstone

    northMaterial = material
    southMaterial = material
    eastMaterial = material
    westMaterial = material

    # Apple
    is_apple = task_descriptor[3] > 0
    apple_string = ''
    if is_apple:
        apple_string = '''<DrawItem x="''' + str(0) + '''" y="''' + str(yStart + 1) + '''" z="''' + str(8) + '''" type="apple"/>'''
    print("apple string:{}".format(apple_string))

    floor = '''<!-- floor of the arena --> <DrawCuboid''' \
            + ''' x1="''' + str(xStart) + '''"''' \
            + ''' y1="''' + str(yStart) + '''"''' \
            + ''' z1="''' + str(zStart) + '''"''' \
            + ''' x2="''' + str(xEnd) + '''"''' \
            + ''' y2="''' + str(yStart) + '''"''' \
            + ''' z2="''' + str(zEnd) + '''"''' \
            + ''' type="''' + material + '''" />'''

    air = '''<!-- air of the arena --> <DrawCuboid''' \
          + ''' x1="''' + str(xStart) + '''"''' \
          + ''' y1="''' + str(yStart) + '''"''' \
          + ''' z1="''' + str(zStart) + '''"''' \
          + ''' x2="''' + str(xEnd) + '''"''' \
          + ''' y2="''' + str(yEnd) + '''"''' \
          + ''' z2="''' + str(zEnd) + '''"''' \
          + ''' type="air" />'''

    lighting = '''<!-- lighting of the arena --> <DrawCuboid''' \
               + ''' x1="''' + str(xStart) + '''"''' \
               + ''' y1="''' + str(yEnd) + '''"''' \
               + ''' z1="''' + str(zStart) + '''"''' \
               + ''' x2="''' + str(xEnd) + '''"''' \
               + ''' y2="''' + str(yEnd + 1) + '''"''' \
               + ''' z2="''' + str(zEnd) + '''"''' \
               + ''' type="glowstone" />'''

    eastWall = '''<!-- east wall of the arena --> <DrawCuboid''' \
               + ''' x1="''' + str(xStart - 1) + '''"''' \
               + ''' y1="''' + str(yStart) + '''"''' \
               + ''' z1="''' + str(zStart) + '''"''' \
               + ''' x2="''' + str(xStart) + '''"''' \
               + ''' y2="''' + str(yEnd) + '''"''' \
               + ''' z2="''' + str(zEnd) + '''"''' \
               + ''' type="''' + eastMaterial + '''" />'''
    westWall = '''<!-- west wall of the arena --> <DrawCuboid''' \
               + ''' x1="''' + str(xEnd) + '''"''' \
               + ''' y1="''' + str(yStart) + '''"''' \
               + ''' z1="''' + str(zStart) + '''"''' \
               + ''' x2="''' + str(xEnd + 1) + '''"''' \
               + ''' y2="''' + str(yEnd) + '''"''' \
               + ''' z2="''' + str(zEnd) + '''"''' \
               + ''' type="''' + westMaterial + '''" />'''
    southWall = '''<!-- south wall of the arena --> <DrawCuboid''' \
                + ''' x1="''' + str(xStart) + '''"''' \
                + ''' y1="''' + str(yStart) + '''"''' \
                + ''' z1="''' + str(zStart - 1) + '''"''' \
                + ''' x2="''' + str(xEnd) + '''"''' \
                + ''' y2="''' + str(yEnd) + '''"''' \
                + ''' z2="''' + str(zStart) + '''"''' \
                + ''' type="''' + southMaterial + '''" />'''
    northWall = '''<!-- north wall of the arena --> <DrawCuboid''' \
                + ''' x1="''' + str(xStart) + '''"''' \
                + ''' y1="''' + str(yStart) + '''"''' \
                + ''' z1="''' + str(zEnd) + '''"''' \
                + ''' x2="''' + str(xEnd) + '''"''' \
                + ''' y2="''' + str(yEnd) + '''"''' \
                + ''' z2="''' + str(zEnd + 1) + '''"''' \
                + ''' type="''' + northMaterial + '''" />'''

    # Rotation
    rot = int(task_descriptor[4] / 4.0 * 180)
    yaw = str(np.random.uniform(-rot, rot))

    summary_str = "Defeat the Monster!"
    if is_apple:
        summary_str = "Get the apple!"

    intro = '''<?xml version="1.0" encoding="UTF-8" ?>
    <Mission xmlns="http://ProjectMalmo.microsoft.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <About>
            <Summary>''' + summary_str + '''</Summary>
        </About>
        <ModSettings>
            <MsPerTick>40</MsPerTick>
            <PrioritiseOffscreenRendering>''' + str(args.malmo_rendering_off).lower() + '''</PrioritiseOffscreenRendering>
        </ModSettings>'''

    ## FLATWORLD
    server = '''  <ServerSection>
                    <ServerInitialConditions>
                        <Time><StartTime>1</StartTime></Time>
                    </ServerInitialConditions>
                    <ServerHandlers>
                    <FlatWorldGenerator generatorString="3;7;biome_1"/> <!-- go to http://www.minecraft101.net/superflat/ -->
                      <DrawingDecorator>
                        ''' + northWall + southWall + eastWall + westWall + '''
                        ''' + air + '''
                        ''' + floor + '''
                        ''' + lighting + '''
                        ''' + monster_string + '''
                        ''' + apple_string + '''
                      </DrawingDecorator>

                      <ServerQuitFromTimeUp timeLimitMs="''' + str(args.malmo_time_in_ms) + '''"/>
                      <ServerQuitWhenAnyAgentFinishes/>
                    </ServerHandlers>
                  </ServerSection>'''

    agent = '''	<AgentSection mode="Survival">
            <Name>Alex</Name>
            <AgentStart>
                <Placement x="''' + str(0) + '''" y="''' + str(yStart + 1) + '''" z="''' + str(0) + '''" pitch="0" yaw="''' + yaw + '''"/>
                <Inventory>
                    <InventoryBlock slot="0" type="iron_sword" quantity="1"/>
                    <InventoryBlock slot="1" type="iron_pickaxe" quantity="1"/>
                    <InventoryBlock slot="2" type="torch" quantity="1"/>
                </Inventory>
            </AgentStart>
            <AgentHandlers>
                <VideoProducer want_depth="false">
                    <Width>''' + str(args.malmo_video_width) + '''</Width>
                    <Height>''' + str(args.malmo_video_height) + '''</Height>
                </VideoProducer>
                <ContinuousMovementCommands/>
                <ObservationFromFullStats/>
                <ObservationFromHotBar/>
                <ObservationFromRay/>
                <MissionQuitCommands/>
                <ObservationFromNearbyEntities>
                  <Range name="Entities" xrange="30" yrange="5" zrange="30" update_frequency="7"/> 
                </ObservationFromNearbyEntities>
            </AgentHandlers>
        </AgentSection>'''

    close = '''</Mission>'''
    mission_spec = intro + server + agent + close
    return mission_spec


