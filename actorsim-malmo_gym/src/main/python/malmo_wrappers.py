
## Wrappers borrowed from openai:baselines/common/atari_wrappers.py
from collections import deque

import gym

from character import CharacterState

from baselines.common.atari_wrappers import WarpFrame, FrameStack, MaxAndSkipEnv
from malmo_connector_loggers import MalmoConnectorLoggers


class WrapReward(gym.RewardWrapper):
    def __init__(self, env, character: CharacterState):
        gym.Wrapper.__init__(self, env)
        self.character = character
        self.steps = 0

    def step(self, action):
        self.steps += 1
        goal_state = self.character.main_goal.subgoal_str()
        goal = self.character.get_lowest_subgoal()
        goal_name = "---"
        if goal != None:
            goal_name = goal.goal_type.name

        observation, reward, done, info = self.env.step(action)
        self.character.process_observation(info)
        self.character.determine_achieved_subgoals(debug=0)
        reward = self.reward(0)
        if self.character.main_goal.is_achieved():
            done = True

        from actorsim_logging.LogManager import LogManager
        logger = MalmoConnectorLoggers.malmoenv_connector_root_logger
        if logger.isEnabledFor(LogManager.TRACE):
            msg = "i:{:7d} {} {:<3} action:{}".format(self.steps, goal_state, goal_name[0:3], action)
            logger.trace()
            if reward < 0:
                msg += "        r:{: 0.02f}".format(reward)
            else:
                msg += " r:{: 0.02f}".format(reward)
            logger.trace(msg)


        return observation, reward, done, info

    def reward(self, reward):
        return reward + self.character.process_rewards()



def wrap_malmo(env, character: CharacterState, frame_stack=True):
    """Configure Malmo environment for DeepMind-style Nature CNN used for Atari.
    """
    env = WrapReward(env, character)

    # if episode_life:
    #     env = EpisodicLifeEnv(env)
    # if 'FIRE' in env.unwrapped.get_action_meanings():
    #     env = FireResetEnv(env)
    env = WarpFrame(env, grayscale=False)
    # if scale:
    #     env = ScaledFloatFrame(env)
    # if clip_rewards:
    #     env = ClipRewardEnv(env)
    env = MaxAndSkipEnv(env, skip=4)
    # if frame_stack:
    #     env = FrameStack(env, 4)
    return env

