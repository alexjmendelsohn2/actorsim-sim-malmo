package nrl.actorsim.malmo;

import nrl.actorsim.domain.WorldObject;

public class MalmoWorldObject extends WorldObject {

    public MalmoWorldObject(String typeName) {
        super(typeName);
    }

    public MalmoWorldObject(String typeName, String ... superTypeNames) {
        super(typeName);
        addSuperType(superTypeNames);
    }

}
