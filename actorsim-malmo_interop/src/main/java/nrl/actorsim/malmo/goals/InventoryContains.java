package nrl.actorsim.malmo.goals;

import nrl.actorsim.domain.NumberTransitionStatement;
import nrl.actorsim.domain.NumericPersistenceStatement;
import nrl.actorsim.domain.Statement;
import nrl.actorsim.malmo.MalmoWorldObject;
import nrl.actorsim.domain.StateVariable;

import static nrl.actorsim.malmo.MalmoPlanningDomain.INVENTORY_CONTAINS;

public class InventoryContains extends MalmoGoal {
    public InventoryContains(Statement statement) {
        super(statement);
    }

    public static MalmoGoal buildPersistence(MalmoWorldObject item, int qty) {
        try {
            StateVariable inv = INVENTORY_CONTAINS.copier()
                    .specializeArgs(item.argument()).build();
            Statement statement = new NumericPersistenceStatement(inv, qty);
            return new InventoryContains(statement);
        } catch (IllegalArgumentException iae) {
            logger.error("{}: {}", InventoryContains.class.getSimpleName(), iae);
        }
        return MalmoGoal.NULL_MALMO_GOAL;
    }

    public static MalmoGoal
    buildConsume(MalmoWorldObject item, int fromQty, int toQty) {
        try {
            StateVariable inv = INVENTORY_CONTAINS.copier()
                    .specializeArgs(item.argument()).build();
            Statement statement = new NumberTransitionStatement(inv, fromQty, toQty);
            return new InventoryContains(statement);
        } catch (IllegalArgumentException iae) {
            logger.error("{}: {}", InventoryContains.class.getSimpleName(), iae);
        }
        return MalmoGoal.NULL_MALMO_GOAL;
    }

}
