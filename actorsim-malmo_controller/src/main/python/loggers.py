from utils.LogManager import LogManager

malmo_root_logger = LogManager.get_logger('actorsim.malmo')
malmo_root_logger.setLevel(LogManager.DEBUG)

class LogLevels():
    #default
    default_level = LogManager.INFO

    #character-related loggers
    character_logger = default_level
    obs_logger = default_level
    calc_logger = default_level
    command_logger = default_level
    memory_logger = default_level
    inventory_logger = default_level

    #action / dbase loggers
    action_logger = default_level
    kqml_logger = default_level

    #planning loggers
    planner_logger = default_level
    jshop2_logger = default_level
    malmo_planner_logger = default_level
    bindings_logger = default_level
    trace_logger = default_level
    type_logger = default_level

    # detailed logging switches
    log_observation_blocks = False
    log_observation_entities = False
    log_mission_spec = False
    log_last_command = False

    def __init__(self, default_level = LogManager.INFO):
        self.default_level = default_level
        self.set_defaults()

    def set_defaults(self):
        #character-related loggers
        self.character_logger = self.default_level
        self.obs_logger = self.default_level
        self.calc_logger = self.default_level
        self.command_logger = self.default_level
        self.memory_logger = self.default_level
        self.inventory_logger = self.default_level

        #action / dbase loggers
        self.action_logger = self.default_level
        self.kqml_logger = self.default_level

        #planning loggers
        self.planner_logger = self.default_level
        self.jshop2_logger = self.default_level
        self.malmo_planner_logger = self.default_level
        self.bindings_logger = self.default_level
        self.trace_logger = self.default_level
        self.type_logger = self.default_level


class MalmoLoggers():
    """A convenience class for accessing the common loggers in the MalmoConnector."""

    malmo_root_logger = LogManager.get_logger('actorsim.malmo')

    #character-related loggers
    character_logger = LogManager.get_logger('actorsim.malmo.character')
    obs_logger = LogManager.get_logger('actorsim.malmo.character.obs')
    calc_logger = LogManager.get_logger('actorsim.malmo.character.calc')
    command_logger = LogManager.get_logger('actorsim.malmo.character.command')
    memory_logger = LogManager.get_logger('actorsim.malmo.character.memory')
    inventory_logger = LogManager.get_logger('actorsim.malmo.inventory')

    #action / dbase loggers
    action_logger = LogManager.get_logger('actorsim.malmo.action')
    kqml_logger = LogManager.get_logger("companions.kqml")

    #planning loggers
    planner_logger = LogManager.get_logger('actorsim.planner')
    jshop2_logger = LogManager.get_logger("actorsim.planner.jshop2")
    malmo_planner_logger = LogManager.get_logger("actorsim.malmo.planner")
    bindings_logger = LogManager.get_logger("actorsim.malmo.planner.bindings")
    trace_logger = LogManager.get_logger("actorsim.malmo.trace_manager")
    type_logger = LogManager.get_logger("actorsim.malmo.type_manager")

    # detailed logging switches
    log_observation_blocks = False
    log_observation_entities = False
    log_mission_spec = False
    log_last_command = False

    @staticmethod
    def set_all_non_root_loggers(level: int):
        MalmoLoggers.character_logger.setLevel(level)
        MalmoLoggers.obs_logger.setLevel(level)
        MalmoLoggers.calc_logger.setLevel(level)
        MalmoLoggers.command_logger.setLevel(level)
        MalmoLoggers.memory_logger.setLevel(level)
        MalmoLoggers.inventory_logger.setLevel(level)

        MalmoLoggers.action_logger.setLevel(level)
        MalmoLoggers.kqml_logger.setLevel(level)

        MalmoLoggers.planner_logger.setLevel(level)
        MalmoLoggers.jshop2_logger.setLevel(level)
        MalmoLoggers.malmo_planner_logger.setLevel(level)
        MalmoLoggers.bindings_logger.setLevel(level)
        MalmoLoggers.trace_logger.setLevel(level)
        MalmoLoggers.type_logger.setLevel(level)

    @staticmethod
    def set_log_levels(levels: LogLevels):
        MalmoLoggers.character_logger.setLevel(levels.character_logger)
        MalmoLoggers.obs_logger.setLevel(levels.obs_logger)
        MalmoLoggers.calc_logger.setLevel(levels.calc_logger)
        MalmoLoggers.command_logger.setLevel(levels.command_logger)
        MalmoLoggers.memory_logger.setLevel(levels.memory_logger)
        MalmoLoggers.inventory_logger.setLevel(levels.inventory_logger)

        MalmoLoggers.action_logger.setLevel(levels.action_logger)
        MalmoLoggers.kqml_logger.setLevel(levels.kqml_logger)

        MalmoLoggers.planner_logger.setLevel(levels.planner_logger)
        MalmoLoggers.jshop2_logger.setLevel(levels.jshop2_logger)
        MalmoLoggers.malmo_planner_logger.setLevel(levels.malmo_planner_logger)
        MalmoLoggers.bindings_logger.setLevel(levels.bindings_logger)
        MalmoLoggers.trace_logger.setLevel(levels.trace_logger)
        MalmoLoggers.type_logger.setLevel(levels.type_logger)
