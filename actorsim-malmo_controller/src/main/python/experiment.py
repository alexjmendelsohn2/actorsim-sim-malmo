from utils.LogManager import LogManager
from character.character import Character
from mission import Mission
from threading import Thread, Lock
import threading # for activeCount method
from experiment_enums import ExperimentCondition
import test_missions
import time

logger = LogManager.get_logger("experiment")


class Experiment():
    def __init__(self, experiment_id, mission: Mission):
        self.experiment_id = experiment_id
        self.mission = mission
        self.character = Character("Alex", mission)
        self.worker = Experiment.Worker(self)
        #self.worker.start()

    def start_exp(self):
        self.character.start_mission()

    class Worker(Thread):
        def __init__(self, parent: 'Experiment'):
            Thread.__init__(self)
            self.parent = parent
            self.name="exp_thread_{}".format(self.character.name)

        def run(self):
            self.parent.start_exp()


class NullExperiment(Experiment):
    def __init__(self):
        pass


class MultiAgentExperiment(Experiment):
    def __init__(self, experiment_id, mission: Mission, condition: ExperimentCondition):
        self.experiment_id = experiment_id
        self.mission = mission
        self.char_names = mission.character_names
        self.condition = condition
        self.companion_ports = [8950, 8900]
        self.malmo_ports = [10000, 10001]
        self.characters = self.init_characters(mission, experiment_id, self.char_names, self.companion_ports)
        # each character should have its own worker thread so missions can run simultaneously
        self.thread_pool = [MultiAgentExperiment.Worker(self, c) for c in self.characters]

    def start_exp(self):
        exp_mt = "(MinecraftExpMtFn {})".format(self.experiment_id)
        agent_type_fact = "(ist-Information {} (agentType {}))".format(exp_mt, self.condition)
        self.characters[0].kqmlUpdater.insert_data('interaction-manager', agent_type_fact)  # TODO: test that this works

        for thread in self.thread_pool:
            logger.debug('number of active threads is: {}'.format(threading.activeCount()))
            thread.start()
            ##time.sleep(10)
#
    def finish(self):
        # in theory, this should kill the threads when the mission is done, but who knows..
        #TODO: figure out why characters don't have kqml_updaters
        #TODO: figure out how to elegently kill connection to Companions
        for c in self.characters:
            if c.use_kqml_updater:
                c.kqml_updater.worker_thread.join()
        for thread in self.thread_pool:
            thread.join()


    def init_characters(self, mission: Mission, experiment_id, char_names, companion_ports=[8950, 8900]):
        characters = list()
        if len(char_names) > len(companion_ports):
            logger.error("Can't initialize characters. Not enough ports passed.")
        else:
            kqml_lock = Lock()
            if len(companion_ports) > len(char_names):
                logger.warn("More ports passed than characters. Only using first {} ports.".format(len(char_names)))
            for i in range(len(char_names)):
                name = char_names[i]
                port = companion_ports[i]
                character = Character(mission, name, port)
                character.role = i
                character.use_kqml_updater = True
                character.kqml_lock = kqml_lock
                character.experimentId = experiment_id
                #TODO use a factory here to add goal priorities
                #self.add_goal_priorities(character)
                characters.append(character)
        return characters

    class Worker(Thread):
        def __init__(self, parent: 'MultiAgentExperiment', character: Character):
            Thread.__init__(self)
            self.parent = parent
            self.malmo_ports = parent.malmo_ports
            self.character = character
            old_name = self.getName()
            self.name = "multi_exp_thread_{}_{}".format(self.character.name, old_name)


        def run(self):
            # TODO: more here?
            self.character.start_mission(self.malmo_ports)
