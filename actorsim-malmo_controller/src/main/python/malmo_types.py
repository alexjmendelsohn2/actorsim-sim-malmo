
from loggers import MalmoLoggers

logger = MalmoLoggers.type_logger


class MalmoTypeManager:
    BLOCK_SET = {"stone", "grass", "dirt", "cobblestone", "planks", "sapling", "bedrock",
                 "flowing_water", "water", "flowing_lava", "lava", "sand", "gravel", "gold_ore",
                 "iron_ore", "coal_ore", "log", "leaves", "sponge", "glass", "lapis_ore", "lapis_block",
                 "dispenser", "sandstone", "noteblock", "bed", "golden_rail", "detector_rail",
                 "sticky_piston", "web", "tallgrass", "deadbush", "piston", "piston_head", "wool",
                 "piston_extension", "yellow_flower", "red_flower", "brown_mushroom", "red_mushroom",
                 "gold_block", "iron_block", "double_stone_slab", "stone_slab", "brick_block", "tnt",
                 "bookshelf", "mossy_cobblestone", "obsidian", "torch", "fire", "mob_spawner",
                 "oak_stairs", "chest", "redstone_wire", "diamond_ore", "diamond_block", "crafting_table",
                 "wheat", "farmland", "furnace", "lit_furnace", "standing_sign", "wooden_door", "ladder",
                 "rail", "stone_stairs", "wall_sign", "lever", "stone_pressure_plate", "iron_door",
                 "wooden_pressure_plate", "redstone_ore", "lit_redstone_ore", "unlit_redstone_torch",
                 "redstone_torch", "stone_button", "snow_layer", "ice", "snow", "cactus", "clay", "reeds",
                 "jukebox", "fence", "pumpkin", "netherrack", "soul_sand", "glowstone", "portal",
                 "lit_pumpkin", "cake", "unpowered_repeater", "powered_repeater", "stained_glass",
                 "trapdoor", "monster_egg", "stonebrick", "brown_mushroom_block", "red_mushroom_block",
                 "iron_bars", "glass_pane", "melon_block", "pumpkin_stem", "melon_stem", "vine", "fence_gate",
                 "brick_stairs", "stone_brick_stairs", "mycelium", "waterlily", "nether_brick",
                 "nether_brick_fence", "nether_brick_stairs", "nether_wart", "enchanting_table",
                 "brewing_stand", "cauldron", "end_portal", "end_portal_frame", "end_stone", "dragon_egg",
                 "redstone_lamp", "lit_redstone_lamp", "double_wooden_slab", "wooden_slab", "cocoa",
                 "sandstone_stairs", "emerald_ore", "ender_chest", "tripwire_hook", "tripwire", "emerald_block",
                 "spruce_stairs", "birch_stairs", "jungle_stairs", "command_block", "beacon",
                 "cobblestone_wall", "flower_pot", "carrots", "potatoes", "wooden_button", "skull",
                 "anvil", "trapped_chest", "light_weighted_pressure_plate", "heavy_weighted_pressure_plate",
                 "unpowered_comparator", "powered_comparator", "daylight_detector", "redstone_block",
                 "quartz_ore", "hopper", "quartz_block", "quartz_stairs", "activator_rail", "dropper",
                 "stained_hardened_clay", "stained_glass_pane", "leaves2", "log2", "acacia_stairs",
                 "dark_oak_stairs", "slime", "barrier", "iron_trapdoor", "prismarine", "sea_lantern",
                 "hay_block", "carpet", "hardened_clay", "coal_block", "packed_ice", "double_plant",
                 "standing_banner", "wall_banner", "daylight_detector_inverted", "red_sandstone",
                 "red_sandstone_stairs", "double_stone_slab2", "stone_slab2", "spruce_fence_gate",
                 "birch_fence_gate", "jungle_fence_gate", "dark_oak_fence_gate", "acacia_fence_gate",
                 "spruce_fence", "birch_fence", "jungle_fence", "dark_oak_fence", "acacia_fence",
                 "spruce_door", "birch_door", "jungle_door", "acacia_door", "dark_oak_door", "end_rod",
                 "chorus_plant", "chorus_flower", "purpur_block", "purpur_pillar", "purpur_stairs",
                 "purpur_double_slab", "purpur_slab", "end_bricks", "beetroots", "grass_path",
                 "end_gateway", "repeating_command_block", "chain_command_block", "frosted_ice", "magma",
                 "nether_wart_block", "red_nether_brick", "bone_block", "structure_void", "observer",
                 "white_shulker_box", "orange_shulker_box", "magenta_shulker_box", "light_blue_shulker_box",
                 "yellow_shulker_box", "lime_shulker_box", "pink_shulker_box", "gray_shulker_box",
                 "silver_shulker_box", "cyan_shulker_box", "purple_shulker_box", "blue_shulker_box",
                 "brown_shulker_box", "green_shulker_box", "red_shulker_box", "black_shulker_box", "structure_block"
                 }

    UNKNOWN_TYPE = "UKNOWN"

    ITEMS_WITH_TYPE_IN_NAME = ["hoe", "sword", "seeds", "axe"]

    ITEMS_WITH_NAME_AS_TYPE = ["hoe", "pumpkin", "sugar", "egg", "milk_bucket", "beef", "leather",
                               "pumpkin_pie", "dirt", "farmland", "bread",
                               "water", "torch", "cow", "feather",
                               "chicken", "potato", "wheat", "carrot", "beetroot",
                               "poisonous_potato", "cake", "shovel", "reed", "bone_meal", "chicken_meat"
                               ]

    TOOL_TYPES = ["tool", "hoe", "shovel", "pickaxe", "axe"]
    WEAPON_TYPES = ["weapon", "sword", "axe"]
    FOOD_TYPES = ["food", "cake", "pumpkin_pie", "carrot", "potato", "beetroot", "beef", "chicken_meat", "bread"]
    ANIMAL_TYPES = ["animal", "cow", "chicken"]
    NON_STEM_SEEDS = ["non_stem_seeds", "wheat_seeds", "potato", "reed", "carrot", "beetroot_seeds"]
    STEM_SEEDS = ["stem_seeds", "pumpkin_seeds", "melon_seeds"]
    SEEDS = ["seeds", *(NON_STEM_SEEDS[1:]), *(STEM_SEEDS[1:])]
    DROP_TYPES = ["drop", *(SEEDS[1:]), "egg", "feather", "chicken_meat", "beef", "leather"]
    CROP_TYPES = ["crop", "carrot", "potato", "beetroot", "wheat", "reed", "pumpkin"]
    INGREDIENTS = ["ingredient", "sugar", "milk_bucket", "bone_meal"]
    BLOCKS = ["block", "dirt", "cobblestone"]
    HOSTILE_MOB = ["hostile", "creeper", "zombie", "skeleton"]
    MOB_TYPES = ["mob", *(ANIMAL_TYPES[1:]), *HOSTILE_MOB]

    ITEM_TYPES = ["item", TOOL_TYPES[0], WEAPON_TYPES[0], FOOD_TYPES[0],
                  DROP_TYPES[0], CROP_TYPES[0], INGREDIENTS[0], BLOCKS[0], SEEDS[0]]
    ENTITY_TYPES = ["entity", "mob", "animal", "drop"]

    TYPE_LISTS = \
    [ITEM_TYPES,
     ENTITY_TYPES,
     HOSTILE_MOB,
     TOOL_TYPES,
     FOOD_TYPES,
     ANIMAL_TYPES,
     WEAPON_TYPES,
     DROP_TYPES,
     CROP_TYPES,
     SEEDS,
     NON_STEM_SEEDS,
     STEM_SEEDS,
     INGREDIENTS,
     BLOCKS,
     HOSTILE_MOB,
     MOB_TYPES,
     ]

    PLANTABLE = ["plantable", "farmland"]

    PLACEHOLDER_TYPES = [PLANTABLE]


    DROPS = [["beef", "cow"],
             ["leather", "cow"]
            ]


    @staticmethod
    def get_types(mc_object: str):
        """Returns a list of types if the mc_object has a vaild type.
           The list starts with the main type and includes other known sub types.

           If type is not known, returns MalmoTypeManager.UNKNOWN_TYPE ."""
        main_type = MalmoTypeManager.UNKNOWN_TYPE

        name = mc_object.lower()

        if name in MalmoTypeManager.ITEMS_WITH_NAME_AS_TYPE:
            main_type = name
        elif name == "reeds":
            main_type = "reed"

        if main_type == MalmoTypeManager.UNKNOWN_TYPE:
            for type in MalmoTypeManager.ITEMS_WITH_TYPE_IN_NAME:
                if name.find(type) >= 0:
                    main_type = type
                    break

        return_type = main_type

        types = set()
        types.add(main_type)
        for type_list in MalmoTypeManager.TYPE_LISTS:
            if name in type_list:
                types.add(type_list[0])
            if main_type in type_list:
                types.add(type_list[0])

        return_type = types

        if MalmoTypeManager.UNKNOWN_TYPE in return_type:
            return_type.remove(MalmoTypeManager.UNKNOWN_TYPE)

        if not isinstance(return_type, str):
            if len(return_type) == 0:
                return_type = MalmoTypeManager.UNKNOWN_TYPE

        return return_type


    HARVESTS_TO = ["grows_from",
                   ("wheat", "wheat_seeds"),
                   ("potato", "potato"),
                   ("pumpkin", "pumpkin_seeds"),
                   ("reeds", "reeds"),
                   ("carrot", "carrot"),
                   ("beetroot", "beetroot_seeds")
                   ]

    FOUND_NEAR = ["found_near",
                   ("egg", "chicken"),
                   ("milk_bucket", "cow")
                   ]

    @staticmethod
    def get_axiom_lists():
        return (MalmoTypeManager.HARVESTS_TO, MalmoTypeManager.FOUND_NEAR)

    @staticmethod
    def get_axioms():
        axiom_strs = list()
        for axiom_list in MalmoTypeManager.get_axiom_lists():
            predicate = axiom_list[0]
            for var_list in axiom_list[1:]:
                axiom_str = "({} {} {})".format(predicate, var_list[0], var_list[1])
                axiom_strs.append(axiom_str)
        return axiom_strs

    @staticmethod
    def get_placeholders():
        placeholder_strs = list()
        placeholder_prefix = "placeholder_"
        included_placeholders = set()
        for type_list in MalmoTypeManager.TYPE_LISTS:
            type = type_list[0]
            for item in type_list[1:]:
                if item not in included_placeholders:
                    included_placeholders.add(item)
                    placeholder_name = placeholder_prefix + item
                    placeholder_str = "(placeholder {})".format(placeholder_name)
                    placeholder_strs.append(placeholder_str)

                    types = MalmoTypeManager.get_types(item)
                    if types != MalmoTypeManager.UNKNOWN_TYPE:
                        for type in types:
                            type_str = ("(type_{} {})".format(type, placeholder_name))
                            placeholder_strs.append(type_str)

                        for axiom_list in MalmoTypeManager.get_axiom_lists():
                            predicate = axiom_list[0]
                            for var_list in axiom_list[1:]:
                                arg1 = var_list[0]
                                arg2 = var_list[1]
                                if item == arg1:
                                    axiom_str = "({} {} {})".format(predicate, placeholder_name, arg2)
                                    placeholder_strs.append(axiom_str)
                                    placeholder_arg2 = placeholder_prefix + arg2
                                    axiom_str = "({} {} {})".format(predicate, placeholder_name, placeholder_arg2)
                                    placeholder_strs.append(axiom_str)

                                if item == arg2:
                                    axiom_str = "({} {} {})".format(predicate, arg1, placeholder_name)
                                    placeholder_strs.append(axiom_str)
                    else:
                        logger.warning("Unknown type for item {}".format(item))

        for special_type_list in MalmoTypeManager.PLACEHOLDER_TYPES:
            attribute = special_type_list[0]
            for item in special_type_list[1:]:
                if item not in included_placeholders:
                    included_placeholders.add(item)
                    placeholder_name = placeholder_prefix + item
                    placeholder_str = "(placeholder {})".format(placeholder_name)
                    placeholder_strs.append(placeholder_str)

                    placeholder_str = "({} {})".format(attribute, placeholder_name)
                    placeholder_strs.append(placeholder_str)

        return placeholder_strs


    REAL_NAMES = \
        {"dye": "bone_meal",
         "Chicken": "chicken",
         "chicken": "chicken_meat",
         "Cow": "cow"}
    @staticmethod
    def get_real_name(name):
        """Passes through the name of an item unless replaced by a dictionary entry in REAL_NAMES."""
        real_name = name
        if name in MalmoTypeManager.REAL_NAMES:
            real_name = MalmoTypeManager.REAL_NAMES[name]
        return real_name


    @staticmethod
    def get_meat_type(animal_type: str):
        meat_type = animal_type.lower()
        if animal_type.lower() == "cow":
            meat_type = "beef"
        if animal_type.lower() == "chicken":
            meat_type = "chicken_meat"
        return meat_type

    @staticmethod
    def dump_java(output_filename: str, package: str = "nrl.actorsim.malmo",  class_name: str = "MalmoTypes"):
        with open(output_filename, mode="w") as out:
            out.write("//---------------------------------------------------------------------------------------------------------\n")
            out.write("//---------------------------------------------------------------------------------------------------------\n")
            out.write("//   DO NOT EDIT - this file is automatically generated by the python interop malmo_types.py::dump_java()\n")
            out.write("//---------------------------------------------------------------------------------------------------------\n")
            out.write("//---------------------------------------------------------------------------------------------------------\n")
            out.write("package {};\n".format(package))
            out.write("\n")
            out.write("public class {} {}\n".format(class_name, "{"))

            included_items = set()
            for type in ["item", "entity"]:
                types = MalmoTypeManager.get_types(type)
                if types == MalmoTypeManager.UNKNOWN_TYPE:
                    MalmoTypeManager.write_type(included_items, type, out)
            out.write("\n")
            out.write("\n")

            mc_objects = set()
            for type_list in MalmoTypeManager.TYPE_LISTS:
                for mc_object in type_list[1:]:  # type: str
                    mc_objects.add(mc_object)
            for mc_object in MalmoTypeManager.ITEMS_WITH_NAME_AS_TYPE:
                mc_objects.add(mc_object)
            for mc_object in MalmoTypeManager.ITEMS_WITH_TYPE_IN_NAME:
                mc_objects.add(mc_object)

            for mc_object in sorted(mc_objects):
                MalmoTypeManager.write_type(included_items, mc_object, out)

            out.write("} // class\n")

    @staticmethod
    def write_type(included_items, item, out):
        if item not in included_items:
            included_items.add(item)
            types = MalmoTypeManager.get_types(item)
            types_string = '\"Object\"'
            if types != MalmoTypeManager.UNKNOWN_TYPE:
                if item in types:
                    types.remove(item)
                types_string = '\"' + '\", \"'.join(map(str, types)) + '\"'

            out.write("    public static MalmoWorldObject {} = new MalmoWorldObject(\"{}\",{});\n".format(item.upper(), item, types_string))

