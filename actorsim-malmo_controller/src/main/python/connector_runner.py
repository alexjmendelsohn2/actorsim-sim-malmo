
from utils.LogManager import LogManager
from character.character import Character

import test_missions
from loggers import MalmoLoggers

malmo_root_logger = LogManager.get_logger('actorsim.malmo')
character_logger = LogManager.get_logger('actorsim.malmo.character')
obs_logger = LogManager.get_logger('actorsim.malmo.character.obs')
calc_logger = LogManager.get_logger('actorsim.malmo.character.calc')
command_logger = LogManager.get_logger('actorsim.malmo.character.command')
action_logger = LogManager.get_logger('actorsim.malmo.action')
kqml_logger = LogManager.get_logger("companions.kqml")
plan_logger = LogManager.get_logger("actorsim.malmo.planner")


def main():
    malmo_root_logger.setLevel(LogManager.DEBUG)

    mission_name = "plant_wheat"
    #mission_name = "kill_chicken"
    #mission_name = "kill_cow"
    #mission_name = "slaughter_chicken"
    #mission_name = "explore"
    #mission_name = "static_multi_agent_mission"
    mission = test_missions.generate_mission(mission_name)
    character1 = Character(mission)
    character1.role = 0
    character1.grid_names = ["blocks3x3"]

    #test_missions.get_mission_goals(mission_name, character)

    ports = [10000, 10001]
    character1.start_mission(ports)
    MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
    MalmoLoggers.log_observation_entities = False
    MalmoLoggers.kqml_logger.setLevel(LogManager.DEBUG)

main()
