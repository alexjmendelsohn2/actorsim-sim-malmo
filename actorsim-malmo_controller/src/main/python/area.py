
class Area:
    """Defines an area box."""

    def __init__(self, min_x=0, max_x=50, min_z=0, max_z=50):
        self.min_x = min_x
        self.max_x = max_x
        self.min_z = min_z
        self.max_z = max_z

    def get_perimeter_size(self):
        x_size = self.max_x - self.min_x
        z_size = self.max_z - self.min_z
        size = (2 * x_size) + (2 * z_size)
        return size
