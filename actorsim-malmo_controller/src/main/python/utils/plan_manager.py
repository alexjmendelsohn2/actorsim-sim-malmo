import collections
import datetime
import traceback

from utils.LogManager import LogManager
from action import SKIP_ACTION, NULL_ACTION, ActionBase
from character.action_trace import ActionTrace
from character.goal_manager import trace_logger, GoalManager, logger, bindings_logger
from character.memory import MemoryObject
from character.state import CharacterState
from container_classes import ItemStack
from location import BlockLocation, EntityLocation, MobLocation
from loggers import MalmoLoggers
from malmo_types import MalmoTypeManager
from utils.plan_utils import DomainDescription, ProblemDescription
from utils.plan_state import VariableBinding
from utils.printing_helpers import debug_print_args

logger = MalmoLoggers.planner_logger


class PlanManagerOptions():
    def __init__(self, planner_name="JSHOP2"):
        self.planner_name = planner_name
        self.domain_file = None
        self.domain_description = DomainDescription()

class PlanManager():
    """A connector for linking to the JSHOP planner."""
    def __init__(self, options: 'PlanManagerOptions' = PlanManagerOptions()):
        logger.debug("Initializing {}".format(options.planner_name))
        self.options = options
        self.shop_interface = None
        if self.options.planner_name == "JSHOP2":
            from jshop2_connector import JShop2Connector
            logger.debug("Initializing JSHOP2 interface...")
            self.shop_interface = JShop2Connector()
            logger.debug("Finished")



    def plan(self, problem: ProblemDescription):
        start_time = datetime.datetime.now()
        logger.debug("Executing planner")
        problem.result.planner_output = self.shop_interface.execute(problem) # type: str
        self.attempt_conversion_to_plan(problem)
        end_time = datetime.datetime.now()
        delta_time = end_time - start_time
        logger.debug("Planning episode took {:.3f} seconds".format(delta_time.total_seconds()))

    def attempt_conversion_to_plan(self, problem: ProblemDescription):
        logger.debug("Attempting to convert plan")
        output = problem.result.planner_output
        if output.startswith("(!"):
            last_index = output.rfind(")")  # find end of last step
            cleaned = output[:last_index]
            steps = cleaned.split(",")
            for i in range(len(steps)):
                step = steps[i]
                step = step.replace("(", "").replace(")", "").replace("!", "")
                step = step.strip()
                steps[i] = step
            problem.result.plan = steps
            problem.result.success = True
            logger.debug("Plan conversion successful")



SKIP_ACTION_MAP = {
    # ordered by the domain file
    'directive_complete_in_sequence',
    'directive_end_sequence',
    'consume_inventory',
    'increment_item_in_inventory',
    'add_new_item_to_inventory',
    'have_item',
    'fail',
    'fail_with_message',
    'success',
    'success_with_message',
    'completed_for',
    'remove_unopened'
}

from actions import ExploreFor, Craft, MonitorNear, CollectDrop, SwapWithChest, CollectFromLocation, SelectItem, \
    MoveNearTarget, MoveNearEntity, MoveNearBlockType, LookAtBlockType, LookAtStaticTarget, LookAtStem, LookAtEntity, \
    Attack, BreakUsingLook, Use, Pause, PauseUntilEntityUpdate, PauseUntilEntityDead

PRIMITIVE_NAME_TO_ACTION_MAP = {
    # ordered by the domain file
    'explore_for': ExploreFor,
    'craft_item': Craft,
    'monitor_near': MonitorNear,
    'gather': CollectDrop,
    'swap_one_from_chest': SwapWithChest,
    'collect_drop': CollectDrop,
    'collect_from_location': CollectFromLocation,
    'select': SelectItem,
    'move_near_target': MoveNearTarget,
    'move_near_entity': MoveNearEntity,
    'move_near_block_type': MoveNearBlockType,
    'look_at_block_type': LookAtBlockType,
    'look_at_crop': LookAtStaticTarget,
    'look_at_stem_crop': LookAtStem,
    'look_at_entity': LookAtEntity,
    'attack': Attack,
    'harvest': Attack,
    'break_using_look': BreakUsingLook,
    'use': Use,
    'pause': Pause,
    'pause_until_entity_update': PauseUntilEntityUpdate,
    'pause_until_entity_dead': PauseUntilEntityDead
    }

def determine_primitive_from_action(trace: ActionTrace, action: ActionBase):
    if hasattr(action, 'primitive_name'):
        trace_logger.debug("The action was created from a primitive")
        trace.primitive_action_name = action.primitive_name
        if hasattr(action, 'primitive_params'):
            trace.primitive_action_params = action.primitive_params
        if hasattr(action, 'primitive_plan_step'):
            trace.primitive_plan_step = action.primitive_plan_step
    elif not trace.action_classname == "":
        action_class_name = trace.action_classname
        for k, v in PRIMITIVE_NAME_TO_ACTION_MAP.items():
            params = list()
            if isinstance(v, collections.Sequence):
                object = v[0]
                value_classname = object.__name__
                params = v[1]
            else:
                value_classname = v.__name__
            trace_logger.trace("Checking v:{!r} name:{!r}".format(value_classname, action_class_name))
            if value_classname == action_class_name:
                params_match = True
                if isinstance(action, Use):
                    trace_logger.debug("  checking params:{}".format(params))
                    if action.item_to_use != params[0]:
                        params_match = False
                if params_match:
                    trace.primitive_action_name = k
                    trace.primitive_action_params = action.name
                    break

def create_action_from_primitive(character: 'Character', state_index: float, problem: ProblemDescription, plan_step: str):
    logger.trace("Creating action for plan step '{}'".format(plan_step))
    step_tokens = plan_step.split()
    primitive = step_tokens[0]
    params = list()

    if primitive in SKIP_ACTION_MAP:
        logger.debug("Primitive {} declared a skip action, so skipping".format(primitive))
        return SKIP_ACTION

    if primitive not in PRIMITIVE_NAME_TO_ACTION_MAP:
        message = "Could not find a primitive for the plan step '{}'".format(plan_step)
        logger.error(message)
        raise Exception(message)

    if len(step_tokens) > 1:
        num_params = len(step_tokens) - 1
        plan_state = character.get_plan_state(state_index)

        bindings = plan_state.variable_bindings  # type: List[VariableBinding]
        for token in step_tokens[1:]:
            if token in bindings:
                binding = bindings[token]
                if binding.ignore_when_converting_to_actions:
                    logger.trace("Skipping binding {} because ignore_when_converting_to_actions is true".format(binding))
                    continue

                param_object = binding.variable_object

                if isinstance(param_object, ItemStack):
                    item_stack = binding.variable_object
                    param_object = item_stack.name

                params.append(param_object)
            else:
                clean_token = token.replace("placeholder_", "").replace("crafted_", "")
                logger.trace("Token {} was not found in bindings; passing it directly through as {}".format(token, clean_token))
                params.append(clean_token)

    action = NULL_ACTION
    action_list = PRIMITIVE_NAME_TO_ACTION_MAP[primitive]
    if isinstance(action_list, (list, tuple)):
        action_ref = action_list[0]
        params.insert(0, action_list[1])
    else:
        action_ref = action_list
    logger.trace("Attempting to create action for '{}' with params: {}".format(plan_step, params))
    try:
        if logger.getEffectiveLevel() == LogManager.TRACE:
            method = debug_print_args
            method(logger, LogManager.TRACE, character, *params)
        action = action_ref(character, *params)
        action.primitive_name = primitive
        action.primitive_params = params
        action.primitive_plan_step = plan_step
        logger.debug("Action created for step:'{}' class:{} type:{} params:{}".format(plan_step, action.__class__.__name__, type(action), params))
    except Exception as e:
        logger.error("For converting primitve {} from plan plan_step:'{}'".format(primitive, plan_step))
        logger.error("exception occurred:{}".format(e))
        logger.error("{}".format(traceback.format_exc()))


    return action


def create_shop_str(problem: ProblemDescription):
    state_index = problem.state_index
    plan_state = problem.character.get_plan_state(state_index)

    predicates = plan_state.get_sorted_predicates()
    plan_list = list()
    plan_list.append("(defproblem problem minecraft")
    plan_list.append("  ;; types and init")
    plan_list.append("  (")
    plan_list.append("")

    plan_list.append("    ;;init sorted by distance from character")
    for predicate_tuple in predicates:
        predicate = predicate_tuple[0]
        distance = predicate_tuple[1]
        distance_str = ""
        if distance >= 0:
            distance_str = " ;; distance={:0.3f}".format(distance)
        if not predicate.startswith("(type"):
            plan_list.append("    {}{}".format(predicate, distance_str))
    plan_list.append("")

    plan_list.append("    ;;Malmo axioms")
    axiom_list = MalmoTypeManager.get_axioms()
    for axiom in axiom_list:
        plan_list.append("    {}".format(axiom))
    plan_list.append("")

    plan_list.append("    ;;Malmo types")
    for predicate_tuple in predicates:  # type: str
        predicate = predicate_tuple[0]
        if predicate.startswith("(type"):
            plan_list.append("    {}".format(predicate))
    plan_list.append("")

    plan_list.append("    ;;placeholders")
    placeholder_list = MalmoTypeManager.get_placeholders()
    for placeholder_predicate in placeholder_list:
        plan_list.append("    {}".format(placeholder_predicate))
    plan_list.append("")

    plan_list.append("    ;;Malmo type hierarchy")
    for type_list in MalmoTypeManager.TYPE_LISTS:
        base_type = type_list[0]
        type_list_str = ""
        for type in type_list[1:]:
            type_list_str += " {}".format(type)
        plan_list.append("    ;;(type_hierarchy_{}{})".format(base_type, type_list_str))
    plan_list.append("  ) ;types and init")
    plan_list.append("")

    plan_list.append("  ;;Goal statement")
    goal_str = "("
    sep = ""
    for token in problem.task_to_decompose:
        goal_str += "{}{}".format(sep, token)
        sep = " "
    goal_str += ")"
    plan_list.append("  {}".format(goal_str))
    plan_list.append(")")
    problem.shop_lines = plan_list

