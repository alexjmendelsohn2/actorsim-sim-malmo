
def debug_print_args(logger: 'LogManager', level, *args):
    """Prints out the method args to the logger.
    """
    logger.log(level, "There are {} args:".format(len(args)))
    for arg in args:
        logger.debug("  {}".format(arg))