import datetime
import threading

from utils.LogManager import LogManager
from malmo_types import MalmoTypeManager
from utils.plan_utils import DomainDescription, PlanRecProblemDescription

logger = LogManager.get_logger('actorsim.plan_recog')

class PlanRecognizerOptions:
    def __init__(self, domain_file=None, bootstrap_problem_files=None, plan_rec_name="PANDA-PR"):
        self.plan_rec_name = plan_rec_name
        if domain_file is None:
            self.domain_file = ""
        if bootstrap_problem_files is None:
            self.bootstrap_problem_files = []


class PlanRecognizer:
    def __init__(self, options=PlanRecognizerOptions()):
        self.__next_thread_id = 0
        self.__instance_id = 0
        self.__thread_id_to_thread = dict()
        self.thread_id_to_returned_data = dict()

        self.options = options
        self.__plan_rec_interface = None
        if self.options.plan_rec_name == "PANDA-PR":
            from plan_recognition_interface import PlanRecognitionInterface
            logger.debug("Initializing Plan Recognition interface... (PANDA-PR)")
            if len(options.domain_file) == 0:
                raise FileNotFoundError("You did not provide a domain file for plan recognition? Please provide this file")
            if len(options.bootstrap_problem_files) == 0:
                raise FileNotFoundError("No bootstrap problem files detected. Cannot convert from SHOP2 to HDDL without this...")
            self.__plan_rec_interface = PlanRecognitionInterface(self.options.domain_file, self.options.bootstrap_problem_files)
            logger.debug("Finished")

    def spawn_thread(self, plan_rec_description: PlanRecProblemDescription):
        logger.debug("Spawning thread for Plan Recognition!")
        thread_id = self.__next_thread_id
        self.__next_thread_id += 1
        self.__instance_id += 1
        t = threading.Thread(name=thread_id, target=self.__recognize_multithreaded(plan_rec_description))
        self.__thread_id_to_thread[thread_id] = t
        t.start()
        return thread_id

    def wait_for_thread(self, thread_id):
        """
            Wait for thread corresponding to thread_id to complete execution. NOTE: It is up to the caller of this class to maintain a list of threads.
        """
        if thread_id not in self.__thread_id_to_thread:
            raise KeyError("Thread id {0} not found!".format(thread_id))
        self.__thread_id_to_thread[thread_id].join()

    def __recognize_multithreaded(self, problem: PlanRecProblemDescription):
        start_time = datetime.datetime.now()
        problem.goals_recognized = self.plan_recognition_interface.recognize(problem)
        end_time = datetime.datetime.now()
        delta_time = end_time - start_time
        logger.debug("Plan Recognition episode took {:.3f} seconds".format(delta_time.total_seconds()))

    def recognize(self, problem: PlanRecProblemDescription):
        start_time = datetime.datetime.now()
        problem.goals_recognized = self.plan_recognition_interface.recognize(problem)
        end_time = datetime.datetime.now()
        delta_time = end_time - start_time
        logger.debug("Plan Recognition episode took {:.3f} seconds".format(delta_time.total_seconds()))