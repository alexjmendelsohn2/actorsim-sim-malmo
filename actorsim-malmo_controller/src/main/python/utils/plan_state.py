import traceback
from operator import itemgetter

from character.state import CharacterState
from container_classes import NULL_BLOCK_INVENTORY
from location import EntityLocation, BlockLocation, MobLocation
from loggers import MalmoLoggers
from malmo_types import MalmoTypeManager

logger = MalmoLoggers.planner_logger
bindings_logger = MalmoLoggers.bindings_logger


class VariableBinding:
    def __init__(self, name, object = None, ignore_when_converting_to_actions = False):
        self.name = name
        self.variable_object = object
        self.ignore_when_converting_to_actions = ignore_when_converting_to_actions

    def toJSON(self):
        class_name = type(self.variable_object).__name__
        object_encoding = self.variable_object
        if hasattr(self.variable_object, "toJSON"):
            object_encoding = self.variable_object.toJSON()
        return dict(name=self.name, class_name=class_name, variable_object=object_encoding)


class PlanState:
    """ Collects details about the world state into a set of predicates and bindings

        Members:
          state_index:
          predicates: a set of predicates representing the world state
          variable_bindings:  variable -> object bindings; should match predicates
    """
    blocks_to_report = {
        "pumpkin",
        "wood",
        "farmland",
        "wheat",
        "glass",
        "torch",
        "chest"
    }

    def __init__(self, character: 'Character', state_index: float):
        logger.debug("Creating plan_state for state_index:{}".format(state_index))
        self.state_index = state_index

        if character != None:
            self._predicates = list()  # type: List[str]
            self.variable_bindings = dict()  # type: Dict[str, VariableBinding]

            state = character.get_state(state_index)
            self.add_character_predicates(character, state)
            self.add_chest_predicates(character)
            self.add_inventory_predicates(character, state)
            self.add_observed_predicates(character, state)

            character.register_new_plan_state(state, self)

    def toJSON(self):
        try:
            d = dict()
            predicates = list()
            for predicate, distance in self._predicates:
               predicates.append(predicate)
            d["predicates"] = predicates
            bindings_dict = dict()
            for name, binding in self.variable_bindings.items():
                if hasattr(binding, "toJSON"):
                    bindings_dict[name] = binding.toJSON()
                else:
                    logger.warning("missing a toJSON() method for object {}".format(binding))
                    bindings_dict[name] = binding
            d["bindings"] = bindings_dict
        except Exception as e:
            logger.error("{}: exception occurred:{}".format(self.name, e))
            logger.error("{}: {}".format(self.name, traceback.format_exc()))

        return d

    def add_predicate(self, predicate: str, distance: float = -1):
        logger.trace("Adding predicate {}".format(predicate))
        self._predicates.append((predicate, distance))

    def get_sorted_predicates(self):
        name_sort = sorted(self._predicates, key=itemgetter(0))
        distance_sort = sorted(name_sort, key=itemgetter(1))
        return distance_sort

    def add_character_predicates(self, character: 'Character', state: CharacterState):
        bindings = self.variable_bindings
        character_location = state.location
        character_name = character.name.lower()
        location_var_name = state.CURRENT_LOCATION_NAME.lower()
        current_loc = VariableBinding(location_var_name, character_location, ignore_when_converting_to_actions=True)
        bindings[location_var_name] = current_loc
        bindings_logger.debug("Added {} at {} ".format(location_var_name, current_loc))
        self.add_predicate("(type_player {})".format(character_name))
        self.add_predicate("(entity_at {} {})".format(character_name, location_var_name))
        self.add_predicate("(type_location {})".format(location_var_name))
        # TODO change this to use the look vector
        self.add_predicate("(looking_at {})".format(location_var_name))
        return character_name

    def add_inventory_predicates(self, character: 'Character', state: CharacterState):
        bindings = self.variable_bindings
        inventory = state.inventory
        for stack_index in range(inventory.inventory_size()):  # type: ItemStack
            item_stack = inventory.stack_at(stack_index)
            location_name = item_stack.name
            index = item_stack.index
            if location_name == "air":
                self.add_predicate("(inventory_open_index {:0>2})".format(index))
            else:
                bindings[location_name] = VariableBinding(location_name, item_stack)
                self.add_predicate("(inventory_count {} {} {})".format(index, location_name, item_stack.count))
                self.add_type_predicates(location_name)

                if item_stack.index < inventory.hot_bar_size:
                    self.add_predicate("(inventory_in_hotbar {})".format(item_stack.name))

        selected_index = character.get_selected_hotbar_slot()
        selected_stack = inventory.stack_at(selected_index) # type: ItemStack
        self.add_predicate("(inventory_selected_item {})".format(selected_stack.name))
        self.add_predicate("(inventory_selected_index {})".format(selected_index))

    def add_chest_predicates(self, character: 'Character'):
        bindings = self.variable_bindings
        chests = character.get_observed_chests()

        for (chest_location, chest_inventory) in chests.items():
            #TODO HERE I WANT SOME CHECK TO MAKE SURE THAT I HAVE THE CHEST INVENTORY;
            # I think this needs to be if null container_classes.py


            self.add_predicate("(type_container chest_{})".format(chest_location))
            for stack_index in range(chest_inventory.inventory_size()):
                item_stack = chest_inventory.stack_at(stack_index)
                item_name = item_stack.name
                item_index = item_stack.index
                item_quantity = item_stack.count
                if item_name == "air":
                    self.add_predicate("(chest_open_index chest_{} {})".format(chest_location, item_index))
                else:
                    type_name = "type_{}".format(item_name)
                    self.add_predicate("({} {})".format(type_name, item_name))
                    bindings[item_name] = VariableBinding(item_name, item_stack)
                    self.add_predicate("(contains chest_{} {} {} {})".format(chest_location, item_index,
                                                                          item_name, item_quantity))
                    bindings_logger.debug(
                        "Added precondition for chest {} to contain {}".format(chest_location, item_name))

    def add_observed_predicates(self, character: 'Character', state: CharacterState):
        character.visit_observed_objects(self.observed_visit_func, [character, state])

    def observed_visit_func(self, character: 'Character', state: CharacterState, observed_object: 'MemoryObject'):
        if not observed_object.is_valid_observation:
            logger.trace("object {} is no longer observed, not including in plan state".format(observed_object))
            return

        location = observed_object.location
        if isinstance(location, BlockLocation):
            self.add_block_predicates(character, state, location)
        elif isinstance(location, EntityLocation):
            self.add_entity_predicates(state, location)
        else:
            logger.error("unknown location type for {}".format(location))

    def add_entity_predicates(self, state: CharacterState, location: EntityLocation):
        entity_type = location.mc_type
        entity_name = entity_type
        location_name = "{}_{}".format(entity_type, location.id)
        if isinstance(location, MobLocation):
            entity_name = location.get_short_entity_name()
            location_name = entity_name
            logger.trace("for entity {}, using unique name {} and location {}".format(entity_type, entity_name, location_name))

        binding = VariableBinding(location_name, location)
        self.variable_bindings[binding.name] = binding
        bindings_logger.debug("Added {} at location {}".format(location_name, location))

        self.add_predicate("(type_location {})".format(location_name))
        types = MalmoTypeManager.get_types(entity_type)
        if types != MalmoTypeManager.UNKNOWN_TYPE:
            for type in types:
                type_name = "type_{}".format(type)
                self.add_predicate("({} {})".format(type_name, entity_name))
        else:
            logger.warning("UNKNOWN type for item {}!!".format(entity_name))

        character_location = state.location
        entity_distance = character_location.dist(location)
        self.add_predicate("(entity_at {} {})".format(entity_name, location_name), distance=entity_distance)

    def add_block_predicates(self, character: 'Character', state: CharacterState, location: BlockLocation):
        block_type = location.mc_type
        if block_type not in self.blocks_to_report:
            bindings_logger.trace("Skipping {} blocks".format(block_type))
            return

        location_name = "{}_{}".format(block_type, location.id)
        binding = VariableBinding(location_name, location)
        self.variable_bindings[binding.name] = binding
        bindings_logger.debug("Added {} at location {}".format(location_name, location))

        self.add_predicate("(type_{} {})".format(block_type, location_name))
        character_location = state.location
        block_distance = character_location.dist(location)
        self.add_predicate("(type_location {})".format(location_name), distance=block_distance)
        if block_type == "farmland":
            location_above = location.copy() # type: BlockLocation
            location_above.y = location.y + 1
            above_type = character.get_block_type_at(location_above)
            if above_type != "air":
                self.add_predicate("(is_planted_on {} {})".format(above_type, location_name))
            else:
                self.add_predicate("(plantable {})".format(location_name), distance=block_distance)

        chests = character.get_observed_chests()
        if block_type == "chest" and chests.get(location.id) is NULL_BLOCK_INVENTORY:
            self.add_predicate("(unopened_container chest_{})".format(location.id))

    def add_type_predicates(self, name):
        types = MalmoTypeManager.get_types(name)
        if types != MalmoTypeManager.UNKNOWN_TYPE:
            for type in types:
                type_name = "type_{}".format(type)
                self.add_predicate("({} {})".format(type_name, name))
        else:
            logger.warning("UNKNOWN type for item {}!!".format(name))

NULL_PLAN_STATE = PlanState(character=None, state_index=0)


class SymbolicPlanState(PlanState):
    """ Symbolic representation of the plan state.

    This representation only contains symbolic values from the plan state. This is to allow
    systems that do not support numerical values or computation to be used.

    NOTE: Some predicates are removed, and others are renamed.
    """

    def __init__(self, state: PlanState):
        super().__init__(state.state_index)
        self.plan_state = state
        self.predicates_to_remove = ["inventory_index_of", "inventory_selected_index", "inventory_open_index"]
        self.predicates_to_rename = {"inventory_count": "in_inventory",
                                     }
        self.variable_bindings = state.variable_bindings

        for non_symbolic_pred, _ in self.plan_state._predicates:
            _tmp = non_symbolic_pred[non_symbolic_pred.find("(")+1:non_symbolic_pred.find(")")]
            name, *params = _tmp.split(" ")
            if name in self.predicates_to_remove:
                logger.trace("Removing predicate from symbolic state: {}".format(non_symbolic_pred))
                continue
            symbolic_params = list()
            for param in params:
                try:
                    int(param)
                    float(param)
                except ValueError:
                    symbolic_params.append(param)

            pred_name = name
            if name in self.predicates_to_rename:
                pred_name = self.predicates_to_rename[name]
            symbolic_pred = "({} {})".format(pred_name, " ".join(symbolic_params))
            logger.trace("Adding symbolic predicate to state: {}. Original: {}".format(symbolic_pred, non_symbolic_pred))
            self.add_predicate(symbolic_pred)

        logger.debug("Non-symbolic Predicate List: {}".format(state._predicates))
        logger.debug("Symbolic Predicate List: {}".format(self._predicates))



