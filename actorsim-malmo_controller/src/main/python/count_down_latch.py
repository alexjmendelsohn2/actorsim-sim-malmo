import threading

from utils.LogManager import TraceLogger


class CountDownLatch():
    def __init__(self, starting_count):
        self.count = starting_count
        self.count_lock = threading.Condition()

    def count_up(self, logger: TraceLogger = None):
        with self.count_lock:
            self.count += 1
            current_count = self.count
        logger.debug("count:{}".format(current_count))

    def count_down(self, logger: TraceLogger = None):
        with self.count_lock:
            self.count -= 1
            current_count = self.count
            if self.count <= 0:
                self.count_lock.notifyAll()
        logger.debug("count:{}".format(current_count))

    def is_count_positive(self, logger: TraceLogger = None):
        is_positive = False
        with self.count_lock:
            current_count = self.count
            if self.count >= 1:
                is_positive = True
        logger.debug("count:{}".format(current_count))
        return is_positive

    def await(self, logger: TraceLogger = None):
        with self.count_lock:
            while self.count > 0:
                logger.debug("waiting on non-positive count.  count:{}".format(self.count))
                self.count_lock.wait()
            logger.debug("count is zero, no more waiting")
