# Author : Jonathan Decker
# Email  : jonathan.decker@nrl.navy.mil
#
# Description : Script that generates a Malmo mission file (xml) from a ascii representation
# of a building. The first part of the file specifies the floorplan, the second
# part of the file defines the decorations.
# Packages: pathlib, argparse 
#
# usage: MinecraftBuilding.py [-h] [--output output] input
#
# Format :
#
# map
# @@@@@@@@@@@   @ = wall (*)
# @A    o@  @   s = player start position (1)
# @e     d s@   e = end position
# @o    B@  @   d = door (*)
# @@@@@@@@@@@   o = observation point (*)
#               A-Z = defines corners of zones
# decorations
# 10 item:log AB     # count block|item|entity:type [top-left label][bottom-right label]
#

import argparse
import os
import random
import re
import sys
import xml.etree.ElementTree as ET
from pathlib import Path

import MinecraftUtil

commentRE    = re.compile("^#")
decoratRE    = re.compile("^([1-9][0-9]*) (block|item|entity)?:?([A-Za-z_]+) ([A-Z])([A-Z])")

wallMaterial = 'sandstone'
floorMaterial = 'sandstone'
ceilingMaterial = 'glass'
ceilingLightStagger = 3
ceilingLightsFlush = True
    
height = 12
doorHeight = 2
floorThickness = 5
ceilingThickness = 2
wallThickness = 1

clearVolume = True
addLights = False
markPositions = True

missionHeaderXML='''<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<Mission xmlns="http://ProjectMalmo.microsoft.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <About>
    <Summary>Sandbox</Summary>
  </About>
  <ServerSection>
    <ServerInitialConditions>
        <Time>
            <StartTime>0</StartTime>
            <AllowPassageOfTime>false</AllowPassageOfTime>
        </Time>
        <Weather>clear</Weather>
    </ServerInitialConditions>
    <ServerHandlers>
      <FlatWorldGenerator generatorString="2;7,1x42:1"/>
      <DrawingDecorator>\n'''

missionFooterXML='''      </DrawingDecorator>
      <ServerQuitFromTimeUp timeLimitMs="%d"/>
      <ServerQuitWhenAnyAgentFinishes/>
    </ServerHandlers>
  </ServerSection>
  <AgentSection mode="Survival">
    <Name>SurveyBot</Name>
    <AgentStart>
        <Placement x="%f" y="%f" z="%f" yaw="180"/>
        <Inventory>
        </Inventory>
    </AgentStart>
    <AgentHandlers>
      <DiscreteMovementCommands/>
      <ObservationFromFullStats/>
      <ObservationFromHotBar/>
      <ObservationFromRay/>
      <SimpleCraftCommands/>
      <InventoryCommands/>
      <ObservationFromFullInventory/>
      <MissionQuitCommands quitDescription="quitting"/>
    </AgentHandlers>
  </AgentSection>
</Mission>'''

def load_data():
    # Parse schema to collect all block types.
    # First find the schema file:
    schema_dir = None
    try:
        schema_dir = os.environ['MALMO_XSD_PATH']
    except KeyError:
        print("MALMO_XSD_PATH not set? Check environment.")
        exit(1)
    types_xsd = schema_dir + os.sep + "Types.xsd"

    # Now try to parse it:
    types_tree = None
    try:
        types_tree = ET.parse(types_xsd)
    except (ET.ParseError, IOError):
        print("Could not find or parse Types.xsd - check Malmo installation.")
        exit(1)

    # Find the BlockType element:
    root = types_tree.getroot()
    block_types_element = root.find("*[@name='BlockType']")
    if block_types_element == None:
        print( "Could not find block types in Types.xsd - file corruption?")
        exit(1)

    # Find the enum inside the BlockType element:
    block_types_enum = block_types_element.find("*[@base]")
    if block_types_enum == None:
        print("Unexpected schema format. Did the format get changed without this test getting updated?")
        exit(1)

    # Now make a list of block types:
    block_types = [block.get("value") for block in block_types_enum]

    # Find the EntityType element:
    root = types_tree.getroot()
    entity_types_element = root.find("*[@name='EntityTypes']")
    if entity_types_element == None:
        print("Could not find entity types in Types.xsd - file corruption?")
        exit(1)

    # Find the enum inside the EntityType element:
    entity_types_enum = entity_types_element.find("*[@base]")
    if entity_types_enum == None:
        print("Unexpected schema format. Did the format get changed without this test getting updated?")
        exit(1)

    # Now make a list of block types:
    entity_types = [entity.get("value") for entity in entity_types_enum]

    # Find the ItemType element:
    root = types_tree.getroot()
    item_types_element = root.find("*[@name='ItemType']")
    if item_types_element == None:
        print("Could not find item types in Types.xsd - file corruption?")
        exit(1)

    # Find the enum inside the ItemType element:
    item_types_enum = item_types_element.find("*[@base]")
    if item_types_enum == None:
        print("Unexpected schema format. Did the format get changed without this test getting updated?")
        exit(1)

    # Now make a list of block types:
    item_types = [item.get("value") for item in item_types_enum]

    return block_types,entity_types,item_types

def valid(d,s,e):
    isValid = s <= e
    if isValid:
        for entry in d:
            es = ord(entry['start'])
            ee = ord(entry['end'])
            if (es < s and ee > e) or (es >= s and es <= e) or (ee >= s and ee <= e):
                isValid = False
                break
    return isValid

def loadDecorations(f):
    decorations = []
    l = f.readline()
    while l:
        m = decoratRE.match(l)
        if m:
            if valid( decorations, ord(m.group(4)), ord(m.group(5))):
                cat = m.group(2)
                if cat is None:
                    cat = 'item'
                val = {'count'    : int(m.group(1)),
                       'category' : cat,
                       'type'     : m.group(3),
                       'start'    : m.group(4),
                       'end'      : m.group(5)}
                decorations.append(val)
            else:
                print("Error: Invalid Decoration: " + l)
        l = f.readline()
    return decorations

def loadMap(f):
    grid = []
    l = f.readline()
    maxRow = 0
    if l == "map\n":
        l = f.readline()
    while l and l != '\n':
        row = list(l)
        if row[-1] == '\n':
            row = row[:-1]
        maxRow = max(maxRow,len(row))
        grid.append(row)
        l = f.readline()
    return grid,maxRow

def getValueAt(m,x,y):
    val = None
    if x >= 0 and y >= 0 and y < len(m) and x < len(m[y]):
        val = m[y][x]
    return val

def getAll(m,charSet):
    locations = []
    y = 0
    for row in m:
        x = 0
        for cell in row:
            if cell in charSet:
                locations.append((x,y))
            x += 1
        y += 1    
    return locations

def checkForAndGetOnePos(m,c):
    locs = getAll(m,c)
    numLocs = len(locs)
    pos = None
    if numLocs == 1:
        pos = locs[0][0],locs[0][1]
    elif numLocs < 1:
        print("\nError: no '%s' character found in layout." % c)
        sys.exit(-4)
    else:
        print("\nError: multiple '%s' characters found in layout; expected one." % c)
        sys.exit(-5)
    return pos

def getValueIfPresentInDict( d, m, x, y, maxRow ):
    val = None
    if (maxRow*y+x) in d:
        val = getValueAt(m,x,y)
    return val

def getNeighbors(m,x,y):
    neighbors = []
    for offset in [[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]]:
        if getValueAt(m, x+offset[0], y+offset[1]) == '@':
            neighbors.append()

# greedy algorithm to find the smallest set of lines that cover all walls
def determineWalls(m,maxRow):
    # get all wall cells
    g = {}
    y = 0
    for row in m:
        x = 0
        for cell in row:
            if cell == '@':
                g[maxRow*y+x] = {'x' : x, 'y' : y }
            x += 1
        y += 1

    walls = []

    while( len(g) != 0 ):
        key,val = g.popitem()

        horiData = {'len' : 1,'sx' : val['x'], 'ex' : val['x'], 'sy' : val['y'], 'ey' : val['y']}
        vertData = {'len' : 1,'sx' : val['x'], 'ex' : val['x'], 'sy' : val['y'], 'ey' : val['y']}

        # expand right
        nextVal = getValueIfPresentInDict(g,m,horiData['ex']+1,val['y'],maxRow)
        while nextVal == '@':
            horiData['ex'] += 1
            horiData['len'] += 1
            nextVal = getValueIfPresentInDict(g,m,horiData['ex']+1,val['y'],maxRow)

        # expand left
        nextVal = getValueIfPresentInDict(g,m,horiData['sx']-1,val['y'],maxRow)
        while nextVal == '@':
            horiData['sx'] -= 1
            horiData['len'] += 1
            nextVal = getValueIfPresentInDict(g,m,horiData['sx']-1,val['y'],maxRow)

        # expand down
        nextVal = getValueIfPresentInDict(g,m,val['x'],vertData['ey']+1,maxRow)
        while nextVal == '@':
            vertData['ey'] += 1
            vertData['len'] += 1
            nextVal = getValueIfPresentInDict(g,m,val['x'],vertData['ey']+1,maxRow)

        # expand up
        nextVal = getValueIfPresentInDict(g,m,val['x'],vertData['sy']-1,maxRow)
        while nextVal == '@':
            vertData['sy'] -= 1
            vertData['len'] += 1
            nextVal = getValueIfPresentInDict(g,m,val['x'],vertData['sy']-1,maxRow)

        wallData = None
        if vertData['len'] > horiData['len']:
            wallData = vertData
            for y in range(wallData['sy'],wallData['ey']+1):
                if y != val['y']:
                    del g[maxRow*y+wallData['sx']]
        else:
            wallData = horiData
            for x in range(wallData['sx'],wallData['ex']+1):
                if x != val['x']:
                    del g[maxRow*wallData['sy']+x]

        walls.append(wallData)

    return walls

def getOKFromUser(msg):
    # s = raw_input(msg).lower()
    # while s != 'y' and s != 'n':
    #     print random.choice(['What?','Sorry?','Please answer the question.','I didn\'t understand that.','...'])
    #     s = raw_input(msg).lower()
    return True 

def markObservationPoints(m,numSpaces):
    spaces = ' ' * numSpaces
    observeXML = ''
    for o in getAll(m,'o'):
        x = o[0]+1
        y = floorThickness+1
        z = o[1]+1
        observeXML += spaces + MinecraftUtil.drawBlock(x, y, z, 'gold_block', 'WHITE')
    return observeXML

def generateBuilding(configFile, startX, startY, startZ, timeout_in_ms=45000, missionFile='mission.xml', observeFile='observations.txt'):
    configPath = Path(configFile)
    missionPath = Path(missionFile)
    observePath = None
    if observeFile:
        observePath = Path(observeFile)
    if not configPath.exists():
        print("Error: input file %s does not exist" % (configFile))
        sys.exit(-1)
    elif missionPath.exists():
        if not getOKFromUser('Overwrite %s? (y/n) : ' % missionFile):
            print("exiting...")
            sys.exit(-2)
    if observePath and observePath.exists():
        if not getOKFromUser('Overwrite %s? (y/n) : ' % observeFile):
            print("exiting...")
            sys.exit(-3)

    print("Reading input file...")

    with open(configFile) as f:
        m,maxRow = loadMap(f)
        decorations = loadDecorations(f)

    if observePath:
        print("Writing observation points file...")
        with open(observeFile,'w') as f:
            f.write('x,y,z\n')
            for o in getAll(m,'o'):
                f.write('%d,%d,%d\n' % (o[0]+1,floorThickness+1,o[1]+1))

    print("Writing mission file...",end="")

    walls = determineWalls(m,maxRow)

    px,pz = checkForAndGetOnePos(m,'s')
    playerStart = {'x' : px + 1.5,
                   'z' : pz + 1.5,
                   'y' : floorThickness+2 }

    ex,ez = checkForAndGetOnePos(m,'e')

    block_types,entity_types,item_types = load_data()

    spaces = ' ' * 8
    with open(missionFile,'w') as f:
        boundsXML = ''
        bounds = {'minx' : 0, 'miny' : 0, 'maxx' : 0, 'maxy' : 0}
        for w in walls:
            bounds['minx'] = min(bounds['minx'],w['sx'])
            bounds['miny'] = min(bounds['miny'],w['sy'])
            bounds['maxx'] = max(bounds['maxx'],w['ex'])
            bounds['maxy'] = max(bounds['maxy'],w['ey'])

        # air volume to clear existing scene
        if clearVolume:
            x  = bounds['minx']
            y  = 1
            z  = bounds['miny']
            lx = bounds['maxx']-bounds['minx']+1
            ly = floorThickness+height+ceilingThickness+1
            lz = bounds['maxy']-bounds['miny']+1
            boundsXML += spaces + MinecraftUtil.fillVolume(x, y, z, lx, ly, lz, "air", 'WHITE')

        # create floor
        x  = bounds['minx']
        y  = 1
        z  = bounds['miny']
        lx = bounds['maxx']-bounds['minx']+1
        ly = floorThickness
        lz = bounds['maxy']-bounds['miny']+1
        boundsXML += spaces + MinecraftUtil.fillVolume(x, y, z, lx, ly, lz, floorMaterial, 'WHITE')

        if ceilingThickness > 0:
            x  = bounds['minx']
            y  = floorThickness+height+1
            z  = bounds['miny']
            lx = bounds['maxx']-bounds['minx']+1
            ly = ceilingThickness
            lz = bounds['maxy']-bounds['miny']+1
            boundsXML += spaces + MinecraftUtil.fillVolume(x, y, z, lx, ly, lz, ceilingMaterial, 'WHITE')

            # create lights
            if addLights:
                if ceilingLightStagger > 0:
                    for ceilingX in range(bounds['minx'],bounds['maxx']+1):
                        for ceilingZ in range(bounds['miny'],bounds['maxy']+1):
                            ceilingY = floorThickness + height + 2
                            lightBuilt = False
                            if ceilingX % ceilingLightStagger == 0:
                                if ceilingZ % ceilingLightStagger == 0:
                                    boundsXML += spaces + MinecraftUtil.drawBlock(ceilingX, ceilingY, ceilingZ, 'glowstone', 'WHITE')
                                    lightBuilt = True

        # add wall cuboids
        for w in walls:
            x  = w['sx']
            y  = floorThickness+1
            z  = w['sy']
            lx = w['ex']-w['sx']+1
            ly = height
            lz = w['ey']-w['sy']+1
            boundsXML += spaces + MinecraftUtil.fillVolume(x, y, z, lx, ly, lz, wallMaterial, 'WHITE')

        # Add wall above door frames
        for d in getAll(m,'d'):
            x  = d[0]
            y  = floorThickness+doorHeight+1
            z  = d[1]
            lx = 1
            ly = height-doorHeight
            lz = 1
            boundsXML += spaces + MinecraftUtil.fillVolume(x, y, z, lx, ly, lz, wallMaterial, 'WHITE')

        decorationsXML = ''

        for decor in decorations:
            spos = checkForAndGetOnePos(m,decor['start'])

            epos = checkForAndGetOnePos(m,decor['end'])

            if decor['type'] == "log":
                boundsXML += spaces + MinecraftUtil.fillVolumeReal(spos[0] + 1, 0, spos[1] + 1, epos[0] + 1, 6, epos[1] + 1, "grass", 'WHITE')
            if decor['type'] == "cobblestone":
                boundsXML += spaces + MinecraftUtil.fillVolumeReal(spos[0] + 1, 0, spos[1] + 1, epos[0] + 1, 6, epos[1] + 1, "cobblestone", 'WHITE')
            if decor['type'] == "iron_ore":
                boundsXML += spaces + MinecraftUtil.fillVolumeReal(spos[0] + 1, 0, spos[1] + 1, epos[0] + 1, 6, epos[1] + 1, "stone", 'WHITE')
            if decor['type'] == "reeds":
                boundsXML += spaces + MinecraftUtil.fillVolumeReal(spos[0] + 1, 0, spos[1] + 1, epos[0] + 1, 6, epos[1] + 1, "dirt", 'WHITE')
            if decor['type'] == "coal":
                boundsXML += spaces + MinecraftUtil.fillVolumeReal(spos[0] + 1, 0, spos[1] + 1, epos[0] + 1, 6, epos[1] + 1, "stone", 'WHITE')

            for i in range(decor['count']):
                x = random.randint(spos[0]+1,epos[0]+1)
                y = floorThickness+2
                z = random.randint(spos[1]+1,epos[1]+1)
                if decor['category'] == 'item' and (decor['type'] in block_types or decor['type'] in item_types):
                    decorationsXML += spaces + MinecraftUtil.drawItem(x, y, z, decor['type'])
                elif decor['category'] == 'block' and decor['type'] in block_types:
                    decorationsXML += spaces + MinecraftUtil.drawBlock(x, y, z, decor['type'], 'WHITE')
                elif decor['category'] == 'entity' and decor['type'] in entity_types:
                    decorationsXML += spaces + MinecraftUtil.drawEntity(x, y, z, decor['type'])
                else:
                    print("Error: type %s not found " % (decor['type']))

        if markPositions:
            decorationsXML += markObservationPoints(m,8)

        if markPositions:
            decorationsXML += spaces + MinecraftUtil.drawBlock(playerStart['x'], playerStart['y'] - 1, playerStart['z'], 'emerald_block', 'WHITE')

        f.write(missionHeaderXML)
        f.write(boundsXML)
        f.write(decorationsXML)
        f.write(missionFooterXML % (timeout_in_ms, startX, startY, startZ))

    print("Done!")

def main():
    parser = argparse.ArgumentParser(description='''Script that generates a Malmo mission file (xml) from a ascii representation of a building.
    The first part of the file specifies the floorplan, the second part of the file defines the decorations.''')
    parser.add_argument('input', metavar='input', type=str, help='name of the file that defines the building layout')
    parser.add_argument('x', metavar='x', type=float, help='agent initial x position')
    parser.add_argument('y', metavar='y', type=float, help='agent initial y position')
    parser.add_argument('z', metavar='z', type=float, help='agent initial z position')
    parser.add_argument('--output', metavar='output', type=str, default='mission.xml', help='desired name of the Malmo mission (optional)')
    parser.add_argument('--observe', metavar='observe', type=str, help='output file for observation points')
    args = parser.parse_args()

    generateBuilding(args.input,args.x,args.y,args.z,args.output,args.observe)

if __name__ == "__main__":
    main()




