import traceback
from threading import Thread, Condition

from action import NullAction, OrderedConjunctiveAction
from loggers import MalmoLoggers
from mission import Mission

logger = MalmoLoggers.malmo_planner_logger
bindings_logger = MalmoLoggers.bindings_logger
trace_logger = MalmoLoggers.trace_logger


class GoalManager():

    def __init__(self, character: 'Character', mission: Mission):
        self.character = character
        self.mission = mission
        self.log_name = "{}_goal_mgr".format(character.name)

        self.pause_cognitive_cycle_during_debugging = False  # debug use only! will cause timing issues otherwise!
        self.post_character_observation_condition = Condition()
        self.worker_tread = GoalManager.Worker(self, self.character)
        self.character.add_thread_to_start_with_mission(self.worker_tread)

        self.last_update_game_time = 0
        self.last_processed_game_time = 0

        self.goal_priorities = list()  # type List[GoalPriority]
        from character.goal_priority import NULL_GOAL_PRIORITY
        self.current_priority = NULL_GOAL_PRIORITY #type: GoalPriority

        self.character.goal_manager = self  # NB: there can only be one goal_manager per character!
        self.character.add_post_hook_010_observation(self.post_observation_hook)

    def enable_pause_cycle_for_debugging(self):
        self.pause_cognitive_cycle_during_debugging = True

    def post_observation_hook(self, character: 'Character'):
        logger.debug("Running GoalManager observation hook!")
        self.last_update_game_time = character.get_current_time()
        with self.post_character_observation_condition:
            self.post_character_observation_condition.notify()
            logger.debug("Notified goal manager of new state with index {}".format(self.character.get_latest_state().state_index))

    class Worker(Thread):
        def __init__(self, manager: 'GoalManager', character : 'Character'):
            Thread.__init__(self)
            self.manager = manager
            self.character = character
            self.connector = character.connector
            old_name = self.getName()
            self.name="{}_{}".format(self.character.name, old_name)

        def run(self):
            while self.character.mission_running():
                with self.manager.post_character_observation_condition:
                    self.manager.post_character_observation_condition.wait()
                    # release the lock for external processing

                if self.manager.pause_cognitive_cycle_during_debugging:
                    self.character.cognitive_loop_countdown_latch.count_up(logger)

                logger.info("{} notified... checking goals".format(self.name))
                latest_state_index = self.character.get_latest_state().state_index
                self.manager._select_goals(latest_state_index)
                # TODO link new state to ControlTrace

                if self.manager.pause_cognitive_cycle_during_debugging:
                    self.character.cognitive_loop_countdown_latch.count_down(logger)

            logger.debug("{} finished run".format(self.name))

    def _select_goals(self, latest_state_index: float):
        """Selects the first action that is consistent with the state."""
        logger.debug("{}: checking {} goal priorities for state_index {}".format(self.log_name, len(self.goal_priorities), latest_state_index))

        for goal_priority in self.goal_priorities: #type GoalPriority
            try:
                logger.debug("{}: Checking {}".format(self.log_name, goal_priority))
                if goal_priority.is_consistent(latest_state_index):
                    logger.debug("{}: is_consistent=True for goal priority {}".format(self.log_name, goal_priority))
                    if self.current_priority == goal_priority:
                        logger.info("{}: priority {} already current".format(self.log_name, goal_priority))
                    else:
                        from character.goal_priority import NULL_GOAL_PRIORITY
                        if self.current_priority == NULL_GOAL_PRIORITY:
                            logger.info("{}: new priority: {}".format(self.log_name, goal_priority))
                        else:
                            logger.info("{}: priority switching from {} to {}".format(self.log_name, self.current_priority, goal_priority))

                        self.current_priority = goal_priority
                    action = self.current_priority.instantiate_action(latest_state_index)

                    if not isinstance(action, NullAction):
                        if not action.is_complete():
                            self.character.clear_action_queue()
                            if isinstance(action, OrderedConjunctiveAction):
                                logger.debug("{}: for {} setting action_queue to {}".format(self.log_name, goal_priority.name, action.to_str(new_line_for_each_subaction=True)))
                            else:
                                logger.debug("{}: for {} setting action_queue to {}".format(self.log_name, goal_priority.name, action))
                            self.character.enqueue_action(action)
                            break
                        else:
                            logger.debug("{}: COMPLETED {}, so skipping".format(self.log_name, goal_priority.name))
                    else:
                        logger.debug("{}: NULL_ACTION {}, so skipping".format(self.log_name, goal_priority))
                else:
                    logger.debug("{}: INCONSISTENT {}, so skipping".format(self.log_name, goal_priority))

            except Exception as e:
                logger.error("{}: exception occurred:{}".format(self.log_name, e))
                logger.error("{}: {}".format(self.log_name, traceback.format_exc()))

