import datetime
import math
from threading import RLock
from typing import List, Dict, Set

import sys

from utils.LogManager import LogManager
from character.state import CharacterState
from container_classes import NULL_BLOCK_INVENTORY
from location import BlockLocation, EntityLocation, Location, NULL_LOCATION, DropLocation, MobLocation, ObjectLocation, \
    NullLocation
from loggers import MalmoLoggers
from malmo_types import MalmoTypeManager
from mission import Mission, ObservationGridDetails
from utils.plan_state import PlanState, NULL_PLAN_STATE

logger = MalmoLoggers.memory_logger
obs_logger = MalmoLoggers.obs_logger


class MemoryObject:
    def __init__(self, id, location: Location = NULL_LOCATION):
        self.id = ""  # type: str
        self.location = location  # type: ObjectLocation
        self.last_observed_index = 0  # type: int
        self.is_valid_observation = False  # type: bool
        self.was_told = False  # type: bool
        self.last_exported_index = 0

    def __repr__(self):
        return "{}:{}".format(self.id, self.location)

    def recently_reported(self, reporting_interval: int):
        report_delta = self.last_observed_index - self.last_exported_index
        return report_delta < reporting_interval

    def toJSON(self):
        return dict(id=self.id,
                    location=self.location.toJSON(),
                    last_observed_index=self.last_observed_index,
                    is_valid_observation=self.is_valid_observation,
                    was_told=self.was_told,
                    last_exported_index=self.last_exported_index)

NULL_MEMORY_OBJECT = MemoryObject("NULL_MEMORY_OBJECT")


class ConvertWorldStateOptions:
    def __init__(self):
        self.reporting_interval = 50



class CharacterMemory:
    """Stores persistent memory details that do not arrive every cognitive cycle such as entities or block updates.
       Also stores the state history.
    """
    def __init__(self, character: 'Character', mission: Mission):
        self._character = character
        self._mission = mission
        self._grid_names = mission.char_to_grids[self._character.name]
        self._entity_grid_names = mission.char_to_ents[self._character.name]
        self._observed_chests = dict()  # type: Dict[BlockLocation, ItemContainer]

        self._state_times_used = dict()  # type: Dict[int, float]
        self._state_history = dict()  # type: Dict[float, CharacterState]
        self._plan_state_history = dict()  # type: Dict[float, PlanState]
        self._latest_state_index = 0  # type: float
        self._latest_entity_update_index = 0  # type: float
        self._observed_objects = dict()  # type: Dict[str, MemoryObject]
        self._observed_types = dict()  # type: Dict[str, Set[str]]

        self._observed_lock = RLock()
        self._selected_hotbar = 0  # type: int

        self._newly_seen_since_last_request = set()

        self._told_locations = list()  # type: List[ObjectLocation]

    def toJSON(self, observation_reporting_interval: int):
        blocks_exported = 0
        mobs_exported = 0
        drops_exported = 0
        latest_state = self.get_latest_state()

        with self._observed_lock:
            observed_dict = dict()
            observed_by_type = dict()
            character_dict = dict(name=latest_state.name,
                                  life=latest_state.life,
                                  food=latest_state.food,
                                  air=latest_state.air,
                                  xp=latest_state.xp)
            for key, observed in self._observed_objects.items():  # type: str, MemoryObject
                if observed.recently_reported(observation_reporting_interval):
                    continue
                observed.last_exported_index = latest_state.state_index

                observed_dict[key] = observed.toJSON()
                if isinstance(observed.location, BlockLocation):
                    blocks_exported += 1
                elif isinstance(observed.location, MobLocation):
                    mobs_exported += 1
                elif isinstance(observed.location, DropLocation):
                    drops_exported += 1

            for type, observed_set in self._observed_types.items():  # type: str, Set[MemoryObject]
                type_list = list()
                for object_id in observed_set:
                    type_list.append(object_id)
                observed_by_type[type] = type_list

        logger.debug("Index:{} Exporting world state blocks:{} mobs:{} drops:{}".format(latest_state.state_index, blocks_exported, mobs_exported, drops_exported))
        d = dict(latest_state_index=latest_state.state_index,
                 world_time=latest_state.world_time,
                 character=character_dict,
                 obs=latest_state.obs,
                 observed=observed_dict,
                 observed_by_type=observed_by_type)
        return d

    def get_latest_entity_update_index(self):
        return self._latest_entity_update_index

    def get_layer_matrix_as_string_array(self, grid_name, start: Location, y_layer: int):
        grid_details = self._mission.observation_grids[grid_name]  # type: ObservationGridDetails
        lines = list()
        half_x = int(grid_details.x_range / 2)
        half_y = int(grid_details.y_range / 2)
        half_z = int(grid_details.z_range / 2)

        x_start = math.floor(start.x) - half_x
        # y_start = math.floor(start.y) - half_y
        z_start = math.floor(start.z) - half_z

        location = Location()

        width = 4    #the printing width for columns
        header = "{:<{width}}".format("", width=width)
        for z in range(grid_details.z_range):
            absolute_z = z_start + z
            header += "{:<{width}}".format(absolute_z, width=width)
        lines.append(header)

        i = 0
        absolute_y = y_layer
        for x in range(grid_details.x_range-1, -1, -1):
            absolute_x = x_start + x
            line = "{:<{width}}".format(absolute_x, width=width)
            for z in range(grid_details.z_range):
                absolute_z = z_start + z
                location.set(absolute_x, absolute_y, absolute_z)
                key_str = location.int_key_str()
                block_type = "?"
                with self._observed_lock:
                    if key_str in self._observed_objects:
                        memory_object = self._observed_objects[key_str]
                        block_location = memory_object.location # type: BlockLocation
                        block_type = block_location.mc_type[0:(width-1)]
                line += "{:<{width}}".format(block_type, width=width)
            lines.append(line)
        return lines

    def reserve_state_index(self, state: CharacterState):
        # NB: append a count suffix because multiple state updates may occur per mc_world_time.
        state_index_used_count = 0
        if state.world_time in self._state_times_used:
            state_index_used_count = self._state_times_used[state.world_time]
        self._state_times_used[state.world_time] = state_index_used_count + 1
        state.state_index = float("{}.{}".format(state.world_time, state_index_used_count))

    def register_new_state(self, state: CharacterState):
        if state.obs == None:
            logger.warning("State object without observation, refusing to store in memory.")
            return

        state.stored_time = datetime.datetime.now()
        self._state_history[state.state_index] = state
        self._latest_state_index = state.state_index
        logger.debug("Stored latest state for index {}".format(self._latest_state_index))

    def get_latest_state(self) -> 'CharacterState':
        logger.trace("Getting the state associated with index {}".format(self._latest_state_index))
        state = self._state_history[self._latest_state_index]  # type: CharacterState
        return state

    def get_latest_state_index(self) -> 'CharacterState':
        return self._latest_state_index

    def has_state(self, state_index: float) -> bool:
        return state_index in self._state_history

    def get_state(self, state_index: float) -> 'CharacterState':
        logger.debug("Getting the state associated with index {}".format(state_index))
        state = None  # type: CharacterState
        if state_index in self._state_history:
            state = self._state_history[state_index]
        return state

    def get_list_observed_block_types(self):
        return self._observed_types.keys()

    def register_new_plan_state(self, state: CharacterState, plan_state: PlanState):
        state_index = state.state_index
        self._plan_state_history[state_index] = plan_state
        logger.debug("Stored plan_state for index {}".format(state_index))

    def get_plan_state(self, state_index: float) -> PlanState:
        plan_state = NULL_PLAN_STATE
        if self.has_plan_state(state_index):
            plan_state = self._plan_state_history[state_index]
        else:
            plan_state = PlanState(self._character, state_index)
        return plan_state

    def has_plan_state(self, state_index: float):
        return state_index in self._plan_state_history

    def get_selected_hotbar_slot(self):
        """Returns the selected hotbar slot.
           Value is zero-indexed"""
        return self._selected_hotbar

    def set_selected_hotbar_slot(self, slot):
        self._selected_hotbar = slot

    def get_observation(self, object_id) -> MemoryObject:
        """Returns a _READ_ONLY_ memory object associated with id.
           Note: Even though const cannot be enforced, only the memory class should modify memory_objects.
                 Clients should not modify memory_objects returned from this method. """
        memory_object = NULL_MEMORY_OBJECT
        with self._observed_lock:
            if object_id in self._observed_objects:
                memory_object = self._observed_objects[object_id]
        return memory_object

    def get_observation_type(self, object_id):
        """Returns a _READ_ONLY_ memory object associated with id.
           Note: Even though const cannot be enforced, only the memory class should modify memory_objects.
                 Clients should not modify memory_objects returned from this method. """
        memory_object = NULL_MEMORY_OBJECT
        with self._observed_lock:
            if object_id in self._observed_objects:
                memory_object = self._observed_objects[object_id]

        mc_type = MalmoTypeManager.UNKNOWN_TYPE
        if memory_object != NULL_MEMORY_OBJECT:
            location = memory_object.location
            if not isinstance(location, NullLocation):
                mc_type = location.mc_type
        return mc_type

    def is_valid_observation(self, object_id):
        """Returns whether the object id has a valid location"""
        valid_observation = False
        memory_object = self.get_observation(object_id)
        if memory_object != NULL_MEMORY_OBJECT:
            if memory_object.is_valid_observation:
                valid_observation = True
        return valid_observation

    def process_blocks_and_entities(self, current_state, json_data):
        with self._observed_lock:
            for grid_name in self._grid_names:
                self.__process_map_locked(current_state, json_data, grid_name)
            for grid_name in self._entity_grid_names:
                self.__process_entities_locked(current_state, json_data, grid_name)

    def __process_map_locked(self, current_state, json_data, grid_name):
        obs_logger.debug("Processing grid:{!r}".format(grid_name))

        nearby_blocks = json_data.get(grid_name)  # type: List[str]
        if nearby_blocks is None:
            return
        if MalmoLoggers.log_observation_blocks:
            level = obs_logger.getEffectiveLevel()
            block_len = len(nearby_blocks)
            logger.log(level, "{} blocks:{}".format(block_len, nearby_blocks))

        obs_logger.trace("blocks: {}".format(nearby_blocks))
        obs_logger.trace("length blocks: {}".format(len(nearby_blocks)))

        details = self._mission.observation_grids[grid_name]  # type: ObservationGridDetails

        half_x = int(details.x_range / 2)
        half_y = int(details.y_range / 2)
        half_z = int(details.z_range / 2)

        location = current_state.location
        x_start = math.floor(location.x) - half_x
        y_start = math.floor(location.y) - half_y
        z_start = math.floor(location.z) - half_z

        tmp_block_location = BlockLocation()
        i = 0
        for y in range(details.y_range):
            absolute_y = y_start + y
            for z in range(details.z_range):
                absolute_z = z_start + z
                for x in range(details.x_range):
                    absolute_x = x_start + x
                    tmp_block_location.set(absolute_x, absolute_y, absolute_z)
                    mc_type = nearby_blocks[i]
                    tmp_block_location.mc_type = mc_type
                    self._update_observed_locked(current_state, tmp_block_location)
                    if mc_type == 'chest':
                        if self._observed_chests.get(tmp_block_location.id) is None:
                            obs_logger.trace("Observed a new chest, chest_{}. Adding it too observed chests as null_container".format(current_state.chest.get_location()))
                            self._observed_chests[tmp_block_location.id] = NULL_BLOCK_INVENTORY
                        elif current_state.chest is not None:
                            obs_logger.trace("Current chest location {}".format(current_state.chest.get_location()))
                            obs_logger.trace("Current block location {}".format(tmp_block_location))
                            if current_state.chest.get_location() == tmp_block_location:
                                obs_logger.trace("updating inventory of chest {}".format(tmp_block_location.id))
                                self._observed_chests[tmp_block_location.id] = current_state.chest
                    i += 1

        logger.debug("blocks processed:{} start:({},{},{}) end({},{},{})".format(i, x_start, y_start, z_start, absolute_x, absolute_y, absolute_z))

    def __process_entities_locked(self, current_state: CharacterState, data, grid_name):
        # entities arrive periodically cf. mission_spec: ObservationFromNearbyEntities.update_frequency
        entities = data.get(grid_name, "")
        level = obs_logger.getEffectiveLevel()
        if MalmoLoggers.log_observation_entities or level == LogManager.DEBUG:
            logger.log(level, "processing entities:'{}'".format(entities))
        if len(entities) > 0:
            self._latest_entity_update_index = current_state.state_index
            for entity in entities:
                name = MalmoTypeManager.get_real_name(entity.get('name', ""))
                mc_type = name
                if name == self._character.name:
                    continue

                x = entity.get('x', 0)
                y = entity.get('y', 0)
                z = entity.get('z', 0)
                id = entity.get('id', 0)
                pitch = entity.get('pitch', 0)
                yaw = entity.get('yaw', 0)
                types = MalmoTypeManager.get_types(name)
                if "mob" in types:
                    tmp_entity_location = MobLocation(x, y, z, pitch, yaw, mc_type=mc_type, uuid=id)
                else:
                    tmp_entity_location = DropLocation(x, y, z, mc_type=mc_type, uuid=id)
                    tmp_entity_location.variant = entity.get('variant', "")
                    tmp_entity_location.quantity = entity.get('quantity', 1)

                self._update_observed_locked(current_state, tmp_entity_location)
                if MalmoLoggers.log_observation_entities:
                    level = obs_logger.getEffectiveLevel()
                    logger.log(level, "  updated entity:{}".format(tmp_entity_location))

            self._remove_intersecting_but_unobserved_entities_locked(grid_name, current_state)

    def _update_observed_locked(self, current_state: CharacterState, object_location: ObjectLocation):
        """Updates the observation data structures.
           This method assumes the observed_lock has already been acquired."""
        id = object_location.id
        if id in self._observed_objects:
            memory_object = self._observed_objects[id]
            old_type = memory_object.location.mc_type
            new_type = object_location.mc_type
            if old_type != new_type:
                self._update_type_id_locked(old_type, new_type, id)
        else:
            memory_object = MemoryObject(id, object_location.copy())
            self._observed_objects[id] = memory_object
            mc_type = object_location.mc_type
            self._add_type_id_locked(mc_type, id)
        memory_object.location.update(object_location)
        memory_object.last_observed_index = current_state.state_index
        memory_object.is_valid_observation = True

    def _add_type_id_locked(self, mc_type, object_id):
        """Adds the type id to observed_types.
           This method assumes the observed_lock has already been acquired."""
        if mc_type in self._observed_types:
            type_set = self._observed_types[mc_type]
        else:
            type_set = set()
            self._observed_types[mc_type] = type_set
            self._newly_seen_since_last_request.add(mc_type)

        type_set.add(object_id)

    def _update_type_id_locked(self, old_mc_type, new_mc_type, object_id):
        """Updates the type id in observed_types.
           This method assumes the observed_lock has already been acquired."""
        if old_mc_type in self._observed_types:
            old_type_set = self._observed_types[old_mc_type]
            old_type_set.remove(object_id)
        else:
            logger.warning("old type not found in observed_types.")

        self._add_type_id_locked(new_mc_type, object_id)

    def _remove_intersecting_but_unobserved_entities_locked(self, grid_name, current_state: CharacterState):
        """Marks invalid entity observations within the intersecting bounds but not present.
           This method assumes the observed_lock has already been acquired."""

        details = self._mission.ents_to_details[grid_name]  # type: ObservationGridDetails

        half_x = int(details.x_range / 2)
        half_y = int(details.y_range / 2)
        half_z = int(details.z_range / 2)

        location = current_state.location
        x_start = math.floor(location.x) - half_x
        y_start = math.floor(location.y) - half_y
        z_start = math.floor(location.z) - half_z

        x_end = x_start + details.x_range
        y_end = y_start + details.y_range
        z_end = z_start + details.z_range

        min_location = Location(x_start, y_start, z_start)
        max_location = Location(x_end, y_end, z_end)

        for key, memory_object in self._observed_objects.items():
            object_location = memory_object.location
            if isinstance(object_location, EntityLocation):
                logger.trace("Checking entity {} for intersection".format(object_location))
                if object_location.intersects(min_location, max_location):
                    last_observed = memory_object.last_observed_index
                    if last_observed != self._latest_entity_update_index:
                        logger.trace("  intersected but not observed, so clearing location")
                        memory_object.is_valid_observation = False

    def tell(self, location: ObjectLocation):
        """Tell the character about the object at location."""
        logger.debug("Told about {}".format(location))
        self._told_locations.append(location)

    def process_told_observations(self, current_state):
        """Performs two tasks:
             1. Adds the told observations while enforcing a preference for known locations to told locations.

                Since some observations might be approximate or dated (i.e., an entity has since moved),
                this only adds the told location if the name does not already exist within the character's relevant map.
                For example, suppose the central location where chickens were spawned is told to the character,
                then this method would only add the told location to memory if no other chickens were in memory.

             2. Removes the told location if the character has invalidated the observation,
                eliminating cases where the character revisits a told location.
        """
        locations_to_remove = list()
        for told_location in self._told_locations:  # type: ObjectLocation
            cur_location = current_state.location

            if told_location.id in self._observed_objects:
                if not self.is_valid_observation(told_location.id):
                    locations_to_remove.append(told_location)
                    logger.debug("Removed told location {}".format(told_location))
                    break

            with self._observed_lock:
                if told_location.mc_type not in self._observed_types:
                    self._update_observed_locked(current_state, told_location)

        for location_to_remove in locations_to_remove:
            self._told_locations.remove(location_to_remove)

    def get_recently_seen(self):
        previously_seen = self._newly_seen_since_last_request
        self._newly_seen_since_last_request = set()
        logger.debug("newly seen since last request: {}".format(previously_seen))
        return previously_seen

    def has_observed(self, mc_type, at_least_one_valid_location=True):
        observed = False
        with self._observed_lock:
            if mc_type in self._observed_types:
                if at_least_one_valid_location:
                    object_keys = self._observed_types[mc_type]
                    for key in object_keys:
                        memory_object = self._observed_objects[key]
                        if not isinstance(memory_object.location, NullLocation):
                            observed = True
                else:
                    observed = True
        return observed

    def block_type_at(self, location: Location):
        type = MalmoTypeManager.UNKNOWN_TYPE
        if not isinstance(location, NullLocation):
            key = location.int_key_str()
            with self._observed_lock:
                if key in self._observed_objects:
                    memory_object = self._observed_objects[key]
                    object_location = memory_object.location
                    if isinstance(object_location, ObjectLocation):
                        type = object_location.mc_type
        return type

    def get_closest_block_location(self, block_type: str) -> BlockLocation:
        """Returns a _copy_ of the closest _valid_ block location of the given type or NULL_LOCATION if none exists."""
        closest = NULL_LOCATION
        if block_type not in self._observed_types:
            logger.debug("block type '{}' has not been observed".format(block_type))
            return closest

        with self._observed_lock:
            closest_dist = sys.maxsize
            for object_id in self._observed_types[block_type]:
                memory_object = self._observed_objects[object_id]
                if memory_object.is_valid_observation:
                    object_location = memory_object.location
                    if isinstance(object_location, BlockLocation):
                        latest_state = self.get_latest_state()
                        block_dist = latest_state.location.dist(object_location)
                        if block_dist <= closest_dist:
                            if closest == NULL_LOCATION:
                                closest = object_location.copy()
                            closest.update(object_location)
                            closest_dist = block_dist
        return closest

    def get_closest_entity_location(self, entity_type: str) -> EntityLocation:
        """Returns a _copy_ of the closest _valid_ entity location of the given type or NULL_LOCATION if none exists."""
        closest = NULL_LOCATION
        if entity_type not in self._observed_types:
            logger.debug("entity type '{}' has not been observed".format(entity_type))
            return closest

        with self._observed_lock:
            closest_dist = sys.maxsize
            if entity_type in self._observed_types:
                key_set = self._observed_types[entity_type]
                for key in key_set:
                    memory_object = self._observed_objects[key]
                    if memory_object.is_valid_observation:
                        object_location = memory_object.location
                        if isinstance(object_location, EntityLocation):
                            latest_state = self.get_latest_state()
                            entity_dist = latest_state.location.dist(object_location)
                            if entity_dist <= closest_dist:
                                if closest == NULL_LOCATION:
                                    closest = object_location.copy()
                                closest.update(object_location)
                                closest_dist = entity_dist

        logger.debug("Returning closest entity:{}".format(closest))
        return closest

    def get_closest_drop_location(self, mc_type: str) -> EntityLocation:
        closest = NULL_LOCATION
        if mc_type not in self._observed_types:
            logger.debug("entity type '{}' has not been observed".format(mc_type))
            return closest

        with self._observed_lock:
            closest_dist = sys.maxsize
            if mc_type in self._observed_types:
                key_set = self._observed_types[mc_type]
                for key in key_set:
                    memory_object = self._observed_objects[key]
                    if memory_object.is_valid_observation:
                        object_location = memory_object.location
                        if isinstance(object_location, DropLocation):
                            latest_state = self.get_latest_state()
                            entity_dist = latest_state.location.dist(object_location)
                            if entity_dist <= closest_dist:
                                if closest == NULL_LOCATION:
                                    closest = object_location.copy()
                                closest.update(object_location)
                                closest_dist = entity_dist

        logger.debug("Returning closest {} drop:{}".format(mc_type, closest))
        return closest

    def visit_observed_objects(self, visit_func_callback, visit_func_kwparams):
        """A synchronized method to visit the observed objects"""
        with self._observed_lock:
            for key, object in self._observed_objects.items():
                visit_func_callback(*visit_func_kwparams, object)

    def get_observed_chests(self):
        return self._observed_chests


