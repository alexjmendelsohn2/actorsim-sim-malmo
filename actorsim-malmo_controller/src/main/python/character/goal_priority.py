
import datetime
import random

from area import Area
from character.action_trace import PlanTrace
from character.character import Character
from action import NullAction, NULL_ACTION, OrderedConjunctiveAction, SKIP_ACTION
from actions import ExploreAreaWithSpiral, SelectItem, SelectHotbarSlot
from loggers import MalmoLoggers
from utils.plan_manager import PlanManager, create_action_from_primitive
from utils.plan_utils import ProblemDescription

logger = MalmoLoggers.planner_logger


class GoalPriority():

    def __init__(self, character, name):
        self.character = character
        self.name = name

    def __repr__(self):
        return "GoalPriority:{}".format(self.name)

    def __str__(self):
        return self.__repr__()

    def is_consistent(self, state_index: float):
        """Checks state to determine if this action could be run."""
        return True

    def instantiate_action(self, state_index: float):
        """Creates the (possibly abstract) action that will achieve this priority."""
        return NULL_ACTION


class NullGoalPriority(GoalPriority):

    def __init__(self):
        GoalPriority.__init__(self, character=None, name="NULL_PRIORITY")

NULL_GOAL_PRIORITY = NullGoalPriority()


class ExploreForPriority(GoalPriority):

    def __init__(self, character: 'Character', area: Area):
        GoalPriority.__init__(self, character, "explore_for")
        self.action = ExploreAreaWithSpiral(character, area)
        self.area = area

    def is_consistent(self, state_index: float):
        return True

    def instantiate_action(self, state_index: float):
        return self.action


class PlanningPriority(GoalPriority):

    def __init__(self, character: 'Character', plan_manager: PlanManager, name, top_level_goal):
        GoalPriority.__init__(self, character, name)
        self.top_level_goal = top_level_goal
        self.plan_manager = plan_manager
        self.problem = None #ProblemDescription
        self.plan = None
        self.root_action = NULL_ACTION
        random_min = random.randrange(0, 5) + random.random()  # NB: randomize to reduce simultaneous requests for planning
        self.min_seconds_between_planning = 3 + random_min
        self.last_planning_attempt = datetime.datetime.min
        self.replan_requested = False

        self.explore_for_items = set()
        self.collect_steps = list()

    def is_consistent(self, state_index: float):
        planning_gap_expired = False
        for item in self.explore_for_items:
            logger.debug("{}: current plan depends on exploring for {}, checking observations".format(self.name, item))
            if self.character.has_observed(item):
                logger.debug("{}: relevant item {} observed, replanning...".format(self.name, item))
                self.plan = None
                planning_gap_expired = True

        now = datetime.datetime.now()
        delta = now - self.last_planning_attempt
        if delta.total_seconds() >= self.min_seconds_between_planning:
            planning_gap_expired = True

        if planning_gap_expired or self.replan_requested:
            self.replan_requested = False
            if self.plan == None:
                self.last_planning_attempt = now
                self.problem = ProblemDescription(self.character)
                problem = self.problem  # for easier debugging
                problem.state_index = state_index
                problem.task_to_decompose = [self.top_level_goal]
                logger.debug("{}: Attempting to decompose {}".format(self.name, problem.task_to_decompose))
                self.plan_manager.plan(problem)
                result = problem.result
                if result.success:
                    self.plan = self.problem.result.plan
                    self.instantiate_action(state_index)
                    self.character.plan_trace_list.append(PlanTrace(self.problem))
                else:
                    logger.info("{}: failed to decompose task {}".format(self.name, self.problem.task_to_decompose))

        valid_plan = False  # there is a plan ready to process or currently running
        active_plan = False # the plan is active and not waiting for something
        if self.plan != None:
            if not isinstance(self.root_action, NullAction):
                logger.debug("{}: A plan is active".format(self.name))
                if self.root_action.is_consistent:
                    logger.debug("{}: The plan is consistent so far...".format(self.name))
                    valid_plan = True
                    if self.root_action.is_active():
                        active_plan = True
                else:
                    logger.debug("{}: The plan's first action has indicated an inconsistency; replanning".format(self.name))
                    self.replan_requested = True
                    self.root_action = NULL_ACTION
                    valid_plan = False
                    self.plan = None
            else:
                logger.debug("{}: The generated plan has no applicable actions".format(self.name))
                valid_plan = False

        continue_plan = valid_plan and active_plan
        return continue_plan

    def instantiate_action(self, state_index: float):
        if not isinstance(self.root_action, NullAction):
            if self.root_action.is_complete():
                logger.debug("{}: root action is complete, removing existing plan".format(self.name))
                logger.debug("{}: completed action:{}".format(self.name, self.root_action.to_str(new_line_for_each_subaction=True)))
                self.last_planning_attempt = datetime.datetime.min
                self.root_action = NULL_ACTION
                self.plan = None

        if isinstance(self.root_action, NullAction):
            logger.debug("{}: root action null, attempting to use plan".format(self.name))
            if self.plan is not None:
                logger.debug("{}: processing plan {}".format(self.name, self.plan))
                self.explore_for_items.clear()
                self.collect_steps.clear()
                for step in self.plan:  # type: str
                    if step.find("explore_for") >= 0:
                        step_tokens = step.split()
                        item = step_tokens[1].replace("placeholder_", "")
                        self.explore_for_items.add(item)
                    if step.find("collect") >= 0:
                        placeholder_index = step.find("placeholder")
                        if placeholder_index < 0:
                            self.collect_steps.append(step)

                invalid_plan = False
                if len(self.collect_steps) > 0:
                    self.root_action = OrderedConjunctiveAction(self.name)
                    for step in self.collect_steps:  #type: str
                        action = create_action_from_primitive(self.character.goal_manager.character, state_index,
                                                              self.problem, step)
                        if action == SKIP_ACTION:
                            logger.debug("action skipped")
                        elif action != NULL_ACTION:
                            self.root_action.add(action)
                        else:
                            logger.debug("invalid action")
                            invalid_plan = True

                elif len(self.explore_for_items) <= 0:
                    self.root_action = OrderedConjunctiveAction(self.name)
                    for step in self.plan:  #type: str
                        action = create_action_from_primitive(self.character.goal_manager.character, state_index,
                                                              self.problem, step)
                        if action == SKIP_ACTION:
                            logger.debug("action skipped")
                        elif action != NULL_ACTION:
                            self.root_action.add(action)
                        else:
                            logger.debug("invalid action")

                if invalid_plan:
                    logger.error("The plan attempted is invalid, removing plan")
                    self.plan = None

                else:
                    logger.debug("{}: the plan requires exploration so no actions can be instantiated".format(self.name))
            else:
                logger.debug("{}: Plan was empty".format(self.name))
        return self.root_action


class ObtainItemPriority(PlanningPriority):
    def __init__(self, character: 'Character', plan_manager: PlanManager, item: str):
        PlanningPriority.__init__(self, character, plan_manager, "obtain_{}".format(item), "(obtain_{})".format(item))


PREFERRED_PLANTING_HOTBAR_POSITIONS = \
    [
        ("iron_sword", 1),
        ("bone_meal", 2),
        ("wheat_seeds", 3),
        ("beetroot_seeds", 4),
        ("carrot", 5),
        ("potato", 6)
    ]


class TidyInventoryPriority(GoalPriority):
    """A hack to ensure plantable things are on the hotbar for shop.
       Uses hotbar_positions, a list of items that need to be on the hotbar along with a preferred location.
       If the item is in inventory but not on the hotbar, moves the item to the hotbar's preferred location.
       If the item is already on the hotbar, does nothing.
    """
    def __init__(self, character: 'Character'):
        GoalPriority.__init__(self, character, "tidy_inventory")
        self.action = NULL_ACTION
        self.swap_list = list()

    def is_consistent(self, state_index: float):
        if self.action.is_complete():
            logger.debug("{}: action was complete; setting to NULL_ACTION".format(self.name))
            self.action = NULL_ACTION
            self.swap_list.clear()
        else:
            logger.debug("{}: action not null, so reusing old action".format(self.name) )

        inventory = self.character.get_inventory()
        swap_list = self.swap_list # handy for debugging
        for item_position in PREFERRED_PLANTING_HOTBAR_POSITIONS:
            item = item_position[0]
            if inventory.amount_of(item) > 0:
                index_set = inventory.index_of_all(item) #inventory.name_to_index_map[item]
                needs_swap_to_hotbar = True
                for index in index_set:
                    if index < inventory.hot_bar_size:
                        needs_swap_to_hotbar = False
                if needs_swap_to_hotbar:
                    swap_list.append(item_position)

        swap_needed = len(swap_list) > 0
        return swap_needed

    def instantiate_action(self, state_index: float):
        if isinstance(self.action, NullAction):
            swap_list = self.swap_list
            ordered = OrderedConjunctiveAction(self.name)
            for item_position in swap_list:
                item = item_position[0]
                needed_position = item_position[1]
                ordered.add(SelectHotbarSlot(self.character, needed_position))
                ordered.add(SelectItem(self.character, item))
            self.action = ordered
        return self.action
