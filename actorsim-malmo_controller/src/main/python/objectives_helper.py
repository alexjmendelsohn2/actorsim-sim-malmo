
def read_objectives(objectives_file):
    objectives = dict()
    lines = []
    with open(objectives_file) as f:
        lines = f.readlines()

    for line in lines:
        line = line.rstrip()
        if line.startswith("#"):
            continue
        print("processing line:{}".format(line))
        tokens = line.split(',')
        objective = tokens[0]
        num_items = len(tokens)
        items = tokens[1:num_items]
        objectives[objective] = items
    return objectives

if __name__ == "__main__":
    objectives_map = read_objectives("objectives.txt")
    print(objectives_map)
    objectives = list(objectives_map.keys())
    print(objectives)
    for i in range(0,100):
        objective = objectives[i%len(objectives)]
        items = objectives_map[objective]
        print("{:3d}: {} with items {}".format(i,objective,items))


