from enum import Enum


class ExperimentType(Enum):
    MULTI_AGENT = 1
    SINGLE_AGENT = 2

class ExperimentCondition(Enum):
    HTN_PLAN_REC = 1
    COMPANION_PLAN_REC = 2
    COMPANION_TOM = 3