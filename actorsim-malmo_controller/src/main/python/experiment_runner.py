
from utils.LogManager import LogManager
from experiment_enums import ExperimentType, ExperimentCondition
from threading import Thread
from experiment import MultiAgentExperiment, NullExperiment
from mission import Mission


logger = LogManager.get_logger("experiment.runner")

class ExperimentRunner:

    def __init__(self, experiment_id, experiment_type: ExperimentType, experiment_condition: ExperimentCondition, mission: Mission):
        self.experiment_id = experiment_id
        self.experiment_type = experiment_type
        self.experiment_condition = experiment_condition
        self.mission = mission
        self.experiment = NullExperiment()
        self.worker_thread = ExperimentRunner.Worker(self)
        self.worker_thread.start()

    def start_experiment(self):
        if self.experiment_type == ExperimentType.MULTI_AGENT:
            self.start_multiagent_experiment()
        # TODO: other experiment types here as necessary
        else:
            logger.error("EXPERIMENT ERROR: no experiment start defined for experiment type: {}".format(self.experiment_type))

    def start_multiagent_experiment(self):
        exp_id = self.experiment_id
        mission = self.mission
        condition = self.experiment_condition
        experiment = MultiAgentExperiment(exp_id, mission, condition)
        self.experiment = experiment
        logger.debug("starting experiment")
        experiment.start_exp()

    def finish_experiment(self):
        logger.debug("finishing experiment")
        self.experiment.finish()


    class Worker(Thread):
        def __init__(self, parent: 'ExperimentRunner'):
            Thread.__init__(self)
            self.parent = parent
            self.experiment_id = parent.experiment_id
            self.experiment_type = parent.experiment_type
            old_name = self.getName()
            self.name = "exp_runner_thread_{}".format(old_name)
        def run(self):
            #experiment_running = False
            #while not experiment_running:
            self.parent.start_experiment()




