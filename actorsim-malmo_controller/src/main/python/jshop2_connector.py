import os
import re
from py4j.java_gateway import JavaGateway, GatewayParameters

from loggers import MalmoLoggers
from utils.plan_utils import ProblemDescription

logger = MalmoLoggers.jshop2_logger

class JShop2Connector:

    def __init__(self, addr="localhost", port=9898, filename=""):
        self.jshop2_planner = JavaGateway(gateway_parameters=GatewayParameters(port=25335))

        self.logging_print_problem_and_result = False
        self.save_problem_file = None

    def execute(self, problem: ProblemDescription, problem_filename=None):
        from utils.plan_manager import create_shop_str
        create_shop_str(problem)

        shop_str = ""
        for line in problem.shop_lines:
            # cleaned_line = re.sub(';.*', "", line).strip()
            # if cleaned_line != "":
            #     shop_str += " {}\n".format(cleaned_line)
            shop_str += " {}\n".format(line)
        shop_str += "\n"
        if self.logging_print_problem_and_result:
            level = logger.getEffectiveLevel()
            logger.log(level, "Problem sent to shop: {}".format(shop_str))

        if self.save_problem_file:
            from datetime import datetime
            taskname = "{}".format(problem.task_to_decompose).strip("'[]()")
            filename = datetime.now().strftime('%Y%m%d-%H%M%S')
            filename += "_{}.lisp".format(taskname)
            logger.log(level, "Writing problem file: {0}".format(filename))
            plan_sequence = self.jshop2_planner.entry_point.execute(shop_str, filename)
        else:
            plan_sequence = self.jshop2_planner.entry_point.execute(shop_str)

        if self.logging_print_problem_and_result:
            level = logger.getEffectiveLevel()
            logger.log(level, "Plan sequence received: {0}".format(plan_sequence))

        return plan_sequence

    def shut_down(self):
        self.socket.close()