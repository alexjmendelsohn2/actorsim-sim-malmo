from builtins import range
#from past.utils import old_div
import MalmoPython
import os
import random
import sys
import time
import json

agent_host = MalmoPython.AgentHost()
agent_host.setObservationsPolicy(MalmoPython.ObservationsPolicy.LATEST_OBSERVATION_ONLY)

#mission_file_no_ext = "C:/qrg/irina/minecraft/test/test_mission_multi"
#mission_file = mission_file_no_ext + ".xml"
#with open(mission_file, 'r') as f:
#    print("Loading mission from %s" % mission_file)
#    mission_xml = f.read()

mission_xml = '''<?xml version="1.0" encoding="UTF-8" ?>
    <Mission xmlns="http://ProjectMalmo.microsoft.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <About>
            <Summary>Basic world for testing Companions Minecraft Agent in multi-agent env</Summary>
        </About>

        <ServerSection>
            <ServerInitialConditions>
                <Time>
                    <StartTime> 1000 </StartTime>
                    <AllowPassageOfTime>true</AllowPassageOfTime>
                </Time>
            </ServerInitialConditions>
            <ServerHandlers>
                <FlatWorldGenerator generatorString="2;7,15x42;1" forceReset="true"/>      
                  <DrawingDecorator>                                                                                                                         
                     <DrawBlock x="10" y="16" z="0" type="dirt"/>   
                     <DrawCuboid x1="11" y1="15" z1="0" x2="11" y2="15" z2="2" type="water"/>                                                                
                     <DrawCuboid x1="10" y1="15" z1="0" x2="10" y2="15" z2="2" type="farmland"/>                                                                
                     <DrawCuboid x1="11" y1="16" z1="0" x2="11" y2="16" z2="2" type="air"/>                                                                  
                  </DrawingDecorator>                                                                                                                        
                  <ServerQuitFromTimeUp timeLimitMs="60000" description="out_of_time"/>                                                                      
                  <ServerQuitWhenAnyAgentFinishes/>                                                                                                          
                </ServerHandlers>
        </ServerSection>

        <AgentSection mode="Survival">
            <Name>Companion</Name>
            <AgentStart>
                    <Placement x="0" y="18" z="0"/>
                    <Inventory>
                        <InventoryObject slot="0" type="diamond_pickaxe"/>
                        <InventoryObject slot="1" type="apple" colour="RED"/>
                    </Inventory>

            </AgentStart>
            <AgentHandlers>
                <DiscreteMovementCommands/>
                <ContinuousMovementCommands turnSpeedDegs="180" /> 
                <!--ObservationFromFullInventory /-->                                                                                          
                <ObservationFromFullStats/>
                <ObservationFromGrid>                                                                                                                      
                      <Grid name="blocks3x3">                                                                                                                
                        <min x="1" y="-1" z="-1"/>                                                                                                           
                        <max x="1" y="1" z="1"/>                                                                                                             
                      </Grid>                                                                                                                                
                  </ObservationFromGrid>                                                                                                                     
                  <ObservationFromRay/>
                  <MissionQuitCommands/>
                  <ObservationFromNearbyEntities>
                    <Range name="Entities" xrange="30" yrange="5" zrange="30" update_frequency="7"/> 
                  </ObservationFromNearbyEntities>
                <ObservationFromFullInventory/>
                <VideoProducer want_depth="false">
                   <Width>960</Width>
                   <Height>640</Height>
                </VideoProducer>
             
            </AgentHandlers>            
        </AgentSection>


<AgentSection mode="Survival">
            <Name>Alex</Name>
            <AgentStart>
                    <Placement x="0" y="18" z="0"/>
                    <Inventory>
                        <InventoryObject slot="0" type="iron_axe"/>
                        <InventoryObject slot="1" type="diamond"/>
                    </Inventory>

            </AgentStart>
            <AgentHandlers>
                <DiscreteMovementCommands/>
                <ObservationFromFullStats/>
                <!--ObservationFromFullInventory/-->
                <ObservationFromRay/>
                <ObservationFromNearbyEntities>
                    <Range name="EntitiesAlex" xrange="1" yrange="1" zrange="1"/>
                </ObservationFromNearbyEntities>        
                <ObservationFromGrid>
                    <Grid name="floor3x3Alex">
                        <min x="-1" y="-1" z="-1"/>
                        <max x="1" y="-1" z="1"/>
                    </Grid> 
                </ObservationFromGrid>
                <VideoProducer want_depth="false">
                   <Width>960</Width>
                   <Height>640</Height>
                </VideoProducer>
             
            </AgentHandlers>            
        </AgentSection>    

    </Mission>
'''

my_mission = MalmoPython.MissionSpec(mission_xml, True)

#mission_record = mission_file_no_ext + ".tgz" # details set inside set_mission_record_spec
mission_record = "test.tgz"
my_mission_record = MalmoPython.MissionRecordSpec(mission_record)
my_mission_record.recordMP4(20, 400000) #TODO: figure out what actually makes sense here
my_mission_record.recordObservations()
my_mission_record.recordCommands()
my_mission_record.recordRewards()

client_pool = MalmoPython.ClientPool()
client_pool.add( MalmoPython.ClientInfo( "127.0.0.1", 10000 ) )
client_pool.add( MalmoPython.ClientInfo( "127.0.0.1", 10001 ) )


max_retries = 3
retry = 0

while True:
    try:
        print("Calling startMission...")
        agent_host.startMission(my_mission, client_pool, my_mission_record, 1, "0" )
        break
    except MalmoPython.MissionException as e:
        errorCode = e.details.errorCode
        if errorCode == MalmoPython.MissionErrorCode.MISSION_SERVER_WARMING_UP:
            print("Server not online yet - will keep waiting as long as needed.")
            time.sleep(1)
        elif errorCode in [MalmoPython.MissionErrorCode.MISSION_INSUFFICIENT_CLIENTS_AVAILABLE,
                           MalmoPython.MissionErrorCode.MISSION_SERVER_NOT_FOUND]:
            retry += 1
            if retry == max_retries:
                print("Error starting mission:", e)
                exit(1)
            print("Resources not found - will wait and retry a limited number of times.")
            time.sleep(5)
        else:
            print("Blocking error:", e.message)
            exit(1)

print("Waiting for the mission to start", end=' ')
start_time = time.time()
world_state = agent_host.getWorldState()
while not world_state.has_mission_begun:
    print(".", end="")
    time.sleep(0.1)
    world_state = agent_host.getWorldState()
    if len(world_state.errors) > 0:
        for err in world_state.errors:
            print(err)
        exit(1)
    if time.time() - start_time > 120:
        print("Mission failed to begin within two minutes - did you forget to start the other agent?")
        exit(1)
print()
print("Mission has begun.")