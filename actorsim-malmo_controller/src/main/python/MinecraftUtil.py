import math
import sys
import base64

blockHash = {}

def clearBlocks():
    global blockHash
    blockHash = {}

def getKeyFromPosition(x,y,z):
    return ':'.join((base64.b64encode(str(x)),base64.b64encode(str(y)),base64.b64encode(str(z))))

def getPositionFromKey(key):
    return { k : int(base64.b64decode(v)) for k,v in zip(('x','y','z'),key.split(':')) }

def fillVolumeBasis(sx, sy, sz, ix, iy, iz, lx, ly, lz, blockType, blockColour):
    xmlStr = ''
    if lx > 0 and ly > 0 and lz > 0:
        xmlStr = '<DrawCuboid x1="%d" y1="%d" z1="%d" x2="%d" y2="%d" z2="%d" type="%s" colour="%s"/>\n' % (sx+ix,sy+iy,sz+iz,sx+lx,sy+ly,sz+lz,blockType,blockColour)
    return xmlStr

def drawEntity(x,y,z,type):
    return '<DrawEntity x="{}" y="{}" z="{}" type="{}" yaw="0"/>\n'.format(x, y, z, type)

def drawBlockFacing(x,y,z,blockType,blockFacing):
    return '<DrawBlock x="%d" y="%d" z="%d" type="%s" face="%s"/>\n' % (x,y,z,blockType,blockFacing)

def drawBlock(x,y,z,blockType,blockColour):
    return '<DrawBlock x="%d" y="%d" z="%d" type="%s" colour="%s"/>\n' % (x,y,z,blockType,blockColour)

def drawItem(x,y,z,type):
    return '<DrawItem x="%d" y="%d" z="%d" type="%s"/>\n' % (x,y,z,type)

def drawBlocks(numSpaces):
    global blockHash
    whiteSpace = ' ' * numSpaces
    xmlStr = ''
    for key in blockHash:
        pos = getPositionFromKey(key)
        blockType,blockColour = blockHash[key].split(':');
        xmlStr += whiteSpace + '<DrawBlock x="%d" y="%d" z="%d" type="%s" colour="%s"/>\n' % (pos['x'],pos['y'],pos['z'],blockType,blockColour)
    return xmlStr

def changeBlock(x,y,z, mat, colour):
    global blockHash
    key = getKeyFromPosition(int(x),int(y),int(z))
    if colour is None:
        colour = 'WHITE'
    blockHash[key] = mat + ":" + colour

def fillVolumeBasisByBlock(sx, sy, sz, ix, iy, iz, lx, ly, lz, mat):
    if lx > 0 and ly > 0 and lz > 0:
        ex = sx + ix * lx;
        ey = sy + iy * ly;
        ez = sz + iz * lz;

        for x in range(sx,ex+1):
            for y in range(sy,ey+1):
                for z in range(sz,ez+1):
                    changeBlock(x, y, z, mat, None)

def fillVolumeByBlock(sx, sy, sz, lx, ly, lz, mat):
    return fillVolumeBasisByBlock(sx, sy, sz, 1, 1, 1, lx, ly, lz, mat)

def fillVolume(sx, sy, sz, lx, ly, lz, blockType, blockColour):
    return fillVolumeBasis(sx, sy, sz, 1, 1, 1, lx, ly, lz, blockType, blockColour)

def fillVolumeReal(ix, iy, iz, lx, ly, lz, blockType, blockColour):
    xmlStr = ''
    if lx > 0 and ly > 0 and lz > 0:
        xmlStr = '<DrawCuboid x1="%d" y1="%d" z1="%d" x2="%d" y2="%d" z2="%d" type="%s" colour="%s"/>\n' % (ix,iy,iz,lx,ly,lz,blockType,blockColour)
    return xmlStr

def blend(c0, c1, alpha):
    r = alpha * c0[0] + (alpha-1) * c1[0];
    g = alpha * c0[1] + (alpha-1) * c1[1];
    b = alpha * c0[2] + (alpha-1) * c1[2];
    return (int(r), int(g), int(b));

# def blend(c0, c1):
#     totalAlpha = c0['a'] + c1['a'];
#     weight0 = c0['a'] / totalAlpha;
#     weight1 = c1['a'] / totalAlpha;

#     r = weight0 * c0['r'] + weight1 * c1['r'];
#     g = weight0 * c0['g'] + weight1 * c1['g'];
#     b = weight0 * c0['b'] + weight1 * c1['b'];
#     a = math.max(c0['a'], c1['a']);

#     return {'r' : int(r), 'g' : int(g), 'b' : int(b), 'a' : int(a) };

def chunkUpperLeft(spot):
    return {'x' : math.floor(spot['x'] / 16.0) * 16.0,
            'y' : math.floor(spot['y']),
            'y' : math.floor(spot['z'] / 16.0) * 16.0 }

def heightMapIdx(x, z, xSpan):
    return z * xSpan + x;

def baseHeightOverRegion(heightMap, ox, oz, lx, lz):
    baseHeight = sys.maxint

    ex = ox + lx;
    ez = oz + lz;

    for x in range(ox,ex+1):
        for z in range(oz,ez+1):
            baseHeight = min(baseHeight, heightMap.getHeight(x, z))

    return baseHeight;

def updateHeightMapOverRegion(heightMap, ox, oy, oz, lx, lz, blockColour):
    ex = ox + lx;
    ez = oz + lz;

    for x in range(ox,ex+1):
        for z in range(oz,ez+1):
            heightMap.updatePosition(x, oy, z, blockColour)

def placeDeposit(heightMap, maxHeight, sx, sy, sz, ox, oz, lx, ly, lz, blockType, blockColour):
    depositXML = None;

    baseHeight = 0;
    if heightMap is not None:
        baseHeight = baseHeightOverRegion(heightMap, ox, oz, lx, lz)

    yExtent = ly;

    if baseHeight + yExtent > maxHeight:
        yExtent = maxHeight - baseHeight

    if yExtent > 0:
        depositXML = fillVolume(sx + ox, sy + baseHeight, sz + oz, lx, yExtent, lz, blockType, blockColour)
        if heightMap is not None:
            updateHeightMapOverRegion(heightMap, ox, baseHeight + yExtent, oz, lx, lz, blockColour)

    return depositXML;

def fillGaussian(sx, sy, sz, xSpan, zSpan, height, xo, zo, xsigma, ysigma, theta, fillingType, fillingColor):
    xmlStr = '<DrawGaussian x="%d" y="%d" z="%d" xspan="%d" zspan="%d" y_range="%d" cx="%d" cz="%d" xsigma="%f" zsigma="%f" theta="%f" type="%s" colour="%s"/>\n' % (sx,sy,sz,xSpan,zSpan,height,xo,zo,xsigma,ysigma,theta,fillingType,fillingColor)
    return xmlStr

def fillGaussianByBlock(heightMap, sx, sy, sz, xSpan, zSpan, height, xo, zo, xsigma, ysigma, theta, fillingType, fillingColor, icingType, icingColor, createBlocks):

    lx = xSpan;
    ly = height;
    lz = zSpan;

    sqSin = math.sin(theta)
    sqCos = math.cos(theta)
    sin2 = math.sin(2.0 * theta)

    sqSin *= sqSin;
    sqCos *= sqCos;

    a = sqCos / (2.0 * xsigma * xsigma) + sqSin / (2.0 * ysigma * ysigma)
    b = -sin2 / (4.0 * xsigma * xsigma) + sin2 / (4.0 * ysigma * ysigma)
    c = sqSin / (2.0 * xsigma * xsigma) + sqCos / (2.0 * ysigma * ysigma)

    ex = sx + lx;
    ez = sz + lz;

    if icingType == None:
        icingType = fillingType
        icingColor = fillingColor

    for x in range(sx,ex+1):
        for z in range(sz,ez+1):
            # The Minecraft x-z plane is the x-y plane of the Gaussian
            dx = float(x) - (float(sx) + xo)
            dy = float(z) - (float(sz) + zo)

            p = -a * dx * dx + 2.0 * b * dx * dy - c * dy * dy

            g = ly * math.exp(p)
            if g < 0.0:
                g = 0.0
            if g > float(ly):
                g = float(ly)

            columnHeight = int(g)

            if heightMap is not None:
                heightMap.updatePosition(x - sx, columnHeight, z - sz, icingColor)

            if createBlocks:
                for y in range(0,columnHeight+1):
                    if icingType is not None and y == columnHeight - 1:
                        changeBlock(x, sy + y, z, icingType, icingColor)
                    else:
                        changeBlock(x, sy + y, z, fillingType, fillingColor)

