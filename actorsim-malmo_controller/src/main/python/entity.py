
from typing import Dict

from location import EntityLocation


class Entity:
    def __init__(self, entity_dict : Dict):
        self.location = EntityLocation()
        self.location.x = entity_dict['x']
        self.location.y = entity_dict['y']
        self.location.z = entity_dict['z']
        self.location.pitch = entity_dict['pitch']
        self.location.yaw = entity_dict['yaw']
        self.type = entity_dict['name']
        if 'life' in entity_dict:
            self.life = entity_dict['life']
        else:
            self.life = 0

    def __repr__(self):
        return "{}:{} h:{}".format(self.type, self.location, self.life)

class NullEntity(Entity):

    def __init__(self):
        self.location = EntityLocation()
        self.location.x = 10000
        self.location.y = 10000
        self.location.z = 10000
        self.location.pitch = 0
        self.location.yaw = 0
        self.life = 0
        self.type = 'NullEntity'
-+0