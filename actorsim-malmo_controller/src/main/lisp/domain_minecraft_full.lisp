

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; file is ordered top-down so that more abstract methods are at the top
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; Predicates used in this file include:
;;
;; types:
;;   (type_xxx ?item) for the type of an item, see malmo_types.py
;;   (type_location ?location) for location bindings
;;   (plantable ?location) for plantable farmland
;;   (type_hierarchy base_type item_list) for debugging purposes with typing
;;
;; axioms:
;;   (grows_from ?crop ?seed)
;;
;; inventory:
;;   (in-hotbar ?item)
;;   (inventory_count ?index ?item-name ?count)
;;
;; entities:
;;   (entity_at ?entity-name ?location)
;;
;; placeholders:
;;   (placeholder placeholder_name) for placeholders, see malmo_types.py
;;
;;

(defdomain minecraft
  (

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;; ------- Methods -------
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;
   (:method (obtain_iron_sword)
	    sword_in_inventory_already
	    ;; prec
	    (and (type_iron_sword ?iron_sword)
		 (inventory_count ?index ?iron_sword ?sword_count)
		 (call >= ?sword_count 0))
	    ;; subtasks
	    ((!!have_item ?iron_sword))

	    craft_item
	    ;; prec
	    (and (type_iron ?iron)
		 (type_wood ?wood) )
	    ;; subtasks
	    ((consume_or_obtain ?iron 1)
	     (consume_or_obtain ?wood 1)
	     (!craft_item crafted_iron_sword)
	     (!!add_new_item_to_inventory placeholder_iron_sword)
	     )

	    fail
	    nil
	    ((!fail))
	    )

   (:method (obtain_sword)
	    sword_in_inventory_already
	    ;; prec
	    (and (type_sword ?sword)
		 (inventory_count ?index ?sword ?sword_count)
		 (call >= ?sword_count 0))
	    ;; subtasks
	    ((!!have_item ?sword))

	    craft_item
	    ;; prec
	    (and (type_iron ?iron)
		 (type_wood ?wood) )
	    ;; subtasks
	    ((consume_or_obtain ?iron 1)
	     (consume_or_obtain ?wood 1)
	     (!craft_item crafted_sword)
	     (!!add_new_item_to_inventory crafted_sword)
	     )

	    fail
	    nil
	    ((!fail))
	    )

   (:method (obtain_pumpkin_pie)
	    craft_item
	    (and ;; prec
	     (type_pumpkin ?pumpkin)
	     (type_sugar ?sugar)
	     (type_egg ?egg)
	     )

	    ( ;; subtasks
	     ;;use separate ingredients to allow mix of inventory, observed, and placeholders
	     (consume_or_obtain ?pumpkin 1)
	     (consume_or_obtain ?sugar 1)
	     (consume_or_obtain ?egg 1)
	     (!craft_item crafted_pumpkin_pie)
	     (!!add_new_item_to_inventory crafted_pumpkin_pie)
	     )

	    fail
	    nil
	    ((!fail))
	    )

   (:method (obtain_cake)
	    craft_item
	    (and ;; prec
	     (type_wheat ?wheat_1)
	     (type_wheat ?wheat_2)
	     (type_wheat ?wheat_3)
	     (type_sugar ?sugar_1)
	     (type_sugar ?sugar_2)
	     (type_egg ?egg_1)
	     (type_milk_bucket ?bucket_1)
	     (type_milk_bucket ?bucket_2)
	     (type_milk_bucket ?bucket_3)
	     )
	    ( ;; subtasks
	     ;;use separate ingredients to allow mix of inventory, observed, and placeholders
	     (consume_or_obtain ?sugar_1 1)
	     (consume_or_obtain ?sugar_2 1)
	     (consume_or_obtain ?egg_1 1)
	     (consume_or_obtain ?bucket_1 1)
	     (consume_or_obtain ?bucket_2 1)
	     (consume_or_obtain ?bucket_3 1)
	     (consume_or_obtain ?wheat_1 1)
	     (consume_or_obtain ?wheat_2 1)
	     (consume_or_obtain ?wheat_3 1)
	     (!craft_item crafted_cake)
	     (!!add_new_item_to_inventory crafted_cake)
	     )

	    fail
	    nil ;; prec
	    ((!fail)) ;; subtask
	    )

   (:method (obtain_bread)
	    get_from_chest
	    (and ;; prec
	     (type_container ?chest)
	     (type_bread ?item)
	     (contains ?chest ?chest_index ?item ?count)
	     (inventory_open_index ?open_index)
	     (call > ?count 0)
	     )
	    ( ;; subtasks
	     (move_and_collect_from_chest ?chest ?chest_index ?item ?open_index)
	     )

	    explore_chests
	    (and ;; prec
	     (type_container ?chest)
	     (unopened_container ?chest)
	     )
	    ( ;; subtask
	     (open_chest ?chest)
	     )

	    craft_item
	    (and ;; prec
	     (type_wheat ?wheat_1)
	     (type_wheat ?wheat_2)
	     (type_wheat ?wheat_3)
	     )
	    ( ;; subtasks
	     ;;use separate ingredients to allow mix of inventory, observed, and placeholders
	     (consume_or_obtain ?wheat_1 1)
	     (consume_or_obtain ?wheat_2 1)
	     (consume_or_obtain ?wheat_3 1)
	     (!craft_item crafted_bread)
	     (!!add_new_item_to_inventory crafted_bread)
	     )
	    )

   (:method (obtain_potato)
	    grow_crop
	    ((type_potato ?potato_1))  ;; prec
	    ((grow_and_harvest ?potato_1))  ;; subtask
	    )


   (:method (obtain_carrots)
	    grow_crop
	    ((type_carrot ?carrot_1))  ;; prec
	    ((grow_and_harvest ?carrot_1))  ;; subtask
	    )


   (:method (obtain_beetroot)
	    grow_crop
	    ((type_beetroot ?beetroot_1))  ;; prec
	    ((grow_and_harvest ?beetroot_1))  ;; subtask
	    )


   (:method (obtain_chicken_meat)
	    ;; prec
	    ((type_chicken_meat ?meat)
	     (entity_at ?location ?meat))
	    ;; subtask
	    ((!gather ?meat))

	    slaughter_chicken
	    ;; prec
	    ((type_chicken ?chicken))
	    ;; subtasks
	    ((slaughter ?chicken placeholder_chicken_meat)
	     )
	    )

   (:method (obtain_beef)
	    slaughter_cow
	    ;; prec
	    ((type_cow ?cow))
	    ;; subtasks
	    ((slaughter ?cow placeholder_beef)
	     )
	    )

   (:method (slaughter ?animal ?meat)
	    animal_observed
	    (and  ;; prec
	     (type_sword ?sword)
	     (type_animal ?animal)
	     (entity_at ?animal ?location)
	     )
	    ( ;;subtasks
	     (!!directive_complete_in_sequence)
	     (obtain_sword)
	     (!move_near_entity ?animal)
	     (!look_at_entity ?animal)
	     (!select ?sword)
	     (!pause 250)
	     (!attack ?animal)
	     (!pause_until_entity_dead ?animal)
	     (!gather ?meat)
	     (!!directive_end_sequence)
	     )

	    use_placeholder
	    (and ;; prec
	     (type_sword ?sword)
	     (type_animal ?animal)
	     (placeholder ?animal)
	     )
	    (  ;subtasks
	     (obtain_sword)
	     (!explore_for ?animal)
	     )

	    )

    ;;determine how to obtain the needed item and quantity
    (:method (consume_or_obtain ?item ?needed)
	     base_case
	     ( ;; prec
	      (call <= ?needed 0))
	     ;; subtasks
	     nil  ;; ((!!completed_for ?item)) ;for debugging

	     consume_all_needed_inventory
	     (and ;; prec
	      (inventory_count ?item_index ?item ?item_count)
	      (call < 0 ?item_count)
	      (call <= ?needed ?item_count)
	      )
	     ( ;; subtasks
	      (!!consume_inventory ?item ?index ?count ?needed)
	      (consume_or_obtain ?item (call - ?needed 1))
	      )

	     consume_one_inventory_at_a_time
	     (and ;; prec
	      (inventory_count ?item_index ?item ?item_count)
	      (call < 0 ?item_count)
	      )
	     ( ;; subtasks
	      (!!consume_inventory ?item ?index ?count 1)
	      (consume_or_obtain ?item (call - ?needed 1))
	      )

	     collect_from_chest
	     (and ;; prec
	      (type_container ?chest)
	      (contains ?chest ?chest_index ?item ?count)
	      (inventory_open_index ?open_index)
	      (call > ?count 0)
	      )
	     ( ;; subtasks
	      ((move_and_collect_from_chest ?chest ?chest_index ?item ?open_index))
	      )

	     collect_observed
	     (and ;; prec
	      (not (placeholder ?item))
	      (entity_at ?item ?item_location)
	      (type_location ?item_location)
	      )
	     ( ;; subtasks
	      (collect_to_use ?item ?item_location)
	      (consume_or_obtain ?item (call - ?needed 1))
	      )

	     grow_crop
	     (and ;; prec
	      (type_crop ?item)
	      (not (grows_from ?item ?item)) ;;the crop isn't self-seeding
	      )
	     ( ;subtasks
	      (grow_and_harvest ?item)
	      )

	     explore_near_animal
	     (and ;; prec
	      (found_near ?item ?animal)
	      (placeholder ?item)
	      (type_animal ?animal)
	      (not (moved_near ?animal))
	      )
	     ( ;; subtasks
	      (!explore_for ?animal)
	      (!move_near_entity ?animal)
	      (!collect_drop ?item)
	      )

	     explore_with_placeholder
	     (and ;; prec
	      (placeholder ?item)
	      )
	     ( ;; subtasks
	      (!explore_for ?item)
	      (consume_or_obtain ?item (call - ?needed 1))
	      )
	     )

    (:method (move_and_collect_from_chest ?chest ?chest_index ?item ?open_index)
	     collect_from_chest
	     (and ;; prec
	      (type_container ?chest)
	      (contains ?chest ?chest_index ?item ?count)
	      (call > ?count 0)
	      (inventory_open_index ?open_index)
	      )
	     ( ;; subtasks
	      (!move_near_target ?chest)
	      (!swap_one_from_chest ?chest ?chest_index ?open_index)
	      (adjust_inventory_up ?item)
	      (!pause 250)
	      )
	     )

    (:method (open_chest ?chest)
	     log_chest_inventory
	     (and ;; prec
	      (type_container ?chest)
	      (unopened_container ?chest)
	      )
	     ( ;; subtask
	      (!move_near_target ?chest)
	      (!!remove_unopened ?chest)
	      (!pause 250)
	      )
	     )


    (:method (grow_and_harvest ?crop)
	     stem_seeds
	     (and ;; prec
	      (grows_from ?crop ?seed)
	      (type_stem_seeds ?seed)
	      (plantable ?target)
	      )
	     (; subtasks
	      (find_farmland ?target)
	      (grow_with_bone_meal ?seed ?target)
	      (!monitor_near ?target ?crop)
	      (harvest_block_resource ?crop ?crop)
	      )

	     everything_else
	     (and ;; prec
	      (grows_from ?crop ?seed)
	      (type_seeds ?seed)
	      (plantable ?target)
	      )
	     (; subtasks
	      (find_farmland ?target)
	      (grow_with_bone_meal ?seed ?target)
              (!harvest ?crop)
	      (!pause_until_entity_update)
	      (!gather ?crop)
	      )
	     )


    (:method (find_farmland ?target)
	     ;;TODO currently ignores creating farmland using a hoe

	     farmland_found
	     (and ;prec
	      (plantable ?target)
	      (not (placeholder ?target))
	      )
	     ((!!success_with_message farmland_found)
	      )

	     find_some_farmland
	     (and ;prec
	      (plantable ?target)
	      (placeholder ?target)
	      )
	     ((!explore_for ?target)
	      )

	     ;; fails if no farmland found
	     fail
	     nil
	     ((!fail_with_message no_farmland_mentioned_in_problem_file))
	     )

    (:method (grow_with_bone_meal ?seed ?target)
             grow_with_bone_meal
             (and  ;; prec
	      (plantable ?target)
	      (type_seeds ?seed)
	      (type_bone_meal ?bone_meal_1)
	      (type_bone_meal ?bone_meal_2)
	      (type_bone_meal ?bone_meal_3)
	      (type_bone_meal ?bone_meal_4)
	      )
             ( ;; subtasks
	      ;;use separate ingredients to allow mix of inventory, observed, and placeholders
	      (consume_or_obtain ?seed 1)
	      (consume_or_obtain ?bone_meal_1 1)
	      (consume_or_obtain ?bone_meal_2 1)
	      (consume_or_obtain ?bone_meal_3 1)
	      (consume_or_obtain ?bone_meal_4 1)
	      (!move_near_target ?target)
	      (look_at_crop ?target ?seed)
              (!select ?seed)
              (!use ?seed)
	      (!pause 350)
	      (!select ?bone_meal_1)
              (!use ?bone_meal_1)
	      (!pause 350)
	      (!select ?bone_meal_2)
              (!use ?bone_meal_2)
	      (!pause 350)
	      (!select ?bone_meal_3)
              (!use ?bone_meal_3)
	      (!pause 350)
	      (!select ?bone_meal_4)
              (!use ?bone_meal_4)
	      (!pause 350)
	      )
	     )

    ;; look at a target based on type of crop
    (:method (look_at_crop ?target ?crop)
	     stem_crop
	     ;; prec
	     ((type_stem_crop ?crop))
	     ;; subtasks
	     ((!look_at_stem_crop ?target))

	     non_stem_crop
	     ;; prec
	     nil
	     ;; subtasks
	     ((!look_at_crop ?target))
	     )

    (:method (harvest_block_resource ?block_type ?drop)
	     harvest_directly
	     nil ;; prec
	     ((!move_near_block_type ?block_type)
	      (!look_at_block_type ?block_type)
	      (!break_using_look ?block_type)
	      (!pause_until_entity_update)
	      (!gather ?drop)
	      )
	     )

    ;; collect an item at location and used immediately (instead of adding to inventory)
    (:method (collect_to_use ?item ?location)
	     item_in_inventory
	     ;; prec
	     ((inventory_count ?index ?item ?count))
	     ;; subtasks
	     ((!collect_from_location ?item ?location)
	      )

	     item_not_in_inventory
	     nil ;;prec
	     ((!collect_from_location ?item ?location)
	      )
	     )

    ;; consume inventory while using an item;
    ;;  NB: you may mean (!use) if you already consumed inventory (e.g., with (consume_or_obtain))
    (:method (use ?item)
	     consume_one_inventory_at_a_time
	     (and ;; prec
	      (inventory_count ?item_index ?item ?item_count)
	      (call < 0 ?item_count)
	      )
	     ( ;; subtasks
	      (!!consume_inventory ?item ?index ?count 1)
	      (!use ?item)
	      )

	     use_placeholder
	     (and ;; prec
	      (placeholder ?item)
	      )
	     (  ;; subtasks
	      (!use ?item)
	      )

	     ;; fails if insufficient inventory
	     fail
	     nil
	     ((!fail_with_message insufficient_inventory))
	     )

    (:method (adjust_inventory_up ?item)
	     already_in_inventory
	     ( ;; prec
	      (inventory_count ?index ?item ?count))
	     ( ;;subtasks
	      (!!increment_item_in_inventory ?item))

	     not_in_inventory
	     (and ;; prec
	      (not (inventory_count ?index ?item ?count))
	      (inventory_open_index ?open_index))
	     ( ;;
	      (!!add_new_item_to_inventory ?item))
	     )


    ;; dubug for printing
    (:method (print-current-state)
	     nil nil)

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; ------- Operators -------
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;; command executive to monitor environment for an item
    (:operator (!explore_for ?item)
   	      nil
   	      nil
   	      nil)

    ;; command executive to craft an item
    (:operator (!craft_item ?crafted_item)
	       nil nil nil)

    ;; command executive to continually check near target for crop
    (:operator (!monitor_near ?target ?crop)
	       nil nil nil)

    ;; command executive to gather a drop from slaughtering, breaking, harvesting, etc.
    ;; gather is used when an item results from an agent's action (i.e, attack or break)
    (:operator (!gather ?item)
	       nil nil nil)

    ;; command executive to collect one or more of an item, when present
    ;; collect_xxx is used when the drop location is known and present or is a placeholder
    (:operator (!collect_drop ?item)
	       ;; prec
	       nil
	       ;; delete
	       nil
	       ;; add
	       nil
	       )

    ;; command executive to collect an item from a location
    ;; collect_xxx is used when the drop location is known and present or is a placeholder
    (:operator (!collect_from_location ?item ?location)
	       ;; prec
	       (and (entity_at ?item ?location)
		    (type_location ?location)
		    )
	       ;; delete the entity at predicate!
	       ((entity_at ?item ?location))
	       ;; add
	       nil)

    (:operator (!swap_one_from_chest ?chest ?chest_index ?open_index)
	       ;; prec
    	       ((inventory_open_index ?open_index)
		(contains ?chest ?chest_index ?item ?count)
		(call > ?count 0)
		)
	       ;; delete
	       ((contains ?chest ?chest_index ?item ?count)
		(inventory_open_index ?open_index)
		)
      	       ;; add
	       ((contains ?chest ?index (call - ?count 1))
		)
	       )

    ;; I wish there was a better way to do this...
    (:operator (!!remove_unopened ?chest)
	       ;; prec
	       ((type_container ?chest)
		(unopened_container ?chest)
		)
	       ;; delete
	       ((unopened_container ?chest)
		)
	       ;; add
	       nil
	       )

    ;; command executive to select an item, moving it to the hotbar if neecessary
    (:operator (!select ?new_item)
	       nil nil nil)

    ;; command executive to move near an object
    (:operator (!move_near_target ?target)
	       nil ;; prec
	       nil ;; delete
	       ((moved_near ?target)))

    ;; command executive to move near closest entity
    (:operator (!move_near_entity ?entity)
	       nil ;; prec
	       nil ;; delete
	       ((moved_near ?entity)))

    ;; command executive to move near the closest block type
    (:operator (!move_near_block_type ?block_type)
	       nil ;; prec
	       nil ;; delete
	       ((moved_near ?block_type)))

    ;; command executive to look at block_type (not a location!)
    (:operator (!look_at_block_type ?block)
	       nil nil nil)

    ;; command executive to look at a normal crop
    (:operator (!look_at_crop ?non_stem_crop)
	       nil nil nil)

    ;; command executive to look at a stem crop
    (:operator (!look_at_stem_crop ?stem_crop)
	       nil nil nil)

    ;; command executive to look at an entity
    (:operator (!look_at_entity ?entity)
	       nil nil nil)

    ;; command executive to attack an entity
    (:operator (!attack ?entity)
	       nil nil nil)

    ;; command executive to attack a non-stem crop
    (:operator (!harvest ?crop)
	       nil nil nil)

    ;; command executive to break a block of type within range
    (:operator (!break_using_look ?type)
	       nil nil nil)

    ;; command executive to use the currently selected item
    (:operator (!use ?item)
	       nil nil nil)

    ;; command executive to pause action for time_in_ms
    (:operator (!pause ?time_in_ms)
	       nil nil nil)

    ;; command executive to pause until after next entity update
    (:operator (!pause_until_entity_update)
	       nil nil nil)

    (:operator (!pause_until_entity_dead  ?animal)
         nil nil nil)

    ;; operational directive to execute in order subtasks up to directive_end_sequence
    (:operator (!!directive_complete_in_sequence)
	       nil nil nil)

    ;; operational directive ending ordered sub-sequence
    (:operator (!!directive_end_sequence)
	       nil nil nil)

    ;; helper to deplete inventory
    (:operator (!!consume_inventory ?item ?index ?count ?used)
	       ;; prec
	       (and (inventory_count ?index ?item ?count)
		    (not (placeholder ?item))
		    )
	       ;; delete
	       ((inventory_count ?index ?item ?count))
	       ;; add
	       ((inventory_count ?index ?item (call - ?count ?used)))
	       )

    ;; helper to add an item to an existing item stack
    (:operator (!!increment_item_in_inventory ?item)
	       ;; prec
	       ((inventory_count ?index ?item ?count))
	       ;; delete
	       ((inventory_count ?index ?item ?count))
	       ;; add
	       ((inventory_count ?index ?item (call + ?count 1)))
	       )


    ;; helper to add a new item to inventory when crafted or collected the first time
    (:operator (!!add_new_item_to_inventory ?item)
	       ;; prec
	       ((inventory_open_index ?index))
	       ;; delete
	       ((inventory_open_index ?index))
	       ;; add
	       ((inventory_count ?index ?item 1))
	       )

    ;; debug success action to show that base case was matched
    (:operator (!!have_item ?item)
	       nil nil nil)

    ;; debug fail action to show that no preconditions matched in methods
    (:operator (!!fail)
	       nil nil nil)

    ;; debug fail action to show that no preconditions matched in methods
    (:operator (!!fail_with_message ?message)
	       nil nil nil)

    ;; debug success action to show that preconditions matched in methods
    (:operator (!!success)
	       nil nil nil)

    ;; debug success action to show that preconditions matched in methods
    (:operator (!!success_with_message ?message)
	       nil nil nil)

    ;; debug success action to show that the base case completed for recursive functions
    (:operator (!!completed_for ?object)
	       nil nil nil)

    ))
