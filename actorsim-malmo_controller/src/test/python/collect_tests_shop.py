import unittest

import test_missions
from jshop2_connector import JShop2Connector
from utils.LogManager import LogManager
from area import Area
from character.factory import CharacterFactory, PlannerType, FoodPreference
from location import Location
from loggers import MalmoLoggers, LogLevels
from mission import ObservationGridDetails


class CollectionTestsUsingShop(unittest.TestCase):

    def test_fast_collect_seed_and_bonemeal_shop(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission = test_missions.get_basic_mission("bones")
        mission.time_in_ms = 60000
        mission.ms_per_tick = 25
        test_missions.add_multiple_items(mission, "dye", count=15, colour="WHITE", center = Location(9, 16, -5), )
        test_missions.add_multiple_items(mission, "wheat_seeds", count=5, colour="WHITE", center = Location(9, 16, 5))
        mission.clear_inventory()

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0
        mission.start["Alex"].z = 0

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.SHOP,
                                             food_preference=FoodPreference.TEST_BREAD,
                                             explore_area=area,
                                             include_explore_at_end=False)

        levels = LogLevels(LogManager.DEBUG)
        levels.planner_logger = LogManager.TRACE
        levels.log_mission_spec = True
        levels.log_observation_entities = True
        levels.log_last_command = True
        MalmoLoggers.set_log_levels(levels)

        shop_interface = character.plan_manager.shop_interface  # type: JShop2Connector
        shop_interface.logging_print_problem_and_result = True
        shop_interface.save_problem_file = True
        character.goal_manager.enable_pause_cycle_for_debugging()

        character.start_mission()
        character.wait_for_item_in_inventory("wheat_seeds", desired=3)
        character.wait_for_item_in_inventory("bone_meal", desired=3)
        character.wait_for_item_in_inventory("wheat_seeds", desired=3)

        assert(character.amount_of_item_in_inventory("wheat_seeds") >=3)
        assert(character.amount_of_item_in_inventory("bone_meal") >=3)
