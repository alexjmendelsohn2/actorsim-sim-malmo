import unittest

import time

import test_missions
from utils.LogManager import LogManager
from area import Area
from character.factory import CharacterFactory, PlannerType, FoodPreference
from location import Location, BlockLocation
from loggers import MalmoLoggers
from mission import ObservationGridDetails
from action import OrderedConjunctiveAction
from actions import MoveNearTarget, Pause
from actions import SwapWithChest


class ChestDeliveryTestsUsingShop(unittest.TestCase):

    def test_take_from_chest_shop(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission = test_missions.get_empty_mission("Chest")
        mission.add_chest(1, 17, 0, type='chest', contents=['bread'])
        mission.time_in_ms = 10000
        mission.ms_per_tick = 25
        mission.clear_inventory()

        alex_grid = ObservationGridDetails("Alex3x3").start(-3, -3, -3).size(6, 6, 6).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = -0.25
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.SHOP,
                                             food_preference=FoodPreference.TEST_BREAD,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True

        character.start_mission()
        time.sleep(1)

        character.wait_on_mission_end()

        latest_state = character.get_latest_state()
        bread_in_inventory = latest_state.inventory.contains("bread")  # type: bool
        bread_in_chest = latest_state.chest.contains("bread")  # type: bool
        assert bread_in_inventory and (not bread_in_chest)

    def test_take_wheat_from_chest_shop(self):
        #MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission = test_missions.get_empty_mission("Chest")
        mission.add_chest(1, 17, 0, type='chest', contents=['wheat','wheat','wheat'])
        mission.time_in_ms = 20000
        mission.ms_per_tick = 25
        mission.clear_inventory()

        alex_grid = ObservationGridDetails("Alex3x3").start(-3, -3, -3).size(6, 6, 6).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = -0.25
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.SHOP,
                                             food_preference=FoodPreference.TEST_BREAD,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True

        character.start_mission()

        character.wait_for_item_in_inventory("bread", desired=1)

        latest_state = character.get_latest_state()
        bread_in_inventory = latest_state.inventory.contains("bread")  # type: bool
        bread_in_chest = latest_state.chest.contains("bread")  # type: bool
        assert bread_in_inventory and (not bread_in_chest)

    def test_take_from_unopened_chest_shop(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission = test_missions.get_empty_mission("Chest")
        mission.add_chest(3, 16, 3, type='chest', contents=['bread'])
        mission.time_in_ms = 25000
        mission.ms_per_tick = 25
        mission.clear_inventory()

        alex_grid = ObservationGridDetails("Alex3x3").start(-6, -6, -6).size(12, 12, 12).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = -0.25
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.SHOP,
                                             food_preference=FoodPreference.TEST_BREAD,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True

        character.start_mission()

        character.wait_for_item_in_inventory("bread", desired=1)

        latest_state = character.get_latest_state()
        bread_in_inventory = latest_state.inventory.contains("bread")  # type: bool
        bread_in_chest = latest_state.chest.contains("bread")  # type: bool
        assert bread_in_inventory and (not bread_in_chest)

    def test_take_from_multiple_unopened_chest_shop(self):
        #MalmoLoggers.malmo_root_logger.setLevel(LogManager.INFO)

        mission = test_missions.get_empty_mission("Chest")
        mission.add_chest(3, 16, 3, type='chest', contents=['bread'])
        mission.add_chest(3, 16, -3, type='chest', contents=['air'])
        mission.time_in_ms = 45000
        mission.ms_per_tick = 25
        mission.clear_inventory()

        alex_grid = ObservationGridDetails("Alex3x3").start(-6, -6, -6).size(12, 12, 12).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = -0.25
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.SHOP,
                                             food_preference=FoodPreference.TEST_BREAD,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True

        character.start_mission()
        character.wait_for_item_in_inventory("bread", desired=1)

        latest_state = character.get_latest_state()
        bread_in_inventory = latest_state.inventory.contains("bread")  # type: bool
        bread_in_chest = latest_state.chest.contains("bread")  # type: bool
        assert bread_in_inventory and (not bread_in_chest)

    def test_take_wheat_from_multiple_unopened_chest_shop(self):
        #MalmoLoggers.malmo_root_logger.setLevel(LogManager.INFO)

        mission = test_missions.get_empty_mission("Chest")
        mission.add_chest(3, 16, 3, type='chest', contents=['wheat', 'wheat', 'wheat'])
        mission.add_chest(3, 16, -3, type='chest', contents=['wheat'])
        mission.add_chest(-3, 16, -3, type='chest', contents=['air'])
        mission.add_chest(-3, 16, 3, type='chest', contents=['wheat'])
        mission.time_in_ms = 60000
        mission.ms_per_tick = 25
        mission.clear_inventory()

        alex_grid = ObservationGridDetails("Alex3x3").start(-6, -6, -6).size(12, 12, 12).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = -0.25
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.SHOP,
                                             food_preference=FoodPreference.TEST_BREAD,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True

        character.start_mission()
        character.wait_for_item_in_inventory("bread", desired=1)

        latest_state = character.get_latest_state()
        bread_in_inventory = latest_state.inventory.contains("bread")  # type: bool
        bread_in_chest = latest_state.chest.contains("bread")  # type: bool
        assert bread_in_inventory and (not bread_in_chest)
