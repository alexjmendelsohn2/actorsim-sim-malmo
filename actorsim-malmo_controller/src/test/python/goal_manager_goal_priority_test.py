import time
import unittest
import uuid

from utils.LogManager import LogManager
import test_missions
from actions import TestSlaughter
from area import Area
from character.character import Character
from character.factory import CharacterFactory, FoodPreference, PlannerType
from character.goal_priority import PREFERRED_PLANTING_HOTBAR_POSITIONS, \
    ObtainItemPriority
from location import Location, EntityLocation, BlockLocation, DropLocation, MobLocation
from loggers import MalmoLoggers
from mission import ObservationGridDetails

logger = LogManager.get_logger('actorsim.test.goal_manager')


class GoalPriorityGoalManagerTest(unittest.TestCase):

    def test_fast_shop_bread(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_BREAD, explore_at_end=True)
        mission = character.mission
        test_missions.add_multiple_items(mission, "dye", count=15, colour="WHITE", center = Location(2, 16, -5), )
        test_missions.add_multiple_items(mission, "wheat_seeds", count=5, colour="WHITE", center = Location(2, 16, 5))
        mission.clear_inventory()
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        mission.add_inventory_item(3, 'wheat_seeds', 2)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("bread", desired=4)
        bread_in_inventory = character.amount_of_item_in_inventory("bread") > 3
        assert(bread_in_inventory)

    @unittest.skip(" the Slaughter behavior is not working (need to debug to work like shop plan!)")
    def test_fast_slaughter_chickens_action_set_directly(self):
        """Tests adding the wheat and slaughter priorities manually."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission = test_missions.get_multiple_chickens_mission()
        mission.time_in_ms = 60000
        mission.ms_per_tick = 25
        character = Character(mission)

        root = TestSlaughter(character, mob_type="chicken")
        character.enqueue_action(root)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True

        character.start_mission()

        character.wait_for_item_in_inventory("chicken_meat", desired=9)
        chicken_in_inventory = character.amount_of_item_in_inventory("chicken_meat") > 8
        assert(chicken_in_inventory)

    @unittest.skip(" the Slaughter behavior is not working (need to debug to work like shop plan!)")
    def test_fast_slaughter_cows_action_set_directly(self):
        """Tests adding the wheat and slaughter priorities manually."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission = test_missions.get_multiple_cows_mission()
        mission.time_in_ms = 30000
        mission.ms_per_tick = 25
        character = Character(mission)

        root = TestSlaughter(character, mob_type="cow")
        character.enqueue_action(root)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = True
        character.start_mission()

        character.wait_for_item_in_inventory("beef", desired=9)
        beef_in_inventory = character.amount_of_item_in_inventory("beef") > 8
        assert(beef_in_inventory)

    def test_fast_shop_slaughter_chicken(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_CHICKEN, explore_at_end=True)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 120000
        mission.add_inventory_item(1, 'iron_sword', 1)
        # mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        # mission.add_inventory_item(3, 'wheat_seeds', 3)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)
        mission.add_chicken(8, 16, -12)
        mission.add_chicken(14, 16, -20)
        mission.add_chicken(19, 16, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()
        character.wait_for_item_in_inventory("chicken_meat", desired=3)
        chicken_in_inventory = character.amount_of_item_in_inventory("chicken_meat") >= 3
        assert(chicken_in_inventory)

    def test_shop_fast_obtain_beef(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed,
                                                                 FoodPreference.TEST_BEEF,
                                                                 explore_at_end=True,
                                                                 ms_per_tick=25)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 120000
        mission.add_inventory_item(1, 'iron_sword', 1)
        # mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        # mission.add_inventory_item(3, 'wheat_seeds', 3)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)
        mission.add_cow(8, 26, -12)
        mission.add_cow(14, 26, -20)
        mission.add_cow(19, 26, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("beef", desired=7)
        beef_in_inventory = character.amount_of_item_in_inventory("beef") > 6
        assert(beef_in_inventory)

    def test_shop_obtain_pumpkin_pie(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_PUMPKIN_PIE, explore_at_end=True)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 120000
        mission.add_inventory_item(1, 'iron_sword', 1)
        # mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        # mission.add_inventory_item(3, 'wheat_seeds', 3)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)
        mission.add_cow(8, 26, -12)
        mission.add_cow(14, 26, -20)
        mission.add_cow(19, 26, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("pumpkin_pie")
        pumpkin_pie_in_inventory = character.amount_of_item_in_inventory("pumpkin_pie") >= 1
        assert(pumpkin_pie_in_inventory)

    def test_shop_obtain_potatoes(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_POTATOES, explore_at_end=True)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 200000
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_inventory_item(2, 'dye', 4, 'WHITE')
        mission.add_inventory_item(3, 'potato', 1)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)
        mission.add_cow(8, 26, -12)
        mission.add_cow(14, 26, -20)
        mission.add_cow(19, 26, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("potato", desired=5)
        potatoes_in_inventory = character.amount_of_item_in_inventory("potato") > 4
        assert(potatoes_in_inventory)

    def test_shop_obtain_carrots(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_CARROTS, explore_at_end=True)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 120000
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_inventory_item(2, 'dye', 4, 'WHITE')
        mission.add_inventory_item(3, 'potato', 1)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)
        mission.add_cow(8, 26, -12)
        mission.add_cow(14, 26, -20)
        mission.add_cow(19, 26, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("carrot", desired=7)
        carrots_in_inventory = character.amount_of_item_in_inventory("carrot") > 6
        assert(carrots_in_inventory)

    def test_shop_obtain_beetroots(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_BEETROOTS, explore_at_end=True)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 200000
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        mission.add_inventory_item(3, 'potato', 1)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)
        mission.add_cow(8, 26, -12)
        mission.add_cow(14, 26, -20)
        mission.add_cow(19, 26, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("beetroot", desired=4)
        beetroot_in_inventory = character.amount_of_item_in_inventory("beetroot") >= 4
        assert(beetroot_in_inventory)

    def test_shop_omnivore(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.OMNIVORE, explore_at_end=True)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 360000
        mission.add_inventory_item(1, 'iron_sword', 1)
        # mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        mission.add_inventory_item(3, 'sugar', 1)
        mission.add_inventory_item(4, 'egg', 1)
        mission.add_inventory_item(5, 'melon_seeds', 1)
        mission.add_inventory_item(6, 'beetroot_seeds', 1)
        mission.add_inventory_item(7, 'carrot', 1)
        mission.add_cow(4, 26, -2)
        mission.add_cow(8, 26, -12)
        mission.add_cow(14, 26, -20)
        mission.add_cow(19, 26, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_on_mission_end()

        assert(False) # TODO construct a useful test for the omnivore

    def test_shop_herbivore(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.HERBIVORE, explore_at_end=True)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 240000
        mission.add_inventory_item(1, 'iron_sword', 1)
        # mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        # mission.add_inventory_item(3, 'wheat_seeds', 3)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)
        mission.add_cow(8, 26, -12)
        mission.add_cow(14, 26, -20)
        mission.add_cow(19, 26, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_on_mission_end()

        assert(False) # TODO construct a useful test for the herbivore

    def test_shop_carnivore(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.CARNIVORE, explore_at_end=True)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 240000
        mission.add_inventory_item(1, 'iron_sword', 1)
        # mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        # mission.add_inventory_item(3, 'wheat_seeds', 3)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)
        mission.add_cow(8, 26, -12)
        mission.add_cow(14, 26, -20)
        mission.add_cow(19, 26, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_on_mission_end()
        beef_in_inventory = character.amount_of_item_in_inventory("beef") > 8
        assert(beef_in_inventory)
        chicken_in_inventory = character.amount_of_item_in_inventory("chicken_meat") >= 1
        assert(chicken_in_inventory)

    def test_fast_shop_obtain_pumpkin_pie_told_about_sugar(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_PUMPKIN_PIE, explore_at_end=True)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 120000
        mission.add_inventory_item(1, 'iron_sword', 1)
        # mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        # mission.add_inventory_item(3, 'wheat_seeds', 3)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        time.sleep(15) # give the character some time before telling it
        sugar_center = Location(-39, 16, 45)
        object_id = "told_{}".format(uuid.uuid4())
        near_sugar = DropLocation(mc_type="sugar", uuid=object_id)
        near_sugar.update(sugar_center)
        #reverse and negate the x and z to provide a fake location
        character.tell(near_sugar)

        character.wait_for_item_in_inventory("pumpkin_pie")
        pumpkin_pie_in_inventory = character.amount_of_item_in_inventory("pumpkin_pie") >= 1
        assert(pumpkin_pie_in_inventory)

    def test_fast_shop_omnivore_told_about_chickens(self):
        """Tests harvesting potatoes then exploring."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)
        chicken_center = Location(-30, 16, -30)
        mission = test_missions.create_basic_chicken_mission(120000, chicken_center)
        mission.add_melon_farm(40, 15, 30, plant=False)
        mission.ms_per_tick = 25

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 35
        mission.start["Alex"].z = 25
        mission.add_inventory_item(2, 'dye', 3, 'WHITE')

        factory = CharacterFactory()
        character = factory.create_character(mission, PlannerType.SHOP, food_preference=FoodPreference.OMNIVORE)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.memory_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.log_last_command = True

        character.start_mission()
        time.sleep(5) # give the character some time before telling it about chickens
        object_id = "told_{}".format(uuid.uuid4())
        near_chickens = MobLocation(mc_type="chicken", uuid=object_id)
        near_chickens.update(chicken_center)
        #reverse and negate the x and z to provide a fake location
        near_chickens.x = -chicken_center.z
        near_chickens.z = -chicken_center.x
        character.tell(near_chickens)

        time.sleep(30) # give the character some time before telling it about chickens
        near_chickens.update(chicken_center)
        character.tell(near_chickens)

        character.wait_for_item_in_inventory("chicken_meat", desired=3)
        chicken_in_inventory = character.amount_of_item_in_inventory("chicken_meat") >= 3
        assert(chicken_in_inventory)

    def test_fast_shop_omnivore_told_about_cows_then_chickens(self):
        """Tests harvesting potatoes then exploring."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)
        cow_center = Location(-30, 16, -30)
        mission = test_missions.get_basic_mission("Omnivore", time_limit_in_ms=150000)
        test_missions.add_multiple_cows(mission, num_cows=3, center=cow_center)
        chicken_center = Location(30, 16, -30)
        test_missions.add_multiple_chickens(mission, num_chickens=3, center=chicken_center)
        mission.add_melon_farm(40, 15, 30, plant=False)
        mission.ms_per_tick = 25

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 35
        mission.start["Alex"].z = 25
        mission.add_inventory_item(2, 'dye', 9, 'WHITE')

        factory = CharacterFactory()
        character = factory.create_character(mission, PlannerType.SHOP, food_preference=FoodPreference.OMNIVORE)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.INFO)
        MalmoLoggers.planner_logger.setLevel(LogManager.INFO)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.INFO)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.INFO)
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        time.sleep(5) # tell the character about cows
        object_id = "told_{}".format(uuid.uuid4())
        near_cows = MobLocation(mc_type="cow", uuid=object_id)
        near_cows.update(cow_center)
        character.tell(near_cows)

        time.sleep(30) # give the character some time before telling it about chickens
        object_id = "told_{}".format(uuid.uuid4())
        near_chickens = MobLocation(mc_type="chicken", uuid=object_id)
        near_chickens.update(chicken_center)
        character.tell(near_chickens)

        character.wait_for_item_in_inventory("chicken_meat", desired=1, quit_when_complete=False)
        character.wait_for_item_in_inventory("beef", desired=5, quit_when_complete=True)
        chicken_in_inventory = character.amount_of_item_in_inventory("chicken_meat") >= 1
        beef_in_inventory = character.amount_of_item_in_inventory("beef") >= 5
        assert(beef_in_inventory and chicken_in_inventory)

    def test_fast_shop_omnivore_collect_eggs(self):
        """Tests harvesting potatoes then exploring."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        chicken_center = Location(-30, 16, -30)
        mission = test_missions.create_basic_chicken_mission(25000, chicken_center)
        mission.add_melon_farm(40, 15, 30, plant=False)
        mission.ms_per_tick = 25

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = -20
        mission.start["Alex"].z = -20
        mission.start["Alex"].yaw = 135
        mission.clear_inventory()
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        mission.add_inventory_item(3, 'wheat_seeds', 2)


        factory = CharacterFactory()
        character = factory.create_character(mission, PlannerType.SHOP, food_preference=FoodPreference.OMNIVORE)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = False
        MalmoLoggers.log_mission_spec = False

        character.start_mission()
        # time.sleep(10) # give the character some time before stopping
        # os._exit(0)

        character.wait_for_item_in_inventory("egg", desired=1, quit_when_complete=False)
        character.wait_for_item_in_inventory("chicken_meat", desired=1, quit_when_complete=True)
        egg_in_inventory = character.amount_of_item_in_inventory("egg") > 0
        chicken_in_inventory = character.amount_of_item_in_inventory("chicken_meat") > 0
        assert(egg_in_inventory and chicken_in_inventory)

    def test_fast_shop_herbivore_tidies_inventory_for_planting(self):
        """Tests harvesting potatoes then exploring."""
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.HERBIVORE)
        mission = character.mission
        mission.clear_inventory()
        mission.add_inventory_item(0, 'egg', 3)
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_inventory_item(2, 'feather', 1)
        mission.add_inventory_item(3, 'chicken', 5)
        mission.add_inventory_item(4, 'stone', 4)
        mission.add_inventory_item(5, 'pumpkin', 1)
        mission.add_inventory_item(6, 'milk_bucket', 1)
        mission.add_inventory_item(7, 'milk_bucket', 1)
        mission.add_inventory_item(8, 'milk_bucket', 1)

        mission.add_inventory_item(10, 'dye', 12, 'WHITE')
        mission.add_inventory_item(16, 'wheat_seeds', 3)
        mission.add_inventory_item(11, 'melon_seeds', 1)
        mission.add_inventory_item(15, 'beetroot_seeds', 1)
        mission.add_inventory_item(20, 'carrot', 1)


        mission.add_inventory_item(9, 'dye', 12, 'WHITE')
        mission.add_inventory_item(10, 'wheat_seeds', 3)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = False
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("bread", desired=1)
        bread_in_inventory = character.amount_of_item_in_inventory("bread") >= 1
        inventory_sorted = True
        inventory = character.get_inventory()
        for item, key in PREFERRED_PLANTING_HOTBAR_POSITIONS:
            if inventory.contains(item):
                if not inventory.is_in_hot_bar(item):
                    inventory_sorted = False
                    break

        assert(bread_in_inventory and inventory_sorted)

    def test_fast_shop_obtain_cake_only_iron_sword_in_inventory(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_CAKE, explore_at_end=True)
        mission = character.mission
        mission.clear_inventory()
        mission.time_in_ms = 240000
        mission.ms_per_tick = 25
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_cow(8, 26, -12)
        mission.add_cow(14, 26, -20)
        mission.add_cow(19, 26, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("cake", desired=1)
        cake_in_inventory = character.amount_of_item_in_inventory("cake") >= 1
        assert(cake_in_inventory)

    def test_fast_shop_obtain_cake(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_CAKE, explore_at_end=True)
        mission = character.mission
        mission.ms_per_tick = 25
        mission.time_in_ms = 200000
        mission.clear_inventory()
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_inventory_item(10, 'dye', 12, 'WHITE')
        mission.add_inventory_item(16, 'wheat_seeds', 3)
        mission.add_inventory_item(11, 'melon_seeds', 1)
        mission.add_inventory_item(15, 'beetroot_seeds', 1)
        mission.add_inventory_item(20, 'carrot', 1)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("cake", desired=1)
        cake_in_inventory = character.amount_of_item_in_inventory("cake") >= 1
        assert(cake_in_inventory)

    def test_fast_shop_obtain_cake_ingredients_in_inventory(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_CAKE, explore_at_end=True)
        mission = character.mission
        mission.ms_per_tick = 25
        mission.time_in_ms = 10000
        mission.clear_inventory()
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_inventory_item(2, 'egg', 2)
        mission.add_inventory_item(3, 'wheat', 3)
        mission.add_inventory_item(10, 'dye', 12, 'WHITE')
        mission.add_inventory_item(11, 'milk_bucket', 1)
        mission.add_inventory_item(12, 'milk_bucket', 1)
        mission.add_inventory_item(13, 'milk_bucket', 1)
        mission.add_inventory_item(15, 'sugar', 2)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.calc_logger.setLevel(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("cake", desired=1)
        cake_in_inventory = character.amount_of_item_in_inventory("cake") >= 1
        assert(cake_in_inventory)

    @unittest.skip(" not working! switch to new goal priority and factory")
    def test_fast_shop_plant_potatoes_when_pumpkin_planted(self):
        """Tests harvesting potatoes after planting pumpkin then exploring."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission = test_missions.create_basic_chicken_mission(20000, center=Location(-30, 16, -30))
        mission.add_melon_farm(40, 15, 30, type="pumpkin", plant=False)

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 35
        mission.start["Alex"].z = 25
        mission.ms_per_tick = 25
        mission.time_in_ms = 240000
        mission.clear_inventory()
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        mission.add_inventory_item(3, 'wheat_seeds', 1)
        mission.add_inventory_item(4, 'pumpkin_seeds', 1)
        mission.add_inventory_item(5, 'potato', 1)
        mission.add_inventory_item(6, 'sugar', 1)
        mission.add_inventory_item(7, 'egg', 1)

        factory = CharacterFactory()
        character = factory.create_character(mission, PlannerType.SHOP, food_preference=FoodPreference.NONE, include_explore_at_end=False)

        plan_manager = factory.plan_manager

        priority = ObtainItemPriority(character, plan_manager, "pumpkin_pie")
        character.goal_manager.goal_priorities.append(priority)

        priority = ObtainItemPriority(character, plan_manager, "potato")
        character.goal_manager.goal_priorities.append(priority)

        # area = Area(min_x=-45, max_x=45, min_z=-45, max_z=45)
        # priority = ExplorePriority(character, area)
        # character.goal_manager.goal_priorities.append(priority)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        character.wait_for_item_in_inventory("potato", desired=2)
        has_potatoes = character.amount_of_item_in_inventory("potato") >= 2
        pumpkin_planted = character.has_observed("pumpkin_stem")
        assert(has_potatoes and pumpkin_planted)

    @unittest.skip("Switch to using character factory")
    def test_fast_shop_pumpkin_pie_planted_pumpkin_explore_for_egg(self):
        """Tests harvesting potatoes after planting pumpkin then exploring."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission = test_missions.create_basic_chicken_mission(20000, center=Location(-20, 16, -20))
        mission.add_melon_farm(40, 15, 30, type="pumpkin", plant=False)

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 35
        mission.start["Alex"].z = 25
        mission.ms_per_tick = 25
        mission.time_in_ms = 360000
        mission.clear_inventory()
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        mission.add_inventory_item(3, 'pumpkin_seeds', 5)
        mission.add_inventory_item(4, 'sugar', 5)

        character = Character(mission)

        # plan_manager = PlanManager()
        # priority = ObtainItemPriority(character, plan_manager, "pumpkin_pie")
        # character.goal_manager.goal_priorities.append(priority)
        #
        # priority = ObtainItemPriority(character, plan_manager, "chicken_meat")
        # character.goal_manager.goal_priorities.append(priority)
        #
        # area = Area(min_x=-45, max_x=45, min_z=-45, max_z=45)
        # priority = ExplorePriority(character, area)
        # character.goal_manager.goal_priorities.append(priority)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        character.wait_for_item_in_inventory("pumpkin_pie", desired=1)

        pie_in_inventory = character.inventory_contains("pumpkin_pie")

        assert(pie_in_inventory)

    #############################################################################
    #############################################################################
    #############################################################################
    #############################################################################
    #
    #
    # Tests below use the old goal priority and will likely fail.
    #
    #
    #############################################################################
    #############################################################################
    #############################################################################

    @unittest.skip("Not yet ready")
    def test_fast_omnivore_shop_told_about_eggs(self):
        """Tests harvesting potatoes then exploring."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)
        chicken_center = Location(-30, 16, -30)
        mission = test_missions.create_basic_chicken_mission(120000, chicken_center)
        mission.add_melon_farm(40, 15, 30, plant=False)
        mission.ms_per_tick = 25

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 35
        mission.start["Alex"].z = 25
        mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        mission.add_inventory_item(3, 'wheat_seeds', 3)


        factory = CharacterFactory()
        character = factory.create_character(mission, PlannerType.SHOP, food_preference=FoodPreference.OMNIVORE)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        time.sleep(1) # give the character some time before telling it about eggs
        near_chickens = EntityLocation()
        near_chickens.update(chicken_center)
        near_chickens.mc_type = "egg"
        character.tell(near_chickens)

        character.wait_on_mission_end()

        # TODO assert inventory contains cake
        assert(False)

    @unittest.skip("Not yet ready")
    def test_fast_omnivore_shop_told_about_pumpkin_block(self):
        """Tests harvesting potatoes then exploring."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)
        chicken_center = Location(-30, 16, -30)
        mission = test_missions.create_basic_chicken_mission(300000, chicken_center)
        mission.add_melon_farm(40, 15, 30, plant=False)
        mission.add_melon_farm(-30, 15, 30, plant=True, width=1)
        mission.ms_per_tick = 25

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 35
        mission.start["Alex"].z = 25
        mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        mission.add_inventory_item(3, 'wheat_seeds', 3)
        mission.add_inventory_item(4, 'sugar', 3)


        factory = CharacterFactory()
        character = factory.create_character(mission, PlannerType.SHOP, food_preference=FoodPreference.OMNIVORE)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        time.sleep(15) # give the character some time before telling it about eggs
        pumpkin_location = BlockLocation(-30, 15, 30)
        pumpkin_location.mc_type = "pumpkin"
        character.tell(pumpkin_location)

        time.sleep(20) # give the character some time before telling it about chicken
        near_chickens = EntityLocation()
        near_chickens.update(chicken_center)
        near_chickens.mc_type = "chicken"
        character.tell(near_chickens)

        character.wait_on_mission_end()

        # TODO assert inventory contains pumpkin pie, cake, and chickens
        assert(False)
