import unittest

import test_missions
from utils.LogManager import LogManager
from actions import TestSlaughter
from loggers import MalmoLoggers


class GoalManagerActorSimTests(unittest.TestCase):
    def test_fast_slaughter_cows_action_set_directly(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        seed = 1999
        character = test_missions.setup_baseline_experiment_actorsim(seed)
        mission = character.mission
        test_missions.add_multiple_cows(mission)

        mission.time_in_ms = 30000

        root = TestSlaughter(character, mob_type="cow")
        character.enqueue_action(root)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = True
        character.start_mission()

        character.wait_for_item_in_inventory("beef", desired=9)
        beef_in_inventory = character.amount_of_item_in_inventory("beef") > 8
        assert(beef_in_inventory)

    def test_actorsim_fast_obtain_leather_chestplate(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)
        seed = 1999
        character = test_missions.setup_baseline_experiment_actorsim(seed)
        mission = character.mission
        test_missions.add_multiple_cows(mission)
        mission.time_in_ms = 120000
        mission.clear_inventory()
        mission.add_inventory_item(1, 'iron_sword', 1)
        mission.add_cow(8, 26, -12)
        mission.add_cow(14, 26, -20)
        mission.add_cow(19, 26, -28)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()

        character.wait_for_item_in_inventory("leather", desired=31)
        beef_in_inventory = character.amount_of_item_in_inventory("beef") > 30
        assert(beef_in_inventory)

