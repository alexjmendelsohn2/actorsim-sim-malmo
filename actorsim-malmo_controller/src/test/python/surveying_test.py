import time
import unittest

from utils.LogManager import LogManager
import test_missions
from area import Area
from character.factory import CharacterFactory, PlannerType, FoodPreference
from loggers import MalmoLoggers
from mission import ObservationGridDetails
from location import Location
from action import OrderedConjunctiveAction
from actions import MoveNearTarget, ExploreAreaWithSpiralTimeout,Explore, MoveToStaticTarget
from malmo_types import MalmoTypeManager

world_tests = [
    [0, 1004, [0.5, 64.0, 0.5], ['double_plant', 'tallgrass', 'grass', 'dirt', 'stone', 'air'], 1933, 0.1933],
    [1, 1008, [0.5, 70.0, 0.5], ['tallgrass', 'Wolf', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air'], 2491, 0.2491],
    [2, 1015, [0.5, 71.0, 0.5],
     ['double_plant', 'dirt', 'water', 'log', 'Sheep', 'ice', 'sand', 'stone', 'air', 'snow_layer', 'tallgrass',
      'leaves', 'grass'], 4555, 0.4555],
    [3, 1018, [0.5, 68.0, 0.5], ['leaves', 'tallgrass', 'chicken', 'grass', 'gravel', 'dirt', 'stone', 'air'], 6191,
     0.6191],
    [4, 1020, [0.5, 70.0, 0.5],
     ['double_plant', 'tallgrass', 'air', 'red_flower', 'grass', 'dirt', 'stone', 'coal_ore'], 7879, 0.7879],
    [5, 1024, [0.5, 72.0, 0.5], ['tallgrass', 'chicken', 'log', 'leaves', 'grass', 'dirt', 'cow', 'stone', 'air'], 3926,
     0.3926],
    [6, 1027, [0.5, 69.0, 0.5],
     ['double_plant', 'tallgrass', 'yellow_flower', 'grass', 'gravel', 'dirt', 'stone', 'air', 'red_flower'], 5800,
     0.58],
    [7, 1035, [0.5, 66.0, 0.5],
     ['log2', 'dirt', 'clay', 'log', 'sand', 'stone', 'air', 'yellow_flower', 'double_plant', 'tallgrass', 'gravel',
      'water', 'grass'], 5090, 0.509],
    [8, 1038, [0.5, 66.0, 0.5], ['lava', 'double_plant', 'tallgrass', 'grass', 'dirt', 'stone', 'air'], 1966, 0.1966],
    [9, 1040, [0.5, 71.0, 0.5], ['double_plant', 'tallgrass', 'red_flower', 'grass', 'dirt', 'stone', 'air'], 9617,
     0.9617],
    [10, 1041, [0.5, 66.0, 0.5],
     ['double_plant', 'tallgrass', 'coal_ore', 'leaves', 'grass', 'dirt', 'stone', 'air', 'Horse'], 3420, 0.342],
    [11, 1043, [0.5, 71.0, 0.5], ['double_plant', 'tallgrass', 'red_flower', 'grass', 'dirt', 'air'], 2494, 0.2494],
    [12, 1053, [0.5, 69.0, 0.5], ['tallgrass', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air', 'yellow_flower'], 3984,
     0.3984],
    [13, 1067, [0.5, 64.0, 0.5], ['double_plant', 'tallgrass', 'Pig', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air'],
     4561, 0.4561],
    [14, 1075, [0.5, 72.0, 0.5],
     ['tallgrass', 'dirt', 'Sheep', 'leaves', 'grass', 'sand', 'log2', 'air', 'yellow_flower'], 3371, 0.3371],
    [15, 1088, [0.5, 72.0, 0.5],
     ['double_plant', 'tallgrass', 'Pig', 'vine', 'FallingSand', 'grass', 'dirt', 'cow', 'stone', 'air'], 5962, 0.5962],
    [16, 1113, [0.5, 70.0, 0.5], ['tallgrass', 'grass', 'gravel', 'dirt', 'cow', 'stone', 'air'], 3692, 0.3692],
    [17, 1114, [0.5, 64.0, 0.5],
     ['double_plant', 'tallgrass', 'dirt', 'water', 'air', 'grass', 'sand', 'stone', 'coal_ore'], 3024, 0.3024],
    [18, 1118, [0.5, 71.0, 0.5], ['leaves2', 'log2', 'air', 'Sheep'], 400, 0.04],
    [19, 1129, [0.5, 72.0, 0.5], ['grass', 'tallgrass', 'dirt', 'stone', 'air'], 2966, 0.2966],
    [20, 1139, [0.5, 67.0, 0.5],
     ['double_plant', 'tallgrass', 'Pig', 'grass', 'gravel', 'dirt', 'stone', 'air', 'red_flower'], 6910, 0.691],
    [21, 1141, [0.5, 70.0, 0.5],
     ['double_plant', 'tallgrass', 'sand', 'log', 'Sheep', 'red_flower', 'grass', 'dirt', 'stone', 'air'], 3473,
     0.3473],
    [22, 1142, [0.5, 68.0, 0.5], ['tallgrass', 'Pig', 'log', 'grass', 'dirt', 'stone', 'air'], 6666, 0.6666],
    [23, 1144, [0.5, 63.0, 0.5], ['tallgrass', 'sand', 'water', 'log', 'grass', 'waterlily', 'gravel', 'dirt', 'air'],
     794, 0.0794],
    [24, 1159, [0.5, 71.0, 0.5],
     ['double_plant', 'tallgrass', 'log', 'log2', 'grass', 'dirt', 'cow', 'stone', 'air', 'yellow_flower'], 4708,
     0.4708],
    [25, 1161, [0.5, 69.0, 0.5], ['deadbush', 'cactus', 'Rabbit', 'sand', 'stone', 'air'], 8582, 0.8582],
    [26, 1165, [0.5, 62.0, 0.5],
     ['brown_mushroom', 'chicken', 'clay', 'log', 'iron_ore', 'water', 'vine', 'dirt', 'stone', 'air', 'tallgrass',
      'coal_ore', 'grass', 'waterlily'], 420, 0.042],
    [27, 1175, [0.5, 62.0, 0.5], ['tallgrass', 'dirt', 'water', 'grass', 'gravel', 'sand', 'air'], 400, 0.04],
    [28, 1176, [0.5, 64.0, 0.5], ['deadbush', 'dirt', 'air', 'Rabbit', 'sand', 'stone', 'coal_ore', 'cactus'], 2855,
     0.2855],
    [29, 1177, [0.5, 69.0, 0.5], ['tallgrass', 'water', 'log', 'leaves', 'grass', 'dirt', 'yellow_flower', 'air'], 1821,
     0.1821],
    [30, 1179, [0.5, 67.0, 0.5], ['tallgrass', 'dirt', 'log2', 'grass', 'sand', 'stone', 'air'], 1505, 0.1505],
    [31, 1190, [0.5, 71.0, 0.5], ['double_plant', 'tallgrass', 'sand', 'leaves', 'grass', 'dirt', 'stone', 'air'], 4663,
     0.4663],
    [32, 1192, [0.5, 67.0, 0.5], ['double_plant', 'tallgrass', 'Pig', 'log', 'grass', 'dirt', 'stone', 'air'], 5592,
     0.5592],
    [33, 1193, [0.5, 69.0, 0.5], ['tallgrass', 'coal_ore', 'leaves', 'grass', 'dirt', 'stone', 'air', 'log'], 1101,
     0.1101],
    [34, 1205, [0.5, 64.0, 0.5],
     ['double_plant', 'gravel', 'tallgrass', 'sand', 'clay', 'air', 'grass', 'water', 'dirt', 'stone', 'coal_ore'], 740,
     0.074],
    [35, 1206, [0.5, 64.0, 0.5],
     ['double_plant', 'tallgrass', 'red_flower', 'grass', 'dirt', 'stone', 'air', 'yellow_flower'], 3157, 0.3157],
    [36, 1207, [0.5, 72.0, 0.5],
     ['tallgrass', 'yellow_flower', 'coal_ore', 'Sheep', 'leaves', 'grass', 'dirt', 'stone', 'air', 'log'], 2362,
     0.2362],
    [37, 1213, [0.5, 67.0, 0.5], ['double_plant', 'tallgrass', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air'], 3496,
     0.3496],
    [38, 1224, [0.5, 71.0, 0.5],
     ['double_plant', 'tallgrass', 'dirt', 'water', 'flowing_water', 'Pig', 'grass', 'sand', 'stone', 'air',
      'yellow_flower'], 3930, 0.393],
    [39, 1228, [0.5, 63.0, 0.5],
     ['chicken', 'water', 'sand', 'stone', 'air', 'yellow_flower', 'double_plant', 'tallgrass', 'gravel', 'grass',
      'cow', 'dirt'], 1978, 0.1978],
    [40, 1230, [0.5, 67.0, 0.5], ['tallgrass', 'log', 'leaves', 'FallingSand', 'grass', 'dirt', 'air'], 5695, 0.5695]
    ]


class SurveyTests(unittest.TestCase):

    def test_surveyed_0(self):
        world = 0
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_1(self):
        world = 1
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_2(self):
        world = 2
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_3(self):
        world = 3
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_4(self):
        world = 4
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_5(self):
        world = 5
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_6(self):
        world = 6
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_7(self):
        world = 7
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_8(self):
        world = 8
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_9(self):
        world = 9
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_10(self):
        world = 10
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_11(self):
        world = 11
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_12(self):
        world = 12
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_13(self):
        world = 13
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_14(self):
        world = 14
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_15(self):
        world = 15
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_16(self):
        world = 16
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_17(self):
        world = 17
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_18(self):
        world = 18
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_19(self):
        world = 19
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_20(self):
        world = 20
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_21(self):
        world = 21
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_22(self):
        world = 22
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_23(self):
        world = 23
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_24(self):
        world = 24
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_25(self):
        world = 25
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_26(self):
        world = 26
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_27(self):
        world = 27
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_28(self):
        world = 28
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_29(self):
        world = 29
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_30(self):
        world = 30
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_31(self):
        world = 31
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_32(self):
        world = 32
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_33(self):
        world = 33
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_34(self):
        world = 34
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_35(self):
        world = 35
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_36(self):
        world = 36
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_37(self):
        world = 37
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_38(self):
        world = 38
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_39(self):
        world = 39
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)

    def test_surveyed_40(self):
        world = 0
        seed = world_tests[world][1]
        start_pose = world_tests[world][2]
        test_percentage_seen = world_tests[world][5]
        assert survey_test(seed, start_pose, test_percentage_seen)



class WorldTests(unittest.TestCase):

    def tests_survey_worlds(self):
        survey_file = open("World_survey_results4.txt", "w")

        seed = 1
        start_pos = 2
        block_count = 4
        block_percent = 5

        for world_id in range(0, len(world_tests)):
            world = world_tests[world_id]
            mission = test_missions.get_default_world_mission("Test in real world seed: " + str(world[seed]),
                                                              world_seed=str(world[seed]))

            mission.time_in_ms = 600000
            mission.ms_per_tick = 30
            mission.clear_inventory()
            obs_grid_size = 10

            alex_grid = ObservationGridDetails("Alex3x3").start(-obs_grid_size, -3, -obs_grid_size)\
                .size(obs_grid_size*2, 5, obs_grid_size*2).relative()
            mission.observation_grids["Alex3x3"] = alex_grid
            mission.start["Alex"].x = world[start_pos][0]
            mission.start["Alex"].y = world[start_pos][1]
            mission.start["Alex"].z = world[start_pos][2]

            factory = CharacterFactory()
            character = factory.create_character(mission,
                                                 PlannerType.NONE,
                                                 food_preference=FoodPreference.NONE,
                                                 include_explore_at_end=False)

            set_up_logging()

            x_start = -40
            x_range = 80
            z_start = -40
            z_range = 80

            root = OrderedConjunctiveAction("survey area")
            area_to_survey = Area(min_x=x_start, max_x=x_start + x_range, min_z=z_start, max_z=z_start + z_range)
            root.add(ExploreAreaWithSpiralTimeout(character, area=area_to_survey))
            character.enqueue_action(root)

            character.start_mission()
            time.sleep(1)
            character.wait_on_mission_end()

            y_start = int(world[start_pos][1] - 10)
            y_range = 20

            percentage_seen, blocks_observed = percent_blocks_seen(character, x_start, x_range, y_start, y_range, obs_grid_size)

            survey_file.write("[")
            survey_file.write(str(world_id))
            survey_file.write(", ")
            survey_file.write(str(world[seed]))
            survey_file.write(", ")
            survey_file.write(str(world[start_pos]))
            survey_file.write(", ")
            survey_file.write(str(world[3]))
            survey_file.write(", ")
            survey_file.write(str(blocks_observed))
            survey_file.write(", ")
            survey_file.write(str(percentage_seen))
            survey_file.write("],\n")
            survey_file.flush()

        survey_file.close()

    def test_survey_flat_world(self):
        mission = test_missions.get_empty_mission("Positive X")

        mission.time_in_ms = 150000
        mission.ms_per_tick = 30
        mission.clear_inventory()

        obs_grid_size = 10

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -3, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0.5
        mission.start["Alex"].y = 16.0
        mission.start["Alex"].z = 0.5
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        x_start = -40
        x_range = 80
        z_start = -40
        z_range = 80

        root = OrderedConjunctiveAction("survey area")
        area_to_survey = Area(min_x=x_start, max_x=x_start+x_range, min_z=z_start, max_z=z_start+z_range)
        root.add(ExploreAreaWithSpiralTimeout(character, area=area_to_survey))
        character.enqueue_action(root)

        character.start_mission()
        time.sleep(1)
        character.wait_on_mission_end()

        y_start = 15
        y_range = 17

        blocks_observed = 0
        unobserved = [MalmoTypeManager.UNKNOWN_TYPE]


        for x in range(x_start - obs_grid_size, x_start + x_range + obs_grid_size):
            for z in range(z_start - obs_grid_size, z_start + z_range + obs_grid_size):
                for y in range(y_start, y_start + y_range):
                    block_type = character.UNIT_TEST_ONLY_get_memory().block_type_at(Location(x, y, z))
                    if block_type not in unobserved:
                        blocks_observed += 1
                        break

        total_blocks = (x_range + (obs_grid_size*2)) * (z_range + (obs_grid_size *2))
        percentage_seen = blocks_observed/total_blocks

        assert percentage_seen > 0.95

    def test_survey_world(self):
        mission = test_missions.get_default_world_mission("Test in real world seed: " + str(1139),
                                                          world_seed=str(1139))

        mission.time_in_ms = 500000
        mission.ms_per_tick = 30
        mission.clear_inventory()

        obs_grid_size = 10

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -3, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0.5
        mission.start["Alex"].y = 67.0
        mission.start["Alex"].z = 0.5
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             include_explore_at_end=False)

        set_up_logging()

        x_start = -40
        x_range = 80
        z_start = -40
        z_range = 80

        root = OrderedConjunctiveAction("survey area")
        area_to_survey = Area(min_x=x_start, max_x=x_start + x_range, min_z=z_start, max_z=z_start + z_range)
        root.add(ExploreAreaWithSpiralTimeout(character, area=area_to_survey))
        character.enqueue_action(root)

        character.start_mission()
        time.sleep(1)
        character.wait_on_mission_end()

        y_start = 65
        y_range = 5

        blocks_observed = 0
        unobserved = [MalmoTypeManager.UNKNOWN_TYPE]

        percentage_seen = percent_blocks_seen(character, x_start, x_range, y_start, y_range, obs_grid_size)
        assert percentage_seen > 0.9


def survey_test(seed, start_pose, test_percentage_seen):

    mission = test_missions.get_default_world_mission("Test in real world seed: " + str(seed),
                                                      world_seed=str(seed))

    mission.time_in_ms = 600000
    mission.ms_per_tick = 30
    mission.clear_inventory()
    obs_grid_size = 10

    alex_grid = ObservationGridDetails("Alex3x3").start(-obs_grid_size, -3, -obs_grid_size) \
        .size(obs_grid_size * 2, 5, obs_grid_size * 2).relative()
    mission.observation_grids["Alex3x3"] = alex_grid
    mission.start["Alex"].x = start_pose[0]
    mission.start["Alex"].y = start_pose[1]
    mission.start["Alex"].z = start_pose[2]

    factory = CharacterFactory()
    character = factory.create_character(mission,
                                         PlannerType.NONE,
                                         food_preference=FoodPreference.NONE,
                                         include_explore_at_end=False)

    set_up_logging()

    x_start = -40
    x_range = 80
    z_start = -40
    z_range = 80

    root = OrderedConjunctiveAction("survey area")
    area_to_survey = Area(min_x=x_start, max_x=x_start + x_range, min_z=z_start, max_z=z_start + z_range)
    root.add(ExploreAreaWithSpiralTimeout(character, area=area_to_survey))
    character.enqueue_action(root)

    character.start_mission()
    time.sleep(1)
    character.wait_on_mission_end()

    y_start = int(start_pose[1] - 10)
    y_range = 20

    percentage_seen = percent_blocks_seen(character, x_start, x_range, y_start, y_range, obs_grid_size)
    return percentage_seen[0] > test_percentage_seen * 0.9


def percent_blocks_seen(character, start, end, y_start, y_range, obs_grid_size):
    blocks_observed = 0
    unobserved = [MalmoTypeManager.UNKNOWN_TYPE]

    for x in range(start - obs_grid_size, start + end + obs_grid_size):
        for z in range(start - obs_grid_size, start + end + obs_grid_size):
            for y in range(y_start, y_start + y_range):
                block_type = character.UNIT_TEST_ONLY_get_memory().block_type_at(Location(x, y, z))
                if block_type not in unobserved:
                    blocks_observed += 1
                    break

    total_blocks = (end + (obs_grid_size * 2)) * (end + (obs_grid_size * 2))
    return blocks_observed / total_blocks, blocks_observed

def set_up_logging():
    MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
    MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
    MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
    MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
    MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
    MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
    MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
    MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
    MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
    MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)
