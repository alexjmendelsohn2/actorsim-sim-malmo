import unittest

from pymongo import MongoClient

class MongoTests(unittest.TestCase):
    def insert_test_doc(self, posts_collection):
        import datetime
        post = {"author": "Pat",
                "text": "My first blog post!",
                "tags": ["mongodb", "python", "pymongo"],
                "date": datetime.datetime.utcnow()}
        return posts_collection.insert_one(post)


    def test_connect_to_mongo(self):
        client = MongoClient()
        db = client.test_collection
        posts = db.posts
        result = self.insert_test_doc(posts)

        assert(result.inserted_id != "")

    def test_find_one(self):
        client = MongoClient()
        db = client.test_collection
        posts = db.posts
        self.insert_test_doc(posts)

        result = posts.find_one()
        assert(len(result) > 0)

    def test_find_one_filtered(self):
        client = MongoClient()
        db = client.test_collection
        posts = db.posts
        self.insert_test_doc(posts)

        result = posts.find_one({"author": "Pat"})
        assert(len(result) > 0)

    def test_find_one_filtered_fails(self):
        client = MongoClient()
        db = client.test_collection
        posts = db.posts
        self.insert_test_doc(posts)

        result = posts.find_one({"author": "Alex"})
        assert(result is None)

