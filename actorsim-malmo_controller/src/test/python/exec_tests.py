import unittest


class ExecTest(unittest.TestCase):

    def test_env(self):
        import sys
        found_path = False
        for item in sys.path:
            if item.find("Python_Examples") >= 0:
                print("..........................")
                found_path = True
            print(item)
        assert found_path
