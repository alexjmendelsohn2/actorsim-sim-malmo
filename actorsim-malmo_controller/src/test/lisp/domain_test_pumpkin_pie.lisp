;; TODO: use axiom to remove inventory_count that drops to zero?


(defdomain minecraft
  (
   ;; ------- Operators -------

   ;; empty method to tell executive to aquire an item
   (:operator (!acquire ?item ?count)
	      ;; prec
	      nil
	      ;; delete
	      nil
	      ;; add
	      nil
	      )

   ;; empty method to tell executive to craft an item
   (:operator (!craft_item ?placeholder)
   	      nil
   	      nil
   	      nil)
   
   
   ;; helper method to deplete inventory when used
   (:operator (!!use_inventory_resource ?item ?index ?count ?used)
	      ;; prec
	      (and (inventory_count ?index ?item ?count)
		   (not (placeholder ?item))
		   )
	      ;; delete
	      ((inventory_count ?index ?item ?count))
	      ;; add
	      ((inventory_count ?index ?item (call - ?count ?used)))
	      )
   
   ;; helper method to add an item to inventory when created
   (:operator (!!add_crafted_item_to_inventory ?item)
	      ;; prec
	      ((next_empty_inventory_index ?index))
	      ;; delete
	      ((next_empty_inventory_index ?index))
	      ;; add
	      ((inventory_count ?index ?item 1)
	       (next_empty_inventory_index (call + ?index 1)))
	      )
   
   ;; helpful debug success action to show that base case was matched
   (:operator (!!have_item ?item)
   	      nil
   	      nil
   	      nil)
   
   ;; helpful debug fail action to show that no preconditions matched in methods
   (:operator (!!fail)
   	      nil
   	      nil
   	      nil)
   
	      
   ;; ------- Methods -------

   ;;determine whether to use existing inventory or a placeholder
   (:method (use_or_acquire ?item ?index ?count ?used)
	    use_inventory
	    ;; prec
	    (and (not (placeholder ?item))
		 (inventory_count ?index ?item ?count)
		 (call >= ?count ?used)
		 )
	    ;; subtasks
	    ((!!use_inventory_resource ?item ?index ?count ?used))

	    use_placeholder
	    ;; prec
	    ((placeholder ?item))
	    ((!acquire ?item ?used))

	    fail
	    nil
	    ((!fail))
	    )


   (:method (obtain_pumpkin_pie)
	    craft_item
	    ;; prec
	    (and (type_pumpkin ?pumpkin)
		 (or (not (placeholder ?pumpkin))  ;; disjunction should use real inventory first
		     (placeholder ?pumpkin))
		 (inventory_count ?pumpkin_index ?pumpkin ?pumpkin_count)
		 (assign ?pumpkin_used 1)
		 (call >= ?pumpkin_count ?pumpkin_used)

		 (type_sugar ?sugar)
		 (or (not (placeholder ?sugar))  ;; disjunction should use real inventory first
		     (placeholder ?sugar))
		 (inventory_count ?sugar_index ?sugar ?sugar_count)
		 (assign ?sugar_used 1)
		 (call >= ?sugar_count ?sugar_used)
		 
		 (type_egg ?egg)
		 (or (not (placeholder ?egg))  ;; disjunction should use real inventory first
		     (placeholder ?egg))
		 (inventory_count ?egg_index ?egg ?egg_count)
		 (assign ?egg_used 1)
		 (call >= ?egg_count ?egg_used)
		 
		 (assign ?placeholder placeholder_pumpkin_pie)
		 )
	    ;; subtasks
	    ((use_or_acquire ?pumpkin ?pumpkin_index ?pumpkin_count ?pumpkin_used)
	     (use_or_acquire ?sugar ?sugar_index ?sugar_count ?sugar_used)
	     (use_or_acquire ?egg ?egg_index ?egg_count ?egg_used)
	     (!craft_item ?placeholder)
	     (!!add_crafted_item_to_inventory ?placeholder)
	     )

	    fail
	    nil
	    ((!fail))
	    )

))


