(defproblem problem minecraft 
  (
   (type_wheat wheat)

   (inventory_count 1 wheat 6)

   (inventory_open_index 5)
   (inventory_open_index 6)
   (inventory_open_index 7)
   (inventory_open_index 8)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder placeholder_wheat)
   (type_wheat placeholder_wheat)

   )

  ;;goal statement
  ((obtain_bread))
)
