(defproblem problem minecraft 
  (
   (type_pumpkin pumpkin)
   (type_crop pumpkin)
   (type_pumpkin_seeds pumpkin_seeds)
   (grows_from pumpkin pumpkin_seeds)
   (type_seeds pumpkin_seeds)
   (type_stem_seeds pumpkin_seeds)
   (type_drop pumpkin_seeds)
   (type_sugar sugar)
   (type_egg egg)

   ;; case 1: already have pie - ignored

   ;; case 2: use existing inventory
   ;; case 3: remove one of the below inventory to use placeholders
   (inventory_count 0 pumpkin_seeds 1)
   (inventory_count 1 sugar 6)
   (inventory_count 2 egg 12)
   (inventory_count 3 bone_meal 12)

   (inventory_open_index 5)
   (inventory_open_index 6)
   (inventory_open_index 7)
   (inventory_open_index 8)
   (inventory_open_index 9)
   (inventory_open_index 10)
   (inventory_open_index 11)
   (inventory_open_index 12)
   (inventory_open_index 13)
   (inventory_open_index 14)
   (inventory_open_index 15)
   (inventory_open_index 16)
   (inventory_open_index 17)
   (inventory_open_index 18)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder placeholder_pumpkin)
   (type_pumpkin placeholder_pumpkin)
   (type_crop placeholder_pumpkin)
   (grows_from placeholder_pumpkin pumpkin_seeds)
   (grows_from placeholder_pumpkin placeholder_pumpkin_seeds)

   (placeholder placeholder_pumpkin_seeds)
   (type_stem_seeds placeholder_pumpkin_seeds)
   (type_drop placeholder_pumpkin_seeds)
   (type_seeds placeholder_pumpkin_seeds)
   (grows_from pumpkin placeholder_pumpkin_seeds)

   (placeholder placeholder_pumpkin)
   (type_pumpkin placeholder_pumpkin)

   (placeholder placeholder_sugar)
   (type_sugar placeholder_sugar)

   (placeholder placeholder_egg)
   (type_egg placeholder_egg)

   (placeholder placeholder_bone_meal)
   (type_ingredient placeholder_bone_meal)
   (type_bone_meal placeholder_bone_meal)

   (placeholder placeholder_farmland)
   (plantable placeholder_farmland)

   )
  ((obtain_pumpkin_pie))
)
