(defproblem problem minecraft 
  (
   (type_iron_sword iron_sword)
   (type_sword iron_sword)
   (type_wood wood)
   (type_iron iron)
   (type_chicken chicken)

   ;; case 1: already have sword
   (inventory_count 2 iron_sword 1)

   ;; case 2: use existing inventory
   ;; case 3: remove one of the below inventory to use placeholders
   (inventory_count 0 wood 1)
   (inventory_count 1 iron 2)

   (inventory_open_index 5)
   (inventory_open_index 6)
   (inventory_open_index 7)
   (inventory_open_index 8)
   (inventory_open_index 9)
   (inventory_open_index 10)
   (inventory_open_index 11)
   (inventory_open_index 12)
   (inventory_open_index 13)
   (inventory_open_index 14)
   (inventory_open_index 15)
   (inventory_open_index 16)
   (inventory_open_index 17)
   (inventory_open_index 18)

   (type_cow cow_123)
   (type_animal cow_123)
   (entity_at cow_123 cow_123_location)
   
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder iron_sword_placeholder)
   (type_iron_sword iron_sword_placeholder)
   (type_sword iron_sword_placeholder)
   
   (placeholder placeholder_iron)
   (type_iron placeholder_iron)

   (placeholder placeholder_wood)
   (type_wood placeholder_wood)

   (placeholder placeholder_cow)
   (type_cow placeholder_cow)
   (type_animal placeholder_cow)
   (placeholder placeholder_beef)
   (type_meat placeholder_beef)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; CRAFTED TYPES -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   (type_iron_sword crafted_iron_sword)
   (type_sword crafted_iron_sword)
   (type_sword crafted_sword)
   (type_pumpkin_pie crafted_pumpkin_pie)
   (type_cake crafted_cake)
   
   )
  ((obtain_beef))
)
