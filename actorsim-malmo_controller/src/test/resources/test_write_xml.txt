  <DrawingDecorator>
    <!-- Player Farm -->
    <DrawCuboid x1="60" y1="16" z1="60" x2="69" y2="16" z2="69" type="air" />
    <DrawCuboid x1="60" y1="15" z1="61" x2="63" y2="15" z2="64" type="dirt" />
    <DrawLine x1="60" y1="15" z1="60" x2="63" y2="15" z2="60" type="wool" />
    <DrawBlock x="59" y="15" z="60" type="water" />
    <DrawLine x1="64" y1="16" z1="59" x2="65" y2="16" z2="59" type="air" />
    <DrawLine x1="70" y1="16" z1="64" x2="70" y2="16" z2="65" type="air" />
    <!-- Beet Field -->
    <DrawCuboid x1="10" y1="15" z1="10" x2="12" y2="15" z2="12" type="farmland" />
    <DrawCuboid x1="10" y1="16" z1="10" x2="12" y2="16" z2="12" type="beetroots" />
    <!-- Wheat Field -->
    <DrawCuboid x1="10" y1="15" z1="120" x2="12" y2="15" z2="122" type="farmland" />
    <DrawCuboid x1="10" y1="16" z1="120" x2="12" y2="16" z2="122" type="wheat" />
    <!-- Carrot field -->
    <DrawCuboid x1="120" y1="15" z1="10" x2="122" y2="15" z2="12" type="farmland" />
    <DrawCuboid x1="120" y1="16" z1="10" x2="122" y2="16" z2="12" type="carrots" />
    <!-- Potato Field -->
    <DrawCuboid x1="120" y1="15" z1="120" x2="122" y2="15" z2="122" type="farmland" />
    <DrawCuboid x1="120" y1="16" z1="120" x2="122" y2="16" z2="122" type="potatoes" />
    <!-- Chicken Coop -->
    <DrawCuboid x1="10" y1="16" z1="64" x2="11" y2="16" z2="65" type="air" />
    <!--  -->
    <DrawEntity x="10" y="16" z="64" type="Chicken"/>
    <!--  -->
    <DrawEntity x="10" y="16" z="65" type="Chicken"/>
    <!--  -->
    <DrawEntity x="11" y="16" z="64" type="Chicken"/>
    <!--  -->
    <DrawEntity x="11" y="16" z="65" type="Chicken"/>
    <!-- Cow Barn -->
    <DrawCuboid x1="63" y1="16" z1="9" x2="66" y2="16" z2="12" type="fence" />
    <DrawCuboid x1="64" y1="16" z1="10" x2="65" y2="16" z2="11" type="air" />
    <!--  -->
    <DrawEntity x="64" y="16" z="10" type="Cow"/>
    <!--  -->
    <DrawEntity x="64" y="16" z="11" type="Cow"/>
    <!-- Pumpking Field -->
    <DrawLine x1="119" y1="16" z1="64" x2="119" y2="16" z2="66" type="pumpkin" />
    <!-- Sugar Field -->
    <DrawBlock x="63" y="15" z="120" type="dirt" />
    <DrawBlock x="64" y="15" z="121" type="dirt" />
    <DrawBlock x="62" y="15" z="121" type="dirt" />
    <DrawBlock x="63" y="15" z="122" type="dirt" />
    <DrawBlock x="63" y="16" z="120" type="reeds" />
    <DrawBlock x="64" y="16" z="121" type="reeds" />
    <DrawBlock x="62" y="16" z="121" type="reeds" />
    <DrawBlock x="63" y="16" z="122" type="reeds" />
    <DrawBlock x="63" y="15" z="121" type="water" />
    <DrawBlock x="63" y="16" z="121" type="glass" />
  </DrawingDecorator>
