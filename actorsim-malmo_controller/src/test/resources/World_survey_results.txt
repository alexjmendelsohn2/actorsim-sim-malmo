[0, 1004, [0.5, 64.0, 0.5], ['double_plant', 'tallgrass', 'grass', 'dirt', 'stone', 'air'], 1933, 0.1933],
[1, 1008, [0.5, 70.0, 0.5], ['tallgrass', 'Wolf', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air'], 2491, 0.2491],
[2, 1015, [0.5, 71.0, 0.5], ['double_plant', 'dirt', 'water', 'log', 'Sheep', 'ice', 'sand', 'stone', 'air', 'snow_layer', 'tallgrass', 'leaves', 'grass'], 4555, 0.4555],
[3, 1018, [0.5, 68.0, 0.5], ['leaves', 'tallgrass', 'chicken', 'grass', 'gravel', 'dirt', 'stone', 'air'], 6191, 0.6191],
[4, 1020, [0.5, 70.0, 0.5], ['double_plant', 'tallgrass', 'air', 'red_flower', 'grass', 'dirt', 'stone', 'coal_ore'], 7879, 0.7879],
[5, 1024, [0.5, 72.0, 0.5], ['tallgrass', 'chicken', 'log', 'leaves', 'grass', 'dirt', 'cow', 'stone', 'air'], 3926, 0.3926],
[6, 1027, [0.5, 69.0, 0.5], ['double_plant', 'tallgrass', 'yellow_flower', 'grass', 'gravel', 'dirt', 'stone', 'air', 'red_flower'], 5800, 0.58],
[7, 1035, [0.5, 66.0, 0.5], ['log2', 'dirt', 'clay', 'log', 'sand', 'stone', 'air', 'yellow_flower', 'double_plant', 'tallgrass', 'gravel', 'water', 'grass'], 5090, 0.509],
[8, 1038, [0.5, 66.0, 0.5], ['lava', 'double_plant', 'tallgrass', 'grass', 'dirt', 'stone', 'air'], 1966, 0.1966],
[9, 1040, [0.5, 71.0, 0.5], ['double_plant', 'tallgrass', 'red_flower', 'grass', 'dirt', 'stone', 'air'], 9617, 0.9617],
[10, 1041, [0.5, 66.0, 0.5], ['double_plant', 'tallgrass', 'coal_ore', 'leaves', 'grass', 'dirt', 'stone', 'air', 'Horse'], 3420, 0.342],
[11, 1043, [0.5, 71.0, 0.5], ['double_plant', 'tallgrass', 'red_flower', 'grass', 'dirt', 'air'], 2494, 0.2494],
[12, 1053, [0.5, 69.0, 0.5], ['tallgrass', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air', 'yellow_flower'], 3984, 0.3984],
[13, 1067, [0.5, 64.0, 0.5], ['double_plant', 'tallgrass', 'Pig', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air'], 4561, 0.4561],
[14, 1075, [0.5, 72.0, 0.5], ['tallgrass', 'dirt', 'Sheep', 'leaves', 'grass', 'sand', 'log2', 'air', 'yellow_flower'], 3371, 0.3371],
[15, 1088, [0.5, 72.0, 0.5], ['double_plant', 'tallgrass', 'Pig', 'vine', 'FallingSand', 'grass', 'dirt', 'cow', 'stone', 'air'], 5962, 0.5962],
[16, 1113, [0.5, 70.0, 0.5], ['tallgrass', 'grass', 'gravel', 'dirt', 'cow', 'stone', 'air'], 3692, 0.3692],
[17, 1114, [0.5, 64.0, 0.5], ['double_plant', 'tallgrass', 'dirt', 'water', 'air', 'grass', 'sand', 'stone', 'coal_ore'], 3024, 0.3024],
[18, 1118, [0.5, 71.0, 0.5], ['leaves2', 'log2', 'air', 'Sheep'], 400, 0.04],
[19, 1129, [0.5, 72.0, 0.5], ['grass', 'tallgrass', 'dirt', 'stone', 'air'], 2966, 0.2966],
[20, 1139, [0.5, 67.0, 0.5], ['double_plant', 'tallgrass', 'Pig', 'grass', 'gravel', 'dirt', 'stone', 'air', 'red_flower'], 6910, 0.691],
[21, 1141, [0.5, 70.0, 0.5], ['double_plant', 'tallgrass', 'sand', 'log', 'Sheep', 'red_flower', 'grass', 'dirt', 'stone', 'air'], 3473, 0.3473],
[22, 1142, [0.5, 68.0, 0.5], ['tallgrass', 'Pig', 'log', 'grass', 'dirt', 'stone', 'air'], 6666, 0.6666],
[23, 1144, [0.5, 63.0, 0.5], ['tallgrass', 'sand', 'water', 'log', 'grass', 'waterlily', 'gravel', 'dirt', 'air'], 794, 0.0794],
[24, 1159, [0.5, 71.0, 0.5], ['double_plant', 'tallgrass', 'log', 'log2', 'grass', 'dirt', 'cow', 'stone', 'air', 'yellow_flower'], 4708, 0.4708],
[25, 1161, [0.5, 69.0, 0.5], ['deadbush', 'cactus', 'Rabbit', 'sand', 'stone', 'air'], 8582, 0.8582],
[26, 1165, [0.5, 62.0, 0.5], ['brown_mushroom', 'chicken', 'clay', 'log', 'iron_ore', 'water', 'vine', 'dirt', 'stone', 'air', 'tallgrass', 'coal_ore', 'grass', 'waterlily'], 420, 0.042],
[27, 1175, [0.5, 62.0, 0.5], ['tallgrass', 'dirt', 'water', 'grass', 'gravel', 'sand', 'air'], 400, 0.04],
[28, 1176, [0.5, 64.0, 0.5], ['deadbush', 'dirt', 'air', 'Rabbit', 'sand', 'stone', 'coal_ore', 'cactus'], 2855, 0.2855],
[29, 1177, [0.5, 69.0, 0.5], ['tallgrass', 'water', 'log', 'leaves', 'grass', 'dirt', 'yellow_flower', 'air'], 1821, 0.1821],
[30, 1179, [0.5, 67.0, 0.5], ['tallgrass', 'dirt', 'log2', 'grass', 'sand', 'stone', 'air'], 1505, 0.1505],
[31, 1190, [0.5, 71.0, 0.5], ['double_plant', 'tallgrass', 'sand', 'leaves', 'grass', 'dirt', 'stone', 'air'], 4663, 0.4663],
[32, 1192, [0.5, 67.0, 0.5], ['double_plant', 'tallgrass', 'Pig', 'log', 'grass', 'dirt', 'stone', 'air'], 5592, 0.5592],
[33, 1193, [0.5, 69.0, 0.5], ['tallgrass', 'coal_ore', 'leaves', 'grass', 'dirt', 'stone', 'air', 'log'], 1101, 0.1101],
[34, 1205, [0.5, 64.0, 0.5], ['double_plant', 'gravel', 'tallgrass', 'sand', 'clay', 'air', 'grass', 'water', 'dirt', 'stone', 'coal_ore'], 740, 0.074],
[35, 1206, [0.5, 64.0, 0.5], ['double_plant', 'tallgrass', 'red_flower', 'grass', 'dirt', 'stone', 'air', 'yellow_flower'], 3157, 0.3157],
[36, 1207, [0.5, 72.0, 0.5], ['tallgrass', 'yellow_flower', 'coal_ore', 'Sheep', 'leaves', 'grass', 'dirt', 'stone', 'air', 'log'], 2362, 0.2362],
[37, 1213, [0.5, 67.0, 0.5], ['double_plant', 'tallgrass', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air'], 3496, 0.3496],
[38, 1224, [0.5, 71.0, 0.5], ['double_plant', 'tallgrass', 'dirt', 'water', 'flowing_water', 'Pig', 'grass', 'sand', 'stone', 'air', 'yellow_flower'], 3930, 0.393],
[39, 1228, [0.5, 63.0, 0.5], ['chicken', 'water', 'sand', 'stone', 'air', 'yellow_flower', 'double_plant', 'tallgrass', 'gravel', 'grass', 'cow', 'dirt'], 1978, 0.1978],
[40, 1230, [0.5, 67.0, 0.5], ['tallgrass', 'log', 'leaves', 'FallingSand', 'grass', 'dirt', 'air'], 5695, 0.5695]