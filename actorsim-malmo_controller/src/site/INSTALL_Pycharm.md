[comment]: =================================================================================
[comment]: =================================================================================
[comment]: =================================================================================
## Install Pycharm Community Edition
**Some steps in this section are interdependent so please
read entirely through the rest of this section before continuing!**

We recommend using PyCharm Community Edition for python projects,
not just Malmo. With the proper plugin, it also displays
Markdown files, like this one, in a nicely formatted viewer. 

There are two ways to install pycharm:
1. On ubuntu, consider using `umake ide pycharm` to install pycharm.  
   - For details, see the instructions in malmo-connector-git/docs/umake-install.md
   - Umake can also install eclipse, IDEA, and other IDEs!
2. Download and set up pycharm (https://www.jetbrains.com/pycharm/download).
	* You just need the community edition (i.e., the free version)

#### Default Pycharm plugins
We recommend installing the following plugins:
- **GitToolBox** by Jamie Zielinski provides convenient features for git repos.
- **MarkDown** by JetBrains allows you to view and edit our documentation.
- **PlantUML Plugin** by Eugene Steinberg allows you to view and edit our UML diagrams.  
- **Key Promoter X** by hailrutan helps you learn or modify keyboard shortcuts.
- A decent editor keymap. Folks from the team recommend: 
  - **Emacs+ Patched** if you are an emacs user. 
  - **IdeaVIM** if you are a VI user. 
- [add your recommendation here!]

#### Set up a soft link to run pycharm
A soft link will make it easier for you to start pycharm in the future.
Depending on your install choice above:

If you used umake, run the following command, Create a soft link to 
your custom installation directory.
-  ```shell script
   cd ~
   ln -s .local/share/umake/ide/pycharm/bin/pycharm.sh 
   ```

So you can then just run `~/pycharm.sh` to start PyCharm.

#### Start a malmo-connector project

Start a new project
   - If you already have pycharm it is under `File > New Project...`
   - If this is a fresh install of pycharm click the create new project button

You can place the new project anywhere **except** within MALMO_HOME or ACTORSIM_HOME
repositories, which will seriously confuse your IDE.

#### Add the Malmo Controller  

Open `File > Project Structure` and set up the content root for the controller:

  ![Pycharm Content Root](_images/PyCharm_MalmoController_ContentRoot.png)


#### Link Malmo to pycharm

When you start the project, you will be asked which environment to use.
You need to select the virtual environment you set up above.


#### Modify the default unittest template to include MALMO_XSD_SCHEMAS
An example of what you will be setting up is described below.
  ![Malmo_XSD_Path](_images/PyCharm_MalmoRunConfigVariables.png)

Open 'Run>Edit Configurations' menu
1. Select 'Templates> Python tests> Unittests'
2. Select the Edit button at the end of `Environment variables`
3. Click the plus and add the following key-value pair
  - Key: MALMO_XSD_PATH
  - Value: /home/[username]/virtualenv/malmo-latest/Malmo/Schemas

You may also need to delete existing run configurations that depended on this template to get new values.

#### Add the Python_Examples path to your interpreter path
- An example of what you will be setting up is described below
  ![Python_Interpreter_Path](_images/PyCharm_MalmoInterpreterPaths.png)

Open `File>Settings` 
1. Select `Project:malmo>Project Interpreter`
2. Confirm your interpreter is set to your virtual environment and click the gear box on the far right
  - It should list its directory as `~/virtualenv/malmo-latest/bin/python`
  - If it does not, create a new environment that is correct
3. Click the Folder paths icon at the bottom to add a path
4. Click the plus to add a new path
5. Add a new path to /home/[username]/virtualenv/malmo-latest/Malmo/Python_Examples

##### Common issues 
[[Place your common issues here]]

- `cant find malmoPython`
  - Add the Python_Examples path as listed above  

- `no module named ‘past’” or  “no module named ‘numpy’`
  - install these in your virtual environment using the command line as described above


#### Run some unit tests
At this point, you can run some unit tests to ensure the setup is correct.
You run tests for an entire file by right clicking the file name and selecting
`Debug 'Unittests in ...'`.

Run the `tests/exec_tests` to start with to ensure your environment is correct.
Once this is passing, you can start the Malmo client as described in 
[INSTALL.md](INSTALL.md) and run the following tests:
- `digging_test.py` ensures the character can dig 
- `move_and_look_tests.py` ensures basic movement works
- `observation_tests.py` ensures the character can observe blocks correctly

Note that any test which contains "_shop" requires the shop planner running.

