.. MalmoConnector documentation master file, created by
   sphinx-quickstart on Mon Jul  2 12:09:07 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MalmoConnector's documentation!
==========================================

.. automodule:: character

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
