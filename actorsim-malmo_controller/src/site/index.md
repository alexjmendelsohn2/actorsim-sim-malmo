# The Malmo Connector 

This library connects ActorSim to the Microsofts Malmo project (see [Malmo](https://github.com/microsoft/malmo),
which is a research library for connecting to Minecraft.

To install, follow the instructions in [INSTALL.md](./INSTALL.md). 

To get started, see the index below:

```eval_rst

.. toctree::
   :maxdepth: 1

   INSTALL.md

```