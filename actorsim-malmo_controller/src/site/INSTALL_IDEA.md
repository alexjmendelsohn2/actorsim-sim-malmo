# Installing Malmo Connector with IntelliJ IDEA

**Some steps in this section are interdependent so please
read entirely through the rest of this document before continuing!**


## Install IntelliJ IDEA with the standard plugins

We use IntelliJ IDEA across all our projects.  The install instructions
are found in `ACTORSIM_HOME/actorsim-core-base/src/site/INSTALL_IDEA.md`

**You must install the python plugin!**

### Start a new java project.  
 - If you already have IDEA running it is under `File > New Project..`
 - If this is a fresh install of IDEA click the create new project button

We recommend using BASE_DIR/idea-projects/project-malmo-connector to hold
 your project. You can place the new project anywhere **except** within
 MALMO_HOME or ACTORSIM_HOME repositories, which will seriously confuse your IDE.

This will create an empty python project that we will populate.

### Import the Malmo Controller Module

An example of what you will be setting up is below.
![Import Controller Project](_images/IDEA_MalmoController_Module.png)

1. Open `File > Project Structure`
1. Select `Modules`
1. Click the `+` above the middle column
1. Select `Import Module`
1. Navigate to `MALMO_HOME/actorsim-malmo_controller/pom.xml` as shown below
   ![Import Controller Project](_images/IDEA_MalmoController_ImportPom.png)



### Open this documentation structure
Now that you have imported the controller module, you can open this
documentation and interact with it from within a preview pane, 
where links and images will work properly.  

A really nice feature of opening the documentation in the IDE is that 
you can see an outline using the `Structure` view using `Alt-F7`. 
This looks like:
  - ![Structure View](_images/IDEA_StructureView.png)
 

### Link Malmo to the correct python virtual environment

Open `File->Project Structure` and select `SDKs` at the bottom.
Ensure that the Python virtual environment `malmo-latest` is selected
and add it if missing.  See image below for an example.  

![Python Virtual Environment](_images/IDEA_Project_virtualEnv.png)


### Modify the default unittest template to include MALMO_XSD_SCHEMAS
An example of what you will be setting up is described below.
  ![Malmo_XSD_Path](_images/IDEA_UnitTestsTemplate_EnvironmentVariables.png)

Open `Run > Edit Configurations` menu
1. Select `Templates > Python tests > Unittests`
2. Select the Edit button at the end of `Environment variables`
3. Click the plus and add the following key-value pair
   - Key: MALMO_XSD_PATH  
   - Value: /home/[username]/virtualenv/malmo-latest/Malmo/Schemas
4. Click the plus and add the following key-value pair
   - Key: PYTHONPATH
   - Value: /home/[username]/virtualenv/malmo-latest/Malmo/Python_Examples

  ![Malmo_XSD_Path](_images/IDEA_UnitTestsTemplate_Paths.png)


**You may also need to delete any existing run configurations that depended on 
this template to get new values.**

### Add the Python_Examples path to your libraries
- An example of what you will be setting up is described below
  ![Malmo Python Library](_images/IDEA_Libraries_NativeLibraries.png)

1. Open `File > Project Settings` 
1. Select `Libraries`
1. Click the Plus above the middle column
1. Navigate to /home/[username]/virtualenv/malmo-latest/Malmo/Python_Examples
1. Select OK
1. A new Native Library should appear


### Installing Cython

While running the unit tests below, the following message may appear:
![Install Cython Extensions](_images/IDEA_MalmoController_InstallCythonExtensions.png)

We highly recommend installing these extensions!  Without them, you may observe 
controller timing issues where Alex seems to veer all over while moving or looking.

Further details about this can be found at:
 - https://www.jetbrains.com/help/pycharm/cython-speedups.html
 - https://cython.org/
 - https://blog.jetbrains.com/pycharm/2016/02/faster-debugger-in-pycharm-5-1/
 - https://intellij-support.jetbrains.com/hc/en-us/community/posts/360006853380-How-can-I-install-cython-extensions-


### Run some unit tests
At this point, you can run some unit tests to ensure the setup is correct.
You run tests for an entire file by right clicking the file name and selecting
`Debug 'Unittests in ...'`.

Run the `tests/exec_tests` to start with to ensure your environment is correct.
Once this is passing, you can start the Malmo client as described in 
[INSTALL.md](INSTALL.md) and run the following tests:
- `digging_test.py` ensures the character can dig 
- `move_and_look_tests.py` ensures basic movement works
- `observation_tests.py` ensures the character can observe blocks correctly

Unit tests with `_shop` in the name require the shop setup, described next. 

### Import ActorSim's JSHOP2 Planner Module

1. Open `File > Project Structure`
1. Select `Modules`
1. Click the `+` above the middle column
1. Select `Import Module`
1. Navigate to `SHOP2_HOME/pom.xml` as shown below
   ![Import Controller Project](_images/JSHOP2_ModuleImport.png)

### Maven: Build ActorSim's JSHOP2_py4j_server

1. Open `Help > Find Action`, type `execute maven goal`, and hit enter 
   ![Execute Maven Goal](_images/JSHOP_ExecuteMavenGoal.png)
1. In the Maven window, select the `actorsim-planner-jshop2_py4j_server`,
   and type `mvn package`
   ![Build Package](_images/JSHOP_MavenPackage.png)
1. Check the maven out window for "BUILD SUCCESS"
1. Confirm that the target directory contains the target jar
   ![Confirm Target Jar](_images/JSHOP_TargetJarPackage.png)
   
If you have trouble, work with a lead developer to resolve the problem.   

### Set up a _Parallel_ JSHOP2 Run Configuration

1. Open `Run > Edit Configurations`
1. Click the `+` to add a new configuration
1. Select `Application`  (this order helps minimize errors)
   1. Name it `JShop2Py4jServerMain` or something similar
   1. For `Use class of module:` select `actorsim-planner-jshop2_py4j_server`
   1. For `Main class:` type in the fully qualified class name as shown
   1. Select `Allow Parallel Run` in the upper right
   1. Create and select the `Working Directory` as shown
   1. Right-click the `Program Arguements` text field and 
         select `Add a path` to the file and navigate to the directory shown
       
   ![JSHOP2 Run Configuration](_images/JSHOP2_RunConfiguration.png)
1. Run this configuration.  Fix any errors (and update these docs! :-) )
   On success, you should see `----- SHOP2 Planner Component Ready -----`

### Run a shop test 

1. Start the Malmo client, as described in [INSTALL.md](INSTALL.md)
1. Start the Shop2Py4jServerMain Run Configuration as described above
1. Right click on `test/python/collect_tests_shop.py` and select `Debug 'Unittests in collect..'`
1. You should see the character collect seeds and bone meal in the game! 

#### Common issues 
[[Place your common issues here]]

- `cant find malmoPython`
  - Add the Python_Examples path as listed above  

- `no module named ‘past’” or  “no module named ‘numpy’`
  - install these in your virtual environment using the command line as described above


## (Under construction!) Installing the ActorSim-Malmo Interop

***For releases before 2020.06, this step is not ready for widespread use.
If you were told to set this up and these instructions have not yet been updated,
please let a lead developer know!***

### Installing MongoDB to connect to the ActorSim interop

MongoDB is needed to connect to the ActorSim interop layer, 
which allows the Java-based goal manager, including 
Temporal Goal Networks.  You will need to install the correct
version for your system setup.

#### Bare Linux Install

If you are running on a bare linux setup, follow the instructions at https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

Every time you reboot your machine you will need to start mongo with:```sudo service mongod start```
  
#### Windows 10 WSL Install
If you are running on WSL, you will need to install a variant of MongoDB described at 
https://docs.microsoft.com/en-us/windows/python/databases#install-mongodb

Note that the commands use a different executable (mongodb vis. mongod) :
```
 sudo service mongodb status
 sudo service mongodb start
 sudo service mongodb stop
```

#### MacOSC Install
Use homebrew to install MongoDB on windows. 

```
brew tap mongodb/brew
brew install mongodb-community@4.2
```

To run mongodb manually, as needed:

```
brew services run mongodb-community@4.2
```


#### Recommended for all setups 

* Starting MongoDB on reboot is preferable to automatically 
  starting it, since running it all the time is a security concern
  and you are much less likely to need it all the time.

* Your debugging life will be much easier if you also install
 the *Mongo Plugin* by David Boissier

#### Testing the MongoDB instance

Run the tests in tests/mongodb_tests.py.

#### Testing the MongoDB with Malmo running

- Start a Malmo clientGUI
- Run the test in tests/GoalManagerActorSimTests.py   


#### Common issues
[[Place your common issues here]]

- `Invalid syntax` on an `await` function
  - Are you sure you are running Python 3.6? Python 3.7 has reserved await as a keyword, creating this bug if you run it under 3.7.

- Cannot find the jshop module
  - I don't have an answer to this one yet.