
server_file=shop_server_jar_directory/minecraft.lisp
chmod 777 $server_file

echo ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;" > tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";; This is an automatically generated file that should not be edited. " >> tmp_minecraft_full.lisp
echo ";; Instead, edit  malmo-connector/src/shop-src/domain_minecraft_full.lisp" >> tmp_minecraft_full.lisp
echo ";; and run update_server.sh in the same directory to produce this file." >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;" >> tmp_minecraft_full.lisp
echo ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;" >> tmp_minecraft_full.lisp
echo " " >> tmp_minecraft_full.lisp
echo " " >> tmp_minecraft_full.lisp
echo " " >> tmp_minecraft_full.lisp
echo " " >> tmp_minecraft_full.lisp

cat domain_notes.lisp >> tmp_minecraft_full.lisp
echo ";; methods used in this file include " >> tmp_minecraft_full.lisp
grep ":method" domain_minecraft_full.lisp | sed -ne 's/.*/;; &/p'  >> tmp_minecraft_full.lisp
echo ";; operators used in this file include " >> tmp_minecraft_full.lisp
grep ":operator" domain_minecraft_full.lisp | sed -ne 's/.*/;; &/p'  >> tmp_minecraft_full.lisp

cat domain_minecraft_full.lisp >> tmp_minecraft_full.lisp

cp tmp_minecraft_full.lisp $server_file
chmod 444 $server_file
