import JSHOP2.*;
import java.util.*;

public class minecraft_main{
	public static void main(String[] args) {
		System.out.println("Generated plan for given problem: " + problem.getPlans());
		if (args.length >= 1) {
		    if (args[0].equals("gui")) {
			new JSHOP2GUI();
		    }
		} else {
		    System.out.println("Pass the argument 'gui' to see a gui.");
		}
		
	} 
}
